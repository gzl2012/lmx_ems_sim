%% tunel尽头区域（与W-mytuggel一致）内？？？处于state1或7的飞机    % 1就等，0就走
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% gg组
function tempLag_clear= Lag_clear(gg,center,biaozhi)
global UavTeam
global UavHighway

M = UavTeam.AvailableNumMax;
tempLag_clear = 1;

for k3=1:M
    %         if k3 == j && gg == g % 排除自己
    % if有飞机在tuggel状态或者wait状态
    if UavTeam.Team(gg).Uav(k3).state == 1 || UavTeam.Team(gg).Uav(k3).state == 7
        tempx = UavTeam.Team(gg).Uav(k3).CurrentPos(1,1);
        tempy = UavTeam.Team(gg).Uav(k3).CurrentPos(2,1);
        if biaozhi == 3
            temptuggel = UavHighway.GoNum;
        elseif biaozhi == 2
            temptuggel = UavHighway.AllNum;
        end
        theta1 = theta(temptuggel,gg);
        ph1x1=UavHighway.group(gg).Highway(temptuggel).ph1(1)+UavHighway.group(gg).Highway(temptuggel).rh*sin(theta1);
        ph1x2=UavHighway.group(gg).Highway(temptuggel).ph2(1)+UavHighway.group(gg).Highway(temptuggel).rh*sin(theta1);
        ph1y1=UavHighway.group(gg).Highway(temptuggel).ph1(2)-UavHighway.group(gg).Highway(temptuggel).rh*cos(theta1);
        ph1y2=UavHighway.group(gg).Highway(temptuggel).ph2(2)-UavHighway.group(gg).Highway(temptuggel).rh*cos(theta1);
        ph2x1=UavHighway.group(gg).Highway(temptuggel).ph1(1)-UavHighway.group(gg).Highway(temptuggel).rh*sin(theta1);
        ph2x2=UavHighway.group(gg).Highway(temptuggel).ph2(1)-UavHighway.group(gg).Highway(temptuggel).rh*sin(theta1);
        ph2y1=UavHighway.group(gg).Highway(temptuggel).ph1(2)+UavHighway.group(gg).Highway(temptuggel).rh*cos(theta1);
        ph2y2=UavHighway.group(gg).Highway(temptuggel).ph2(2)+UavHighway.group(gg).Highway(temptuggel).rh*cos(theta1);
        p1=[(ph1x1+ph1x2)/2;(ph1y1+ph1y2)/2];
        p2=[(ph2x1+ph2x2)/2;(ph2y1+ph2y2)/2];
        %% one more time
        p1=[(p1(1)+ph1x2)/2;(p1(2)+ph1y2)/2];
        p2=[(p2(1)+ph2x2)/2;(p2(2)+ph2y2)/2];
        %% one more time
        p1=[(p1(1)+ph1x2)/2;(p1(2)+ph1y2)/2];
        p2=[(p2(1)+ph2x2)/2;(p2(2)+ph2y2)/2];
        x1 = ph2x1;
        x2 = ph2x2;
        x3 = p2(1);
        x4 = p1(1);
        y1 = ph2y1;
        y2 = ph2y2;
        y3 = p2(2);
        y4 = p1(2);
        xv= [x1 x2 x3 x4 x1];
        yv= [y1 y2 y3 y4 y1];
        in = inpolygon(tempx,tempy,xv,yv);
        % 且在终点区域
        if in ==1
            ctn = UavTeam.Team(gg).Uav(k3).CurrentTaskNum;
            NextPdes  =  UavTeam.Team(gg).Uav(k3).Waypoint(:,ctn+1);
            if NextPdes == center
                tempLag_clear = 0;
                break;
            end
        end
    end
end
function theta = theta(ctn,g)
global UavHighway
    Tunnelnow = UavHighway.group(g).Highway(ctn).ph2 - UavHighway.group(g).Highway(ctn).ph1;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    注意Tunnnel1！！
    Tunnel1= [1 0]';
    tempc = dot(Tunnelnow,Tunnel1);   %求内积
    tempd = dot(Tunnelnow,Tunnelnow);  
    tempe = sqrt(tempd);%求Tunnelnow的长度
    tempf = dot(Tunnel1,Tunnel1); 
    tempg = sqrt(tempf); %求Tunnel1的长度
    temph = tempc/(tempe*tempg);
    theta = acos(temph);   %两个向量的夹角
    if Tunnelnow(2)<0
        theta = -theta;
    end
end

% 该飞机位置CP，在不在判断的飞机center区域里，在为1，要等待
function yes= localarea(g,i,center,CP) % 圆心
global currenttunnel
global UavHighway
global UavTeam

yes = 0;
%%%%% 本子上第五点！！ 要改要动3个地方！！非常一致
temp = currenttunnel;
% temp = currenttunnel - 1; 
M = UavTeam.AvailableNumMax;

r = norm(center-UavHighway.group(g).Highway(temp(M*(g-1)+i,1)).ph2); % 半径

if norm(CP-center)<=r
    yes=1;         % 判断在区域内=1
end
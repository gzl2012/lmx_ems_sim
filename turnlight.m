%% 还没完善，完全按照互斥的两组做的
function turnlight()
global UavHighway
global N 
global turnTlight

tempmod = mod (turnTlight,2) + 1; % tunrnTlight从0开始的！！！！！！第一次红灯余0，第二次绿灯余1；+1后刚好和组数一致。
% 所有灯先变
for lg = 1: UavHighway.LightGroup
    for ln = 1: UavHighway.LightNum_g
        templg = UavHighway.Light(lg).gn(1,ln);% 竖着读
        templn = UavHighway.Light(lg).gn(2,ln);
        UavHighway.group(templg).Highway(templn).light = tempmod + 1;
    end
end
% 部分灯再变
for ln = 1: UavHighway.LightNum_g %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 当前只考虑了两组红灯，若多组，需要重新写程序
    templg = UavHighway.Light(1).gn(1,ln); %%%%%% 注意这个1 Light(1)
    templn = UavHighway.Light(1).gn(2,ln); %%%%%% 注意这个1 Light(1)
    if tempmod == 1
        UavHighway.group(templg).Highway(templn).light = 3;     % 不变的是绿灯，变得是红灯
    elseif tempmod == 2
        UavHighway.group(templg).Highway(templn).light = 2;     % 不变的是红灯，变得是绿灯
    end
end
% 灯的关联性
for gg = 1:N
    for mm = 1:UavHighway.AllNum
        if mm == UavHighway.AllNum
            UavHighway.group(gg).Highway(mm).nextconnectedlight = 0;
        else
            UavHighway.group(gg).Highway(mm).nextconnectedlight = UavHighway.group(gg).Highway(mm+1).light;
        end
    end
end
turnTlight = turnTlight + 1;
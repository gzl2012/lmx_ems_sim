function  new = coordinate1(ctn,x,g)
global UavHighway
    %% ƽ��
    o_coordinate =  UavHighway.group(g).Highway(ctn).ph1 - [0;0];
    x_translation = x - o_coordinate;
    %% ��ת��
    Theta = theta(ctn,g);
    if Theta~=0
        x_rotation = x_translation' * [cos(Theta) -sin(Theta); sin(Theta) cos(Theta)];
        new = x_rotation';
    else
        new=x_translation;
    end
end
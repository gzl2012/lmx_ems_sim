function out = MyMap()
global UavHighway
global UavTeam
global N

M = UavTeam.AvailableNumMax;

%% 场景2-2 画家点和投放点
for i = 1: N
    plot(UavTeam.Team(i).Uav(1).HomePos(1,1), UavTeam.Team(i).Uav(1).HomePos(2,1),'gp')
    hold on
    plot(UavTeam.Team(i).Uav(1).Toufangdian(1,1), UavTeam.Team(i).Uav(1).Toufangdian(2,1),'mp')
    hold on
end

%% 周围区域
NetA=[50,20]';
NetB=[50,1500]';
NetC=[1650,1500]';
NetD=[1650,20]';
lineABX=[NetA(1),NetB(1)];
lineABY=[NetA(2),NetB(2)];
plot(lineABX, lineABY,'k');
hold on
lineBCX=[NetC(1),NetB(1)];
lineBCY=[NetC(2),NetB(2)];
plot(lineBCX, lineBCY,'k');
hold on
lineCDX=[NetC(1),NetD(1)];
lineCDY=[NetC(2),NetD(2)];
plot(lineCDX, lineCDY,'k');
hold on
lineDAX=[NetA(1),NetD(1)];
lineDAY=[NetA(2),NetD(2)];
plot(lineDAX, lineDAY,'k');
hold on

%% 场景2-2 Highway plot
for ii=1:N
    % 出航
    for i = 1 : UavHighway.GoNum
        theta1 = theta(i,ii);
        line11 = [UavHighway.group(ii).Highway(i).ph1(1)+UavHighway.group(ii).Highway(i).rh*sin(theta1),UavHighway.group(ii).Highway(i).ph2(1)+UavHighway.group(ii).Highway(i).rh*sin(theta1)];
        line12 = [UavHighway.group(ii).Highway(i).ph1(2)-UavHighway.group(ii).Highway(i).rh*cos(theta1),UavHighway.group(ii).Highway(i).ph2(2)-UavHighway.group(ii).Highway(i).rh*cos(theta1)];
        plot(line11,line12,'g');
        hold on
        line21 = [UavHighway.group(ii).Highway(i).ph1(1)-UavHighway.group(ii).Highway(i).rh*sin(theta1),UavHighway.group(ii).Highway(i).ph2(1)-UavHighway.group(ii).Highway(i).rh*sin(theta1)];
        line22 = [UavHighway.group(ii).Highway(i).ph1(2)+UavHighway.group(ii).Highway(i).rh*cos(theta1),UavHighway.group(ii).Highway(i).ph2(2)+UavHighway.group(ii).Highway(i).rh*cos(theta1)];
        plot(line21,line22,'g');
        hold on
    end
    % 返航
    for i = 1 + UavHighway.GoNum : UavHighway.BackNum + UavHighway.GoNum
        theta2 = theta(i,ii);
        line11 = [UavHighway.group(ii).Highway(i).ph1(1)+UavHighway.group(ii).Highway(i).rh*sin(theta2),UavHighway.group(ii).Highway(i).ph2(1)+UavHighway.group(ii).Highway(i).rh*sin(theta2)];
        line12 = [UavHighway.group(ii).Highway(i).ph1(2)-UavHighway.group(ii).Highway(i).rh*cos(theta2),UavHighway.group(ii).Highway(i).ph2(2)-UavHighway.group(ii).Highway(i).rh*cos(theta2)];
        plot(line11,line12,'m');
        hold on
        line21 = [UavHighway.group(ii).Highway(i).ph1(1)-UavHighway.group(ii).Highway(i).rh*sin(theta2),UavHighway.group(ii).Highway(i).ph2(1)-UavHighway.group(ii).Highway(i).rh*sin(theta2)];
        line22 = [UavHighway.group(ii).Highway(i).ph1(2)+UavHighway.group(ii).Highway(i).rh*cos(theta2),UavHighway.group(ii).Highway(i).ph2(2)+UavHighway.group(ii).Highway(i).rh*cos(theta2)];
        plot(line21,line22,'m');
        hold on
    end
end

axis([-100 1800 -100 1700])
grid on
xlabel('x')
ylabel('y')

out = 0;
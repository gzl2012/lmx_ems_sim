function  new = coordinate2(ctn,x,g)
%% �����
global UavHighway
    %% ��ת��
    Theta = theta(ctn,g);
    if Theta ~= 0
        x_rotation = x' * [cos(Theta) sin(Theta); -sin(Theta) cos(Theta)];
        x_rotation = x_rotation';
    else
        x_rotation = x;
    end
    
    %% ƽ��
    o_coordinate =  UavHighway.group(g).Highway(ctn).ph1 - [0;0];
    new = x_rotation + o_coordinate;
end
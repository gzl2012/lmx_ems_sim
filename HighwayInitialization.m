%% ………………………………手动输入Highway
function UavHighway = HighwayInitialization()  % 分组个数
global N
%% 场景2-2测试
UavHighway.AllNum = 6;
UavHighway.GoNum = 3; % 出航个数，为了简化初始UAV的waypoint过程中tunnel个数的使用
UavHighway.BackNum = 3; % 返航个数，为了简化初始UAV的waypoint过程中tunnel个数的使用

%% Initialize 组2
% Highway 1
UavHighway.group(2).Highway(1).ph1  = [400  900]';
UavHighway.group(2).Highway(1).ph2   = [550  900]';
UavHighway.group(2).Highway(1).rh = 50;
UavHighway.group(2).Highway(1).light = 0; % 红绿灯，0=没灯，1=有灯。2=绿灯，3=红灯
% Highway 2
UavHighway.group(2).Highway(2).ph1  = [550  900]';
UavHighway.group(2).Highway(2).ph2   = [750  900]';
UavHighway.group(2).Highway(2).rh = 50;
UavHighway.group(2).Highway(2).light = 3; % 红绿灯，0=没灯，1=有灯。2=绿灯，3=红灯
% Highway 3
UavHighway.group(2).Highway(3).ph1  = [750  900]';
UavHighway.group(2).Highway(3).ph2   = [1300  900]';
UavHighway.group(2).Highway(3).rh = 50;
UavHighway.group(2).Highway(3).light = 0; % 红绿灯，0=没灯，1=有灯。2=绿灯，3=红灯
% Highway 4
UavHighway.group(2).Highway(4).ph1  = [1300  1000]';
UavHighway.group(2).Highway(4).ph2   = [750  1000]';
UavHighway.group(2).Highway(4).rh = 50;
UavHighway.group(2).Highway(4).light = 0; % 红绿灯，0=没灯，1=有灯。2=绿灯，3=红灯
% Highway 5
UavHighway.group(2).Highway(5).ph1  = [750  1000]';
UavHighway.group(2).Highway(5).ph2   = [550  1000]';
UavHighway.group(2).Highway(5).rh = 50;
UavHighway.group(2).Highway(5).light = 3; % 红绿灯，0=没灯，1=有灯。2=绿灯，3=红灯
% Highway 6
UavHighway.group(2).Highway(6).ph1  = [550  1000]';
UavHighway.group(2).Highway(6).ph2   = [400  1000]';
UavHighway.group(2).Highway(6).rh = 50;
UavHighway.group(2).Highway(6).light = 0; % 红绿灯，0=没灯，1=有灯。2=绿灯，3=红灯

%% Initialize 组1
% Highway 1
UavHighway.group(1).Highway(1).ph1  = [600  1300]';
UavHighway.group(1).Highway(1).ph2   = [600  1050]';
UavHighway.group(1).Highway(1).rh = 50;
UavHighway.group(1).Highway(1).light = 0; % 红绿灯，0=没灯，1=有灯。2=绿灯，3=红灯
% Highway 2
UavHighway.group(1).Highway(2).ph1  = [600  1050]';
UavHighway.group(1).Highway(2).ph2   = [600  850]';
UavHighway.group(1).Highway(2).rh = 50;
UavHighway.group(1).Highway(2).light = 3; % 红绿灯，0=没灯，1=有灯。2=绿灯，3=红灯
% Highway 3
UavHighway.group(1).Highway(3).ph1  = [600  850]';
UavHighway.group(1).Highway(3).ph2   = [600  300]';
UavHighway.group(1).Highway(3).rh = 50;
UavHighway.group(1).Highway(3).light = 0; % 红绿灯，0=没灯，1=有灯。2=绿灯，3=红灯
% Highway 4
UavHighway.group(1).Highway(4).ph1  = [700  300]';
UavHighway.group(1).Highway(4).ph2   = [700  850]';
UavHighway.group(1).Highway(4).rh = 50;
UavHighway.group(1).Highway(4).light = 0; % 红绿灯，0=没灯，1=有灯。2=绿灯，3=红灯
% Highway 5
UavHighway.group(1).Highway(5).ph1  = [700  850]';
UavHighway.group(1).Highway(5).ph2   = [700  1050]';
UavHighway.group(1).Highway(5).rh = 50;
UavHighway.group(1).Highway(5).light = 3; % 红绿灯，0=没灯，1=有灯。2=绿灯，3=红灯
% Highway 6
UavHighway.group(1).Highway(6).ph1  = [700  1050]';
UavHighway.group(1).Highway(6).ph2   = [700  1300]';
UavHighway.group(1).Highway(6).rh = 50;
UavHighway.group(1).Highway(6).light = 0; % 红绿灯，0=没灯，1=有灯。2=绿灯，3=红灯

%% 红绿灯的关联性 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 人工设置的红绿灯变换关联性(分组没能实现自动化)
UavHighway.LightGroup = 2; % 多少组红绿灯，变化一致的为一组
UavHighway.LightNum_g = 2; % 每组多少个红绿灯
temp1 = [1 2]';
temp2 = [1 5]';
temp3 = [2 2]';
temp4 = [2 5]';
UavHighway.Light(1).gn = temp1;
UavHighway.Light(1).gn = [UavHighway.Light(1).gn temp2];
UavHighway.Light(2).gn = temp3;
UavHighway.Light(2).gn = [UavHighway.Light(2).gn temp4];

%% 初始的红绿灯 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 人工设置的只有两个方向的红绿灯，多个方向就必须变化
% 所有红灯 部分绿灯
for ln = 1: UavHighway.LightNum_g % 每组多少个红绿灯
    templg = UavHighway.Light(1).gn(1,ln); % 竖着读
    templn = UavHighway.Light(1).gn(2,ln);
    UavHighway.group(templg).Highway(templn).light = 2;     % 不变的是红灯，变得是绿灯
end
for gg = 1:N
    for mm = 1:UavHighway.AllNum
        if mm == UavHighway.AllNum
            UavHighway.group(gg).Highway(mm).nextconnectedlight = 0;
        else
            UavHighway.group(gg).Highway(mm).nextconnectedlight = UavHighway.group(gg).Highway(mm+1).light;
        end
    end
end
%% 人工初始化UAV
function UavTeam = UAVInitialization(M,rm,l,vmax)
global UavHighway
global N

UavTeam.AvailableNumMax  = M;
UavTeam.gain             = l;

%% 场景2-2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 人工设置HomePos和Toufangdian
% 此处只有两组飞机
for j=1:M
    UavTeam.Team(1).Uav(j).HomePos = [600 1400]';
    UavTeam.Team(1).Uav(j).Toufangdian = [700 200]';
    UavTeam.Team(2).Uav(j).HomePos = [300 900]';
    UavTeam.Team(2).Uav(j).Toufangdian =[1400 1000]';
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 人工设置waypoint
for m = 1:N
    for k = 1: M
        UavTeam.Team(m).Uav(k).Waypoint = [];
        %% 飞机基本属性
        UavTeam.Team(m).Uav(k).r = rm;
        UavTeam.Team(m).Uav(k).vmax = vmax;
        UavTeam.Team(m).Uav(k).ra = 2 * ( rm + (UavTeam.Team(m).Uav(k).vmax/UavTeam.gain) + 1);
        UavTeam.Team(m).Uav(k).CurrentPos =  UavTeam.Team(m).Uav(k).HomePos;
% 基本逻辑遵循：Waypoint = 1.家点隐藏，2.家点起飞，3.进入Tunnel     ----投点*2----    -3.离开tunnel，-2.家点降落，-1.家点消失
        %% 场景2-2 有13个task
        temphome = UavTeam.Team(m).Uav(k).HomePos;
        temptou = UavTeam.Team(m).Uav(k).Toufangdian;
        % 前面家点
        UavTeam.Team(m).Uav(k).Waypoint = [UavTeam.Team(m).Uav(k).Waypoint temphome temphome];
        % 前面tunnel
        for gotunnel = 1:UavHighway.GoNum
            temptunnel= UavHighway.group(m).Highway(gotunnel).ph2;
            UavTeam.Team(m).Uav(k).Waypoint = [UavTeam.Team(m).Uav(k).Waypoint temptunnel];
        end
        % 中间投放点
        UavTeam.Team(m).Uav(k).Waypoint = [UavTeam.Team(m).Uav(k).Waypoint temptou temptou];
        % 后面tunnel
        for backtunnel = 1+UavHighway.GoNum : UavHighway.BackNum+UavHighway.GoNum
            temptunnel= UavHighway.group(m).Highway(backtunnel).ph2;
            UavTeam.Team(m).Uav(k).Waypoint = [UavTeam.Team(m).Uav(k).Waypoint temptunnel];
        end
        % 后面家点
        UavTeam.Team(m).Uav(k).Waypoint = [UavTeam.Team(m).Uav(k).Waypoint temphome temphome temphome];
        % Velocity
        UavTeam.Team(m).Uav(k).Velocity = zeros(2,1);
        % Current task number
        UavTeam.Team(m).Uav(k).CurrentTaskNum = 1;
        % 初始在隐身状态
        UavTeam.Team(m).Uav(k).state = 2;
    end
end
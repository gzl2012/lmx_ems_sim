function [sys,x0,str,ts] = spacemodel(t,x,u,flag)
switch flag,
    case 0,
        [sys,x0,str,ts]=mdlInitializeSizes;
    case 1,
        sys=mdlDerivatives(t,x,u);
    case 3,
        sys=mdlOutputs(t,x,u);
    case {2,4,9}
        sys=[];
    otherwise
        error(['Unhandled flag = ',num2str(flag)]);
end
function [sys,x0,str,ts]=mdlInitializeSizes
global UavTeam
global N
% Initialize multicopter positions
Pint = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 人工设置了读取数据顺序！
% N=1,M=1,X-Y-VX-VY;N=1,M=2,X-Y-VX-VY;N=2,M=1,X-Y-VX-VY;N=2,M=2,X-Y-VX-VY
for lmx = 1:N
    for  k = 1: UavTeam.AvailableNumMax
        Pint = [Pint; UavTeam.Team(lmx).Uav(k).HomePos];
    end
    for  k = 1: UavTeam.AvailableNumMax
        Pint = [Pint; UavTeam.Team(lmx).Uav(k).Velocity];
    end
end
sizes = simsizes;
sizes.NumContStates  = 4*UavTeam.AvailableNumMax*N; % 模块连续状态变量的个数(每个飞机有两个位置两个速度)
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 4*UavTeam.AvailableNumMax*N; % 模块输出变量的个数(每个飞机有两个位置两个速度)
sizes.NumInputs      = 2*UavTeam.AvailableNumMax*N; % 模块输入变量的个数(每个飞机有一个u，包含xy两个分量)
sizes.DirFeedthrough = 0;
sizes.NumSampleTimes = 1;
sys = simsizes(sizes);
x0  = Pint;
str = [];
ts  = [0 0];

function sys = mdlDerivatives(t,x,u)
% dot(p) = v
% dot(v) = -UavTeam.gain*(v-vc)
% x: position
% v: velocity
% u = vc: velocity command
global UavTeam
global N
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 做循环的时候,u需要拆分，不然提示矩阵维度不一致！
M = UavTeam.AvailableNumMax;
for lmx=1:N
    sys(4*M*(lmx-1)+1:4*M*(lmx-1)+2*M) = x(4*M*(lmx-1)+2*M+1:4*M*(lmx-1)+4*M);
    sys(4*M*(lmx-1)+2*M+1:4*M*(lmx-1)+4*M) = -UavTeam.gain*(x(4*M*(lmx-1)+2*M+1:4*M*(lmx-1)+4*M)-u(2*M*(lmx-1)+1:2*M*(lmx-1)+2*M));
end

function sys=mdlOutputs(t,x,u)
sys = x;
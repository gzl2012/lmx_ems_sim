%% 清空了返回1，有飞机返回0
function clear= Home_clear(center)
global UavTeam
global N

M = UavTeam.AvailableNumMax;
clear = 1;
temp = 0;

for nn=1:N
    for k3=1:M
        if UavTeam.Team(nn).Uav(k3).state == 10 || UavTeam.Team(nn).Uav(k3).state == 6 || UavTeam.Team(nn).Uav(k3).state == 4
            ctn = UavTeam.Team(nn).Uav(k3).CurrentTaskNum;
            Pdes  =  UavTeam.Team(nn).Uav(k3).Waypoint(:,ctn);
            if Pdes == center
                clear = 0;
                temp = 1;
            end
        end
    end
    if temp ==1
        break
    end
end
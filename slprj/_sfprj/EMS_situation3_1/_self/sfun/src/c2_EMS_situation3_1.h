#ifndef __c2_EMS_situation3_1_h__
#define __c2_EMS_situation3_1_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc2_EMS_situation3_1InstanceStruct
#define typedef_SFc2_EMS_situation3_1InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c2_sfEvent;
  uint8_T c2_tp_FreeFlight;
  uint8_T c2_tp_Quit;
  uint8_T c2_tp_Standby;
  uint8_T c2_tp_Delivery;
  uint8_T c2_tp_Tunnel;
  uint8_T c2_tp_Landing;
  uint8_T c2_tp_Takeoff;
  boolean_T c2_isStable;
  uint8_T c2_is_active_c2_EMS_situation3_1;
  uint8_T c2_is_c2_EMS_situation3_1;
  uint8_T c2_doSetSimStateSideEffects;
  const mxArray *c2_setSimStateSideEffectsInfo;
} SFc2_EMS_situation3_1InstanceStruct;

#endif                                 /*typedef_SFc2_EMS_situation3_1InstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c2_EMS_situation3_1_get_eml_resolved_functions_info
  (void);

/* Function Definitions */
extern void sf_c2_EMS_situation3_1_get_check_sum(mxArray *plhs[]);
extern void c2_EMS_situation3_1_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

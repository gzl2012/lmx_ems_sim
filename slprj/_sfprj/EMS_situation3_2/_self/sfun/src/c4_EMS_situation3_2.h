#ifndef __c4_EMS_situation3_2_h__
#define __c4_EMS_situation3_2_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc4_EMS_situation3_2InstanceStruct
#define typedef_SFc4_EMS_situation3_2InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c4_sfEvent;
  uint8_T c4_tp_FreeFlight;
  uint8_T c4_tp_Quit;
  uint8_T c4_tp_Standby;
  uint8_T c4_tp_Delivery;
  uint8_T c4_tp_Tunnel;
  uint8_T c4_tp_Landing;
  uint8_T c4_tp_Takeoff;
  boolean_T c4_isStable;
  uint8_T c4_is_active_c4_EMS_situation3_2;
  uint8_T c4_is_c4_EMS_situation3_2;
  uint8_T c4_doSetSimStateSideEffects;
  const mxArray *c4_setSimStateSideEffectsInfo;
} SFc4_EMS_situation3_2InstanceStruct;

#endif                                 /*typedef_SFc4_EMS_situation3_2InstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c4_EMS_situation3_2_get_eml_resolved_functions_info
  (void);

/* Function Definitions */
extern void sf_c4_EMS_situation3_2_get_check_sum(mxArray *plhs[]);
extern void c4_EMS_situation3_2_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif

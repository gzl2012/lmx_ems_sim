#ifndef __c16_EMS_situation3_test2_h__
#define __c16_EMS_situation3_test2_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc16_EMS_situation3_test2InstanceStruct
#define typedef_SFc16_EMS_situation3_test2InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c16_sfEvent;
  uint8_T c16_tp_FreeFlight;
  uint8_T c16_tp_Quit;
  uint8_T c16_tp_Standby;
  uint8_T c16_tp_Delivery;
  uint8_T c16_tp_Tunnel;
  uint8_T c16_tp_Landing;
  uint8_T c16_tp_Takeoff;
  boolean_T c16_isStable;
  uint8_T c16_is_active_c16_EMS_situation3_test2;
  uint8_T c16_is_c16_EMS_situation3_test2;
  uint8_T c16_doSetSimStateSideEffects;
  const mxArray *c16_setSimStateSideEffectsInfo;
} SFc16_EMS_situation3_test2InstanceStruct;

#endif                                 /*typedef_SFc16_EMS_situation3_test2InstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c16_EMS_situation3_test2_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c16_EMS_situation3_test2_get_check_sum(mxArray *plhs[]);
extern void c16_EMS_situation3_test2_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif

/* Include files */

#include <stddef.h>
#include "blas.h"
#include "EMS_situation3_test2_sfun.h"
#include "c7_EMS_situation3_test2.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "EMS_situation3_test2_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c7_debug_family_names[18] = { "nargin", "nargout", "state",
  "j", "M", "G", "go", "quit", "deliver", "land", "load", "free", "wait",
  "wait2free", "wait2tunnel", "takeoff", "i", "g" };

/* Function Declarations */
static void initialize_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance);
static void initialize_params_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance);
static void enable_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance);
static void disable_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance);
static void c7_update_debugger_state_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance);
static void set_sim_state_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance, const mxArray *c7_st);
static void finalize_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance);
static void sf_gateway_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance);
static void initSimStructsc7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c7_machineNumber, uint32_T
  c7_chartNumber, uint32_T c7_instanceNumber);
static const mxArray *c7_sf_marshallOut(void *chartInstanceVoid, void *c7_inData);
static real_T c7_emlrt_marshallIn(SFc7_EMS_situation3_test2InstanceStruct
  *chartInstance, const mxArray *c7_g, const char_T *c7_identifier);
static real_T c7_b_emlrt_marshallIn(SFc7_EMS_situation3_test2InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_b_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static const mxArray *c7_c_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static int32_T c7_c_emlrt_marshallIn(SFc7_EMS_situation3_test2InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static uint8_T c7_d_emlrt_marshallIn(SFc7_EMS_situation3_test2InstanceStruct
  *chartInstance, const mxArray *c7_b_is_active_c7_EMS_situation3_test2, const
  char_T *c7_identifier);
static uint8_T c7_e_emlrt_marshallIn(SFc7_EMS_situation3_test2InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void init_dsm_address_info(SFc7_EMS_situation3_test2InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance)
{
  chartInstance->c7_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c7_is_active_c7_EMS_situation3_test2 = 0U;
}

static void initialize_params_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c7_update_debugger_state_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance)
{
  const mxArray *c7_st;
  const mxArray *c7_y = NULL;
  real_T c7_hoistedGlobal;
  real_T c7_u;
  const mxArray *c7_b_y = NULL;
  real_T c7_b_hoistedGlobal;
  real_T c7_b_u;
  const mxArray *c7_c_y = NULL;
  real_T c7_c_hoistedGlobal;
  real_T c7_c_u;
  const mxArray *c7_d_y = NULL;
  real_T c7_d_hoistedGlobal;
  real_T c7_d_u;
  const mxArray *c7_e_y = NULL;
  real_T c7_e_hoistedGlobal;
  real_T c7_e_u;
  const mxArray *c7_f_y = NULL;
  real_T c7_f_hoistedGlobal;
  real_T c7_f_u;
  const mxArray *c7_g_y = NULL;
  real_T c7_g_hoistedGlobal;
  real_T c7_g_u;
  const mxArray *c7_h_y = NULL;
  real_T c7_h_hoistedGlobal;
  real_T c7_h_u;
  const mxArray *c7_i_y = NULL;
  real_T c7_i_hoistedGlobal;
  real_T c7_i_u;
  const mxArray *c7_j_y = NULL;
  real_T c7_j_hoistedGlobal;
  real_T c7_j_u;
  const mxArray *c7_k_y = NULL;
  real_T c7_k_hoistedGlobal;
  real_T c7_k_u;
  const mxArray *c7_l_y = NULL;
  real_T c7_l_hoistedGlobal;
  real_T c7_l_u;
  const mxArray *c7_m_y = NULL;
  uint8_T c7_m_hoistedGlobal;
  uint8_T c7_m_u;
  const mxArray *c7_n_y = NULL;
  real_T *c7_deliver;
  real_T *c7_free;
  real_T *c7_g;
  real_T *c7_go;
  real_T *c7_i;
  real_T *c7_land;
  real_T *c7_load;
  real_T *c7_quit;
  real_T *c7_takeoff;
  real_T *c7_wait;
  real_T *c7_wait2free;
  real_T *c7_wait2tunnel;
  c7_g = (real_T *)ssGetOutputPortSignal(chartInstance->S, 12);
  c7_i = (real_T *)ssGetOutputPortSignal(chartInstance->S, 11);
  c7_takeoff = (real_T *)ssGetOutputPortSignal(chartInstance->S, 10);
  c7_wait2tunnel = (real_T *)ssGetOutputPortSignal(chartInstance->S, 9);
  c7_wait2free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 8);
  c7_wait = (real_T *)ssGetOutputPortSignal(chartInstance->S, 7);
  c7_free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 6);
  c7_load = (real_T *)ssGetOutputPortSignal(chartInstance->S, 5);
  c7_land = (real_T *)ssGetOutputPortSignal(chartInstance->S, 4);
  c7_deliver = (real_T *)ssGetOutputPortSignal(chartInstance->S, 3);
  c7_quit = (real_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c7_go = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c7_st = NULL;
  c7_st = NULL;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_createcellmatrix(13, 1), false);
  c7_hoistedGlobal = *c7_deliver;
  c7_u = c7_hoistedGlobal;
  c7_b_y = NULL;
  sf_mex_assign(&c7_b_y, sf_mex_create("y", &c7_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 0, c7_b_y);
  c7_b_hoistedGlobal = *c7_free;
  c7_b_u = c7_b_hoistedGlobal;
  c7_c_y = NULL;
  sf_mex_assign(&c7_c_y, sf_mex_create("y", &c7_b_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 1, c7_c_y);
  c7_c_hoistedGlobal = *c7_g;
  c7_c_u = c7_c_hoistedGlobal;
  c7_d_y = NULL;
  sf_mex_assign(&c7_d_y, sf_mex_create("y", &c7_c_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 2, c7_d_y);
  c7_d_hoistedGlobal = *c7_go;
  c7_d_u = c7_d_hoistedGlobal;
  c7_e_y = NULL;
  sf_mex_assign(&c7_e_y, sf_mex_create("y", &c7_d_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 3, c7_e_y);
  c7_e_hoistedGlobal = *c7_i;
  c7_e_u = c7_e_hoistedGlobal;
  c7_f_y = NULL;
  sf_mex_assign(&c7_f_y, sf_mex_create("y", &c7_e_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 4, c7_f_y);
  c7_f_hoistedGlobal = *c7_land;
  c7_f_u = c7_f_hoistedGlobal;
  c7_g_y = NULL;
  sf_mex_assign(&c7_g_y, sf_mex_create("y", &c7_f_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 5, c7_g_y);
  c7_g_hoistedGlobal = *c7_load;
  c7_g_u = c7_g_hoistedGlobal;
  c7_h_y = NULL;
  sf_mex_assign(&c7_h_y, sf_mex_create("y", &c7_g_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 6, c7_h_y);
  c7_h_hoistedGlobal = *c7_quit;
  c7_h_u = c7_h_hoistedGlobal;
  c7_i_y = NULL;
  sf_mex_assign(&c7_i_y, sf_mex_create("y", &c7_h_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 7, c7_i_y);
  c7_i_hoistedGlobal = *c7_takeoff;
  c7_i_u = c7_i_hoistedGlobal;
  c7_j_y = NULL;
  sf_mex_assign(&c7_j_y, sf_mex_create("y", &c7_i_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 8, c7_j_y);
  c7_j_hoistedGlobal = *c7_wait;
  c7_j_u = c7_j_hoistedGlobal;
  c7_k_y = NULL;
  sf_mex_assign(&c7_k_y, sf_mex_create("y", &c7_j_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 9, c7_k_y);
  c7_k_hoistedGlobal = *c7_wait2free;
  c7_k_u = c7_k_hoistedGlobal;
  c7_l_y = NULL;
  sf_mex_assign(&c7_l_y, sf_mex_create("y", &c7_k_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 10, c7_l_y);
  c7_l_hoistedGlobal = *c7_wait2tunnel;
  c7_l_u = c7_l_hoistedGlobal;
  c7_m_y = NULL;
  sf_mex_assign(&c7_m_y, sf_mex_create("y", &c7_l_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 11, c7_m_y);
  c7_m_hoistedGlobal = chartInstance->c7_is_active_c7_EMS_situation3_test2;
  c7_m_u = c7_m_hoistedGlobal;
  c7_n_y = NULL;
  sf_mex_assign(&c7_n_y, sf_mex_create("y", &c7_m_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c7_y, 12, c7_n_y);
  sf_mex_assign(&c7_st, c7_y, false);
  return c7_st;
}

static void set_sim_state_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance, const mxArray *c7_st)
{
  const mxArray *c7_u;
  real_T *c7_deliver;
  real_T *c7_free;
  real_T *c7_g;
  real_T *c7_go;
  real_T *c7_i;
  real_T *c7_land;
  real_T *c7_load;
  real_T *c7_quit;
  real_T *c7_takeoff;
  real_T *c7_wait;
  real_T *c7_wait2free;
  real_T *c7_wait2tunnel;
  c7_g = (real_T *)ssGetOutputPortSignal(chartInstance->S, 12);
  c7_i = (real_T *)ssGetOutputPortSignal(chartInstance->S, 11);
  c7_takeoff = (real_T *)ssGetOutputPortSignal(chartInstance->S, 10);
  c7_wait2tunnel = (real_T *)ssGetOutputPortSignal(chartInstance->S, 9);
  c7_wait2free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 8);
  c7_wait = (real_T *)ssGetOutputPortSignal(chartInstance->S, 7);
  c7_free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 6);
  c7_load = (real_T *)ssGetOutputPortSignal(chartInstance->S, 5);
  c7_land = (real_T *)ssGetOutputPortSignal(chartInstance->S, 4);
  c7_deliver = (real_T *)ssGetOutputPortSignal(chartInstance->S, 3);
  c7_quit = (real_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c7_go = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  chartInstance->c7_doneDoubleBufferReInit = true;
  c7_u = sf_mex_dup(c7_st);
  *c7_deliver = c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c7_u, 0)), "deliver");
  *c7_free = c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c7_u,
    1)), "free");
  *c7_g = c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c7_u, 2)),
    "g");
  *c7_go = c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c7_u, 3)),
    "go");
  *c7_i = c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c7_u, 4)),
    "i");
  *c7_land = c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c7_u,
    5)), "land");
  *c7_load = c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c7_u,
    6)), "load");
  *c7_quit = c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c7_u,
    7)), "quit");
  *c7_takeoff = c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c7_u, 8)), "takeoff");
  *c7_wait = c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c7_u,
    9)), "wait");
  *c7_wait2free = c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c7_u, 10)), "wait2free");
  *c7_wait2tunnel = c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c7_u, 11)), "wait2tunnel");
  chartInstance->c7_is_active_c7_EMS_situation3_test2 = c7_d_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c7_u, 12)),
     "is_active_c7_EMS_situation3_test2");
  sf_mex_destroy(&c7_u);
  c7_update_debugger_state_c7_EMS_situation3_test2(chartInstance);
  sf_mex_destroy(&c7_st);
}

static void finalize_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance)
{
  int32_T c7_i0;
  real_T c7_hoistedGlobal;
  real_T c7_b_hoistedGlobal;
  real_T c7_c_hoistedGlobal;
  int32_T c7_i1;
  real_T c7_state[44];
  real_T c7_j;
  real_T c7_M;
  real_T c7_G;
  uint32_T c7_debug_family_var_map[18];
  real_T c7_nargin = 4.0;
  real_T c7_nargout = 12.0;
  real_T c7_go;
  real_T c7_quit;
  real_T c7_deliver;
  real_T c7_land;
  real_T c7_load;
  real_T c7_free;
  real_T c7_wait;
  real_T c7_wait2free;
  real_T c7_wait2tunnel;
  real_T c7_takeoff;
  real_T c7_i;
  real_T c7_g;
  real_T *c7_b_go;
  real_T *c7_b_j;
  real_T *c7_b_quit;
  real_T *c7_b_deliver;
  real_T *c7_b_land;
  real_T *c7_b_load;
  real_T *c7_b_free;
  real_T *c7_b_wait;
  real_T *c7_b_wait2free;
  real_T *c7_b_wait2tunnel;
  real_T *c7_b_takeoff;
  real_T *c7_b_i;
  real_T *c7_b_M;
  real_T *c7_b_G;
  real_T *c7_b_g;
  real_T (*c7_b_state)[44];
  c7_b_g = (real_T *)ssGetOutputPortSignal(chartInstance->S, 12);
  c7_b_G = (real_T *)ssGetInputPortSignal(chartInstance->S, 3);
  c7_b_M = (real_T *)ssGetInputPortSignal(chartInstance->S, 2);
  c7_b_i = (real_T *)ssGetOutputPortSignal(chartInstance->S, 11);
  c7_b_takeoff = (real_T *)ssGetOutputPortSignal(chartInstance->S, 10);
  c7_b_wait2tunnel = (real_T *)ssGetOutputPortSignal(chartInstance->S, 9);
  c7_b_wait2free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 8);
  c7_b_wait = (real_T *)ssGetOutputPortSignal(chartInstance->S, 7);
  c7_b_free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 6);
  c7_b_load = (real_T *)ssGetOutputPortSignal(chartInstance->S, 5);
  c7_b_land = (real_T *)ssGetOutputPortSignal(chartInstance->S, 4);
  c7_b_deliver = (real_T *)ssGetOutputPortSignal(chartInstance->S, 3);
  c7_b_quit = (real_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c7_b_j = (real_T *)ssGetInputPortSignal(chartInstance->S, 1);
  c7_b_go = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c7_b_state = (real_T (*)[44])ssGetInputPortSignal(chartInstance->S, 0);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 6U, chartInstance->c7_sfEvent);
  for (c7_i0 = 0; c7_i0 < 44; c7_i0++) {
    _SFD_DATA_RANGE_CHECK((*c7_b_state)[c7_i0], 0U);
  }

  chartInstance->c7_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 6U, chartInstance->c7_sfEvent);
  c7_hoistedGlobal = *c7_b_j;
  c7_b_hoistedGlobal = *c7_b_M;
  c7_c_hoistedGlobal = *c7_b_G;
  for (c7_i1 = 0; c7_i1 < 44; c7_i1++) {
    c7_state[c7_i1] = (*c7_b_state)[c7_i1];
  }

  c7_j = c7_hoistedGlobal;
  c7_M = c7_b_hoistedGlobal;
  c7_G = c7_c_hoistedGlobal;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 18U, 18U, c7_debug_family_names,
    c7_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargin, 0U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargout, 1U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c7_state, 2U, c7_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c7_j, 3U, c7_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c7_M, 4U, c7_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c7_G, 5U, c7_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_go, 6U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_quit, 7U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_deliver, 8U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_land, 9U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_load, 10U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_free, 11U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_wait, 12U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_wait2free, 13U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_wait2tunnel, 14U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_takeoff, 15U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_i, 16U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_g, 17U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 3);
  c7_go = c7_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j", c7_M * 11.0 * (c7_G - 1.0) + c7_j), 1, 44,
    1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 4);
  c7_quit = c7_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+1*M", (c7_M * 11.0 * (c7_G - 1.0) + c7_j) +
                       c7_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 5);
  c7_deliver = c7_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+2*M", (c7_M * 11.0 * (c7_G - 1.0) + c7_j) +
                       2.0 * c7_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 6);
  c7_land = c7_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+3*M", (c7_M * 11.0 * (c7_G - 1.0) + c7_j) +
                       3.0 * c7_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 7);
  c7_load = c7_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+4*M", (c7_M * 11.0 * (c7_G - 1.0) + c7_j) +
                       4.0 * c7_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 8);
  c7_free = c7_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+5*M", (c7_M * 11.0 * (c7_G - 1.0) + c7_j) +
                       5.0 * c7_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 9);
  c7_wait = c7_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+6*M", (c7_M * 11.0 * (c7_G - 1.0) + c7_j) +
                       6.0 * c7_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 10);
  c7_wait2free = c7_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+7*M", (c7_M * 11.0 * (c7_G - 1.0) + c7_j) +
                       7.0 * c7_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 11);
  c7_wait2tunnel = c7_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+8*M", (c7_M * 11.0 * (c7_G - 1.0) + c7_j) +
                       8.0 * c7_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 12);
  c7_takeoff = c7_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+9*M", (c7_M * 11.0 * (c7_G - 1.0) + c7_j) +
                       9.0 * c7_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 13);
  c7_i = c7_j;
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 14);
  c7_g = c7_G;
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, -14);
  _SFD_SYMBOL_SCOPE_POP();
  *c7_b_go = c7_go;
  *c7_b_quit = c7_quit;
  *c7_b_deliver = c7_deliver;
  *c7_b_land = c7_land;
  *c7_b_load = c7_load;
  *c7_b_free = c7_free;
  *c7_b_wait = c7_wait;
  *c7_b_wait2free = c7_wait2free;
  *c7_b_wait2tunnel = c7_wait2tunnel;
  *c7_b_takeoff = c7_takeoff;
  *c7_b_i = c7_i;
  *c7_b_g = c7_g;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c7_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_EMS_situation3_test2MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  _SFD_DATA_RANGE_CHECK(*c7_b_go, 1U);
  _SFD_DATA_RANGE_CHECK(*c7_b_j, 2U);
  _SFD_DATA_RANGE_CHECK(*c7_b_quit, 3U);
  _SFD_DATA_RANGE_CHECK(*c7_b_deliver, 4U);
  _SFD_DATA_RANGE_CHECK(*c7_b_land, 5U);
  _SFD_DATA_RANGE_CHECK(*c7_b_load, 6U);
  _SFD_DATA_RANGE_CHECK(*c7_b_free, 7U);
  _SFD_DATA_RANGE_CHECK(*c7_b_wait, 8U);
  _SFD_DATA_RANGE_CHECK(*c7_b_wait2free, 9U);
  _SFD_DATA_RANGE_CHECK(*c7_b_wait2tunnel, 10U);
  _SFD_DATA_RANGE_CHECK(*c7_b_takeoff, 11U);
  _SFD_DATA_RANGE_CHECK(*c7_b_i, 12U);
  _SFD_DATA_RANGE_CHECK(*c7_b_M, 13U);
  _SFD_DATA_RANGE_CHECK(*c7_b_G, 14U);
  _SFD_DATA_RANGE_CHECK(*c7_b_g, 15U);
}

static void initSimStructsc7_EMS_situation3_test2
  (SFc7_EMS_situation3_test2InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c7_machineNumber, uint32_T
  c7_chartNumber, uint32_T c7_instanceNumber)
{
  (void)c7_machineNumber;
  (void)c7_chartNumber;
  (void)c7_instanceNumber;
}

static const mxArray *c7_sf_marshallOut(void *chartInstanceVoid, void *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  real_T c7_u;
  const mxArray *c7_y = NULL;
  SFc7_EMS_situation3_test2InstanceStruct *chartInstance;
  chartInstance = (SFc7_EMS_situation3_test2InstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_u = *(real_T *)c7_inData;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static real_T c7_emlrt_marshallIn(SFc7_EMS_situation3_test2InstanceStruct
  *chartInstance, const mxArray *c7_g, const char_T *c7_identifier)
{
  real_T c7_y;
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_g), &c7_thisId);
  sf_mex_destroy(&c7_g);
  return c7_y;
}

static real_T c7_b_emlrt_marshallIn(SFc7_EMS_situation3_test2InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  real_T c7_y;
  real_T c7_d0;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_d0, 1, 0, 0U, 0, 0U, 0);
  c7_y = c7_d0;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_g;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  real_T c7_y;
  SFc7_EMS_situation3_test2InstanceStruct *chartInstance;
  chartInstance = (SFc7_EMS_situation3_test2InstanceStruct *)chartInstanceVoid;
  c7_g = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_g), &c7_thisId);
  sf_mex_destroy(&c7_g);
  *(real_T *)c7_outData = c7_y;
  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_b_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  int32_T c7_i2;
  real_T c7_b_inData[44];
  int32_T c7_i3;
  real_T c7_u[44];
  const mxArray *c7_y = NULL;
  SFc7_EMS_situation3_test2InstanceStruct *chartInstance;
  chartInstance = (SFc7_EMS_situation3_test2InstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  for (c7_i2 = 0; c7_i2 < 44; c7_i2++) {
    c7_b_inData[c7_i2] = (*(real_T (*)[44])c7_inData)[c7_i2];
  }

  for (c7_i3 = 0; c7_i3 < 44; c7_i3++) {
    c7_u[c7_i3] = c7_b_inData[c7_i3];
  }

  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", c7_u, 0, 0U, 1U, 0U, 1, 44), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

const mxArray *sf_c7_EMS_situation3_test2_get_eml_resolved_functions_info(void)
{
  const mxArray *c7_nameCaptureInfo = NULL;
  c7_nameCaptureInfo = NULL;
  sf_mex_assign(&c7_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c7_nameCaptureInfo;
}

static const mxArray *c7_c_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  int32_T c7_u;
  const mxArray *c7_y = NULL;
  SFc7_EMS_situation3_test2InstanceStruct *chartInstance;
  chartInstance = (SFc7_EMS_situation3_test2InstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_u = *(int32_T *)c7_inData;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static int32_T c7_c_emlrt_marshallIn(SFc7_EMS_situation3_test2InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  int32_T c7_y;
  int32_T c7_i4;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_i4, 1, 6, 0U, 0, 0U, 0);
  c7_y = c7_i4;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_b_sfEvent;
  const char_T *c7_identifier;
  emlrtMsgIdentifier c7_thisId;
  int32_T c7_y;
  SFc7_EMS_situation3_test2InstanceStruct *chartInstance;
  chartInstance = (SFc7_EMS_situation3_test2InstanceStruct *)chartInstanceVoid;
  c7_b_sfEvent = sf_mex_dup(c7_mxArrayInData);
  c7_identifier = c7_varName;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_sfEvent),
    &c7_thisId);
  sf_mex_destroy(&c7_b_sfEvent);
  *(int32_T *)c7_outData = c7_y;
  sf_mex_destroy(&c7_mxArrayInData);
}

static uint8_T c7_d_emlrt_marshallIn(SFc7_EMS_situation3_test2InstanceStruct
  *chartInstance, const mxArray *c7_b_is_active_c7_EMS_situation3_test2, const
  char_T *c7_identifier)
{
  uint8_T c7_y;
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c7_b_is_active_c7_EMS_situation3_test2), &c7_thisId);
  sf_mex_destroy(&c7_b_is_active_c7_EMS_situation3_test2);
  return c7_y;
}

static uint8_T c7_e_emlrt_marshallIn(SFc7_EMS_situation3_test2InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  uint8_T c7_y;
  uint8_T c7_u0;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_u0, 1, 3, 0U, 0, 0U, 0);
  c7_y = c7_u0;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void init_dsm_address_info(SFc7_EMS_situation3_test2InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c7_EMS_situation3_test2_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3396517901U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(81883110U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(257939917U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(383693349U);
}

mxArray *sf_c7_EMS_situation3_test2_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1,1,5,
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("XCrGfUXNuRGJ6YEGhgrnzF");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,4,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(44);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,12,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,5,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,5,"type",mxType);
    }

    mxSetField(mxData,5,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,6,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,6,"type",mxType);
    }

    mxSetField(mxData,6,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,7,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,7,"type",mxType);
    }

    mxSetField(mxData,7,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,8,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,8,"type",mxType);
    }

    mxSetField(mxData,8,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,9,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,9,"type",mxType);
    }

    mxSetField(mxData,9,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,10,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,10,"type",mxType);
    }

    mxSetField(mxData,10,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,11,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,11,"type",mxType);
    }

    mxSetField(mxData,11,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c7_EMS_situation3_test2_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c7_EMS_situation3_test2_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c7_EMS_situation3_test2(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x10'type','srcId','name','auxInfo'{{M[1],M[9],T\"deliver\",},{M[1],M[12],T\"free\",},{M[1],M[21],T\"g\",},{M[1],M[5],T\"go\",},{M[1],M[6],T\"i\",},{M[1],M[10],T\"land\",},{M[1],M[11],T\"load\",},{M[1],M[8],T\"quit\",},{M[1],M[17],T\"takeoff\",},{M[1],M[14],T\"wait\",}}",
    "100 S1x3'type','srcId','name','auxInfo'{{M[1],M[15],T\"wait2free\",},{M[1],M[16],T\"wait2tunnel\",},{M[8],M[0],T\"is_active_c7_EMS_situation3_test2\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 13, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c7_EMS_situation3_test2_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc7_EMS_situation3_test2InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc7_EMS_situation3_test2InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _EMS_situation3_test2MachineNumber_,
           7,
           1,
           1,
           0,
           16,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize ist own list of scripts */
        init_script_number_translation(_EMS_situation3_test2MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_EMS_situation3_test2MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _EMS_situation3_test2MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"state");
          _SFD_SET_DATA_PROPS(1,2,0,1,"go");
          _SFD_SET_DATA_PROPS(2,1,1,0,"j");
          _SFD_SET_DATA_PROPS(3,2,0,1,"quit");
          _SFD_SET_DATA_PROPS(4,2,0,1,"deliver");
          _SFD_SET_DATA_PROPS(5,2,0,1,"land");
          _SFD_SET_DATA_PROPS(6,2,0,1,"load");
          _SFD_SET_DATA_PROPS(7,2,0,1,"free");
          _SFD_SET_DATA_PROPS(8,2,0,1,"wait");
          _SFD_SET_DATA_PROPS(9,2,0,1,"wait2free");
          _SFD_SET_DATA_PROPS(10,2,0,1,"wait2tunnel");
          _SFD_SET_DATA_PROPS(11,2,0,1,"takeoff");
          _SFD_SET_DATA_PROPS(12,2,0,1,"i");
          _SFD_SET_DATA_PROPS(13,1,1,0,"M");
          _SFD_SET_DATA_PROPS(14,1,1,0,"G");
          _SFD_SET_DATA_PROPS(15,2,0,1,"g");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,440);

        {
          unsigned int dimVector[1];
          dimVector[0]= 44;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c7_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(9,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(10,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(11,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(12,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(13,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(14,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(15,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);

        {
          real_T *c7_go;
          real_T *c7_j;
          real_T *c7_quit;
          real_T *c7_deliver;
          real_T *c7_land;
          real_T *c7_load;
          real_T *c7_free;
          real_T *c7_wait;
          real_T *c7_wait2free;
          real_T *c7_wait2tunnel;
          real_T *c7_takeoff;
          real_T *c7_i;
          real_T *c7_M;
          real_T *c7_G;
          real_T *c7_g;
          real_T (*c7_state)[44];
          c7_g = (real_T *)ssGetOutputPortSignal(chartInstance->S, 12);
          c7_G = (real_T *)ssGetInputPortSignal(chartInstance->S, 3);
          c7_M = (real_T *)ssGetInputPortSignal(chartInstance->S, 2);
          c7_i = (real_T *)ssGetOutputPortSignal(chartInstance->S, 11);
          c7_takeoff = (real_T *)ssGetOutputPortSignal(chartInstance->S, 10);
          c7_wait2tunnel = (real_T *)ssGetOutputPortSignal(chartInstance->S, 9);
          c7_wait2free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 8);
          c7_wait = (real_T *)ssGetOutputPortSignal(chartInstance->S, 7);
          c7_free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 6);
          c7_load = (real_T *)ssGetOutputPortSignal(chartInstance->S, 5);
          c7_land = (real_T *)ssGetOutputPortSignal(chartInstance->S, 4);
          c7_deliver = (real_T *)ssGetOutputPortSignal(chartInstance->S, 3);
          c7_quit = (real_T *)ssGetOutputPortSignal(chartInstance->S, 2);
          c7_j = (real_T *)ssGetInputPortSignal(chartInstance->S, 1);
          c7_go = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
          c7_state = (real_T (*)[44])ssGetInputPortSignal(chartInstance->S, 0);
          _SFD_SET_DATA_VALUE_PTR(0U, *c7_state);
          _SFD_SET_DATA_VALUE_PTR(1U, c7_go);
          _SFD_SET_DATA_VALUE_PTR(2U, c7_j);
          _SFD_SET_DATA_VALUE_PTR(3U, c7_quit);
          _SFD_SET_DATA_VALUE_PTR(4U, c7_deliver);
          _SFD_SET_DATA_VALUE_PTR(5U, c7_land);
          _SFD_SET_DATA_VALUE_PTR(6U, c7_load);
          _SFD_SET_DATA_VALUE_PTR(7U, c7_free);
          _SFD_SET_DATA_VALUE_PTR(8U, c7_wait);
          _SFD_SET_DATA_VALUE_PTR(9U, c7_wait2free);
          _SFD_SET_DATA_VALUE_PTR(10U, c7_wait2tunnel);
          _SFD_SET_DATA_VALUE_PTR(11U, c7_takeoff);
          _SFD_SET_DATA_VALUE_PTR(12U, c7_i);
          _SFD_SET_DATA_VALUE_PTR(13U, c7_M);
          _SFD_SET_DATA_VALUE_PTR(14U, c7_G);
          _SFD_SET_DATA_VALUE_PTR(15U, c7_g);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _EMS_situation3_test2MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "w8FifxirnEw5wMRT7CP4HE";
}

static void sf_opaque_initialize_c7_EMS_situation3_test2(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc7_EMS_situation3_test2InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c7_EMS_situation3_test2
    ((SFc7_EMS_situation3_test2InstanceStruct*) chartInstanceVar);
  initialize_c7_EMS_situation3_test2((SFc7_EMS_situation3_test2InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c7_EMS_situation3_test2(void *chartInstanceVar)
{
  enable_c7_EMS_situation3_test2((SFc7_EMS_situation3_test2InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c7_EMS_situation3_test2(void *chartInstanceVar)
{
  disable_c7_EMS_situation3_test2((SFc7_EMS_situation3_test2InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c7_EMS_situation3_test2(void *chartInstanceVar)
{
  sf_gateway_c7_EMS_situation3_test2((SFc7_EMS_situation3_test2InstanceStruct*)
    chartInstanceVar);
}

extern const mxArray* sf_internal_get_sim_state_c7_EMS_situation3_test2
  (SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[4];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_raw2high");
  prhs[1] = mxCreateDoubleScalar(ssGetSFuncBlockHandle(S));
  prhs[2] = (mxArray*) get_sim_state_c7_EMS_situation3_test2
    ((SFc7_EMS_situation3_test2InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
  prhs[3] = (mxArray*) sf_get_sim_state_info_c7_EMS_situation3_test2();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 4, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  mxDestroyArray(prhs[3]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_raw2high'.\n");
  }

  return plhs[0];
}

extern void sf_internal_set_sim_state_c7_EMS_situation3_test2(SimStruct* S,
  const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[3];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_high2raw");
  prhs[1] = mxDuplicateArray(st);      /* high level simctx */
  prhs[2] = (mxArray*) sf_get_sim_state_info_c7_EMS_situation3_test2();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 3, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_high2raw'.\n");
  }

  set_sim_state_c7_EMS_situation3_test2((SFc7_EMS_situation3_test2InstanceStruct*)
    chartInfo->chartInstance, mxDuplicateArray(plhs[0]));
  mxDestroyArray(plhs[0]);
}

static const mxArray* sf_opaque_get_sim_state_c7_EMS_situation3_test2(SimStruct*
  S)
{
  return sf_internal_get_sim_state_c7_EMS_situation3_test2(S);
}

static void sf_opaque_set_sim_state_c7_EMS_situation3_test2(SimStruct* S, const
  mxArray *st)
{
  sf_internal_set_sim_state_c7_EMS_situation3_test2(S, st);
}

static void sf_opaque_terminate_c7_EMS_situation3_test2(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc7_EMS_situation3_test2InstanceStruct*) chartInstanceVar
      )->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_EMS_situation3_test2_optimization_info();
    }

    finalize_c7_EMS_situation3_test2((SFc7_EMS_situation3_test2InstanceStruct*)
      chartInstanceVar);
    utFree((void *)chartInstanceVar);
    if (crtInfo != NULL) {
      utFree((void *)crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc7_EMS_situation3_test2((SFc7_EMS_situation3_test2InstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c7_EMS_situation3_test2(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c7_EMS_situation3_test2
      ((SFc7_EMS_situation3_test2InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c7_EMS_situation3_test2(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_EMS_situation3_test2_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,7);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,7,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,7,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,7);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,7,4);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,7,12);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=12; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 4; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,7);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(1551012474U));
  ssSetChecksum1(S,(116798206U));
  ssSetChecksum2(S,(2855444837U));
  ssSetChecksum3(S,(3942075024U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c7_EMS_situation3_test2(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c7_EMS_situation3_test2(SimStruct *S)
{
  SFc7_EMS_situation3_test2InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc7_EMS_situation3_test2InstanceStruct *)utMalloc(sizeof
    (SFc7_EMS_situation3_test2InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc7_EMS_situation3_test2InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c7_EMS_situation3_test2;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c7_EMS_situation3_test2;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c7_EMS_situation3_test2;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c7_EMS_situation3_test2;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c7_EMS_situation3_test2;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c7_EMS_situation3_test2;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c7_EMS_situation3_test2;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c7_EMS_situation3_test2;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c7_EMS_situation3_test2;
  chartInstance->chartInfo.mdlStart = mdlStart_c7_EMS_situation3_test2;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c7_EMS_situation3_test2;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c7_EMS_situation3_test2_method_dispatcher(SimStruct *S, int_T method, void *
  data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c7_EMS_situation3_test2(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c7_EMS_situation3_test2(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c7_EMS_situation3_test2(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c7_EMS_situation3_test2_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

/* Include files */

#include <stddef.h>
#include "blas.h"
#include "EMS_situation22_test2_sfun.h"
#include "c21_EMS_situation22_test2.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "EMS_situation22_test2_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c21_debug_family_names[18] = { "nargin", "nargout", "state",
  "j", "M", "G", "go", "quit", "deliver", "land", "load", "free", "wait",
  "wait2free", "wait2tunnel", "takeoff", "i", "g" };

/* Function Declarations */
static void initialize_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance);
static void initialize_params_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance);
static void enable_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance);
static void disable_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance);
static void c21_update_debugger_state_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance);
static void set_sim_state_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance, const mxArray
   *c21_st);
static void finalize_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance);
static void sf_gateway_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance);
static void initSimStructsc21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c21_machineNumber, uint32_T
  c21_chartNumber, uint32_T c21_instanceNumber);
static const mxArray *c21_sf_marshallOut(void *chartInstanceVoid, void
  *c21_inData);
static real_T c21_emlrt_marshallIn(SFc21_EMS_situation22_test2InstanceStruct
  *chartInstance, const mxArray *c21_g, const char_T *c21_identifier);
static real_T c21_b_emlrt_marshallIn(SFc21_EMS_situation22_test2InstanceStruct
  *chartInstance, const mxArray *c21_u, const emlrtMsgIdentifier *c21_parentId);
static void c21_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c21_mxArrayInData, const char_T *c21_varName, void *c21_outData);
static const mxArray *c21_b_sf_marshallOut(void *chartInstanceVoid, void
  *c21_inData);
static const mxArray *c21_c_sf_marshallOut(void *chartInstanceVoid, void
  *c21_inData);
static int32_T c21_c_emlrt_marshallIn(SFc21_EMS_situation22_test2InstanceStruct *
  chartInstance, const mxArray *c21_u, const emlrtMsgIdentifier *c21_parentId);
static void c21_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c21_mxArrayInData, const char_T *c21_varName, void *c21_outData);
static uint8_T c21_d_emlrt_marshallIn(SFc21_EMS_situation22_test2InstanceStruct *
  chartInstance, const mxArray *c21_b_is_active_c21_EMS_situation22_test2, const
  char_T *c21_identifier);
static uint8_T c21_e_emlrt_marshallIn(SFc21_EMS_situation22_test2InstanceStruct *
  chartInstance, const mxArray *c21_u, const emlrtMsgIdentifier *c21_parentId);
static void init_dsm_address_info(SFc21_EMS_situation22_test2InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance)
{
  chartInstance->c21_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c21_is_active_c21_EMS_situation22_test2 = 0U;
}

static void initialize_params_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c21_update_debugger_state_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance)
{
  const mxArray *c21_st;
  const mxArray *c21_y = NULL;
  real_T c21_hoistedGlobal;
  real_T c21_u;
  const mxArray *c21_b_y = NULL;
  real_T c21_b_hoistedGlobal;
  real_T c21_b_u;
  const mxArray *c21_c_y = NULL;
  real_T c21_c_hoistedGlobal;
  real_T c21_c_u;
  const mxArray *c21_d_y = NULL;
  real_T c21_d_hoistedGlobal;
  real_T c21_d_u;
  const mxArray *c21_e_y = NULL;
  real_T c21_e_hoistedGlobal;
  real_T c21_e_u;
  const mxArray *c21_f_y = NULL;
  real_T c21_f_hoistedGlobal;
  real_T c21_f_u;
  const mxArray *c21_g_y = NULL;
  real_T c21_g_hoistedGlobal;
  real_T c21_g_u;
  const mxArray *c21_h_y = NULL;
  real_T c21_h_hoistedGlobal;
  real_T c21_h_u;
  const mxArray *c21_i_y = NULL;
  real_T c21_i_hoistedGlobal;
  real_T c21_i_u;
  const mxArray *c21_j_y = NULL;
  real_T c21_j_hoistedGlobal;
  real_T c21_j_u;
  const mxArray *c21_k_y = NULL;
  real_T c21_k_hoistedGlobal;
  real_T c21_k_u;
  const mxArray *c21_l_y = NULL;
  real_T c21_l_hoistedGlobal;
  real_T c21_l_u;
  const mxArray *c21_m_y = NULL;
  uint8_T c21_m_hoistedGlobal;
  uint8_T c21_m_u;
  const mxArray *c21_n_y = NULL;
  real_T *c21_deliver;
  real_T *c21_free;
  real_T *c21_g;
  real_T *c21_go;
  real_T *c21_i;
  real_T *c21_land;
  real_T *c21_load;
  real_T *c21_quit;
  real_T *c21_takeoff;
  real_T *c21_wait;
  real_T *c21_wait2free;
  real_T *c21_wait2tunnel;
  c21_g = (real_T *)ssGetOutputPortSignal(chartInstance->S, 12);
  c21_i = (real_T *)ssGetOutputPortSignal(chartInstance->S, 11);
  c21_takeoff = (real_T *)ssGetOutputPortSignal(chartInstance->S, 10);
  c21_wait2tunnel = (real_T *)ssGetOutputPortSignal(chartInstance->S, 9);
  c21_wait2free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 8);
  c21_wait = (real_T *)ssGetOutputPortSignal(chartInstance->S, 7);
  c21_free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 6);
  c21_load = (real_T *)ssGetOutputPortSignal(chartInstance->S, 5);
  c21_land = (real_T *)ssGetOutputPortSignal(chartInstance->S, 4);
  c21_deliver = (real_T *)ssGetOutputPortSignal(chartInstance->S, 3);
  c21_quit = (real_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c21_go = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c21_st = NULL;
  c21_st = NULL;
  c21_y = NULL;
  sf_mex_assign(&c21_y, sf_mex_createcellmatrix(13, 1), false);
  c21_hoistedGlobal = *c21_deliver;
  c21_u = c21_hoistedGlobal;
  c21_b_y = NULL;
  sf_mex_assign(&c21_b_y, sf_mex_create("y", &c21_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c21_y, 0, c21_b_y);
  c21_b_hoistedGlobal = *c21_free;
  c21_b_u = c21_b_hoistedGlobal;
  c21_c_y = NULL;
  sf_mex_assign(&c21_c_y, sf_mex_create("y", &c21_b_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c21_y, 1, c21_c_y);
  c21_c_hoistedGlobal = *c21_g;
  c21_c_u = c21_c_hoistedGlobal;
  c21_d_y = NULL;
  sf_mex_assign(&c21_d_y, sf_mex_create("y", &c21_c_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c21_y, 2, c21_d_y);
  c21_d_hoistedGlobal = *c21_go;
  c21_d_u = c21_d_hoistedGlobal;
  c21_e_y = NULL;
  sf_mex_assign(&c21_e_y, sf_mex_create("y", &c21_d_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c21_y, 3, c21_e_y);
  c21_e_hoistedGlobal = *c21_i;
  c21_e_u = c21_e_hoistedGlobal;
  c21_f_y = NULL;
  sf_mex_assign(&c21_f_y, sf_mex_create("y", &c21_e_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c21_y, 4, c21_f_y);
  c21_f_hoistedGlobal = *c21_land;
  c21_f_u = c21_f_hoistedGlobal;
  c21_g_y = NULL;
  sf_mex_assign(&c21_g_y, sf_mex_create("y", &c21_f_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c21_y, 5, c21_g_y);
  c21_g_hoistedGlobal = *c21_load;
  c21_g_u = c21_g_hoistedGlobal;
  c21_h_y = NULL;
  sf_mex_assign(&c21_h_y, sf_mex_create("y", &c21_g_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c21_y, 6, c21_h_y);
  c21_h_hoistedGlobal = *c21_quit;
  c21_h_u = c21_h_hoistedGlobal;
  c21_i_y = NULL;
  sf_mex_assign(&c21_i_y, sf_mex_create("y", &c21_h_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c21_y, 7, c21_i_y);
  c21_i_hoistedGlobal = *c21_takeoff;
  c21_i_u = c21_i_hoistedGlobal;
  c21_j_y = NULL;
  sf_mex_assign(&c21_j_y, sf_mex_create("y", &c21_i_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c21_y, 8, c21_j_y);
  c21_j_hoistedGlobal = *c21_wait;
  c21_j_u = c21_j_hoistedGlobal;
  c21_k_y = NULL;
  sf_mex_assign(&c21_k_y, sf_mex_create("y", &c21_j_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c21_y, 9, c21_k_y);
  c21_k_hoistedGlobal = *c21_wait2free;
  c21_k_u = c21_k_hoistedGlobal;
  c21_l_y = NULL;
  sf_mex_assign(&c21_l_y, sf_mex_create("y", &c21_k_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c21_y, 10, c21_l_y);
  c21_l_hoistedGlobal = *c21_wait2tunnel;
  c21_l_u = c21_l_hoistedGlobal;
  c21_m_y = NULL;
  sf_mex_assign(&c21_m_y, sf_mex_create("y", &c21_l_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c21_y, 11, c21_m_y);
  c21_m_hoistedGlobal = chartInstance->c21_is_active_c21_EMS_situation22_test2;
  c21_m_u = c21_m_hoistedGlobal;
  c21_n_y = NULL;
  sf_mex_assign(&c21_n_y, sf_mex_create("y", &c21_m_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c21_y, 12, c21_n_y);
  sf_mex_assign(&c21_st, c21_y, false);
  return c21_st;
}

static void set_sim_state_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance, const mxArray
   *c21_st)
{
  const mxArray *c21_u;
  real_T *c21_deliver;
  real_T *c21_free;
  real_T *c21_g;
  real_T *c21_go;
  real_T *c21_i;
  real_T *c21_land;
  real_T *c21_load;
  real_T *c21_quit;
  real_T *c21_takeoff;
  real_T *c21_wait;
  real_T *c21_wait2free;
  real_T *c21_wait2tunnel;
  c21_g = (real_T *)ssGetOutputPortSignal(chartInstance->S, 12);
  c21_i = (real_T *)ssGetOutputPortSignal(chartInstance->S, 11);
  c21_takeoff = (real_T *)ssGetOutputPortSignal(chartInstance->S, 10);
  c21_wait2tunnel = (real_T *)ssGetOutputPortSignal(chartInstance->S, 9);
  c21_wait2free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 8);
  c21_wait = (real_T *)ssGetOutputPortSignal(chartInstance->S, 7);
  c21_free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 6);
  c21_load = (real_T *)ssGetOutputPortSignal(chartInstance->S, 5);
  c21_land = (real_T *)ssGetOutputPortSignal(chartInstance->S, 4);
  c21_deliver = (real_T *)ssGetOutputPortSignal(chartInstance->S, 3);
  c21_quit = (real_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c21_go = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  chartInstance->c21_doneDoubleBufferReInit = true;
  c21_u = sf_mex_dup(c21_st);
  *c21_deliver = c21_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c21_u, 0)), "deliver");
  *c21_free = c21_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c21_u, 1)), "free");
  *c21_g = c21_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c21_u,
    2)), "g");
  *c21_go = c21_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c21_u,
    3)), "go");
  *c21_i = c21_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c21_u,
    4)), "i");
  *c21_land = c21_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c21_u, 5)), "land");
  *c21_load = c21_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c21_u, 6)), "load");
  *c21_quit = c21_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c21_u, 7)), "quit");
  *c21_takeoff = c21_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c21_u, 8)), "takeoff");
  *c21_wait = c21_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c21_u, 9)), "wait");
  *c21_wait2free = c21_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c21_u, 10)), "wait2free");
  *c21_wait2tunnel = c21_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c21_u, 11)), "wait2tunnel");
  chartInstance->c21_is_active_c21_EMS_situation22_test2 =
    c21_d_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c21_u, 12)),
    "is_active_c21_EMS_situation22_test2");
  sf_mex_destroy(&c21_u);
  c21_update_debugger_state_c21_EMS_situation22_test2(chartInstance);
  sf_mex_destroy(&c21_st);
}

static void finalize_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance)
{
  int32_T c21_i0;
  real_T c21_hoistedGlobal;
  real_T c21_b_hoistedGlobal;
  real_T c21_c_hoistedGlobal;
  int32_T c21_i1;
  real_T c21_state[44];
  real_T c21_j;
  real_T c21_M;
  real_T c21_G;
  uint32_T c21_debug_family_var_map[18];
  real_T c21_nargin = 4.0;
  real_T c21_nargout = 12.0;
  real_T c21_go;
  real_T c21_quit;
  real_T c21_deliver;
  real_T c21_land;
  real_T c21_load;
  real_T c21_free;
  real_T c21_wait;
  real_T c21_wait2free;
  real_T c21_wait2tunnel;
  real_T c21_takeoff;
  real_T c21_i;
  real_T c21_g;
  real_T *c21_b_go;
  real_T *c21_b_j;
  real_T *c21_b_quit;
  real_T *c21_b_deliver;
  real_T *c21_b_land;
  real_T *c21_b_load;
  real_T *c21_b_free;
  real_T *c21_b_wait;
  real_T *c21_b_wait2free;
  real_T *c21_b_wait2tunnel;
  real_T *c21_b_takeoff;
  real_T *c21_b_i;
  real_T *c21_b_M;
  real_T *c21_b_g;
  real_T *c21_b_G;
  real_T (*c21_b_state)[44];
  c21_b_G = (real_T *)ssGetInputPortSignal(chartInstance->S, 3);
  c21_b_g = (real_T *)ssGetOutputPortSignal(chartInstance->S, 12);
  c21_b_M = (real_T *)ssGetInputPortSignal(chartInstance->S, 2);
  c21_b_i = (real_T *)ssGetOutputPortSignal(chartInstance->S, 11);
  c21_b_takeoff = (real_T *)ssGetOutputPortSignal(chartInstance->S, 10);
  c21_b_wait2tunnel = (real_T *)ssGetOutputPortSignal(chartInstance->S, 9);
  c21_b_wait2free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 8);
  c21_b_wait = (real_T *)ssGetOutputPortSignal(chartInstance->S, 7);
  c21_b_free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 6);
  c21_b_load = (real_T *)ssGetOutputPortSignal(chartInstance->S, 5);
  c21_b_land = (real_T *)ssGetOutputPortSignal(chartInstance->S, 4);
  c21_b_deliver = (real_T *)ssGetOutputPortSignal(chartInstance->S, 3);
  c21_b_quit = (real_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c21_b_j = (real_T *)ssGetInputPortSignal(chartInstance->S, 1);
  c21_b_go = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c21_b_state = (real_T (*)[44])ssGetInputPortSignal(chartInstance->S, 0);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 8U, chartInstance->c21_sfEvent);
  for (c21_i0 = 0; c21_i0 < 44; c21_i0++) {
    _SFD_DATA_RANGE_CHECK((*c21_b_state)[c21_i0], 0U);
  }

  chartInstance->c21_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 8U, chartInstance->c21_sfEvent);
  c21_hoistedGlobal = *c21_b_j;
  c21_b_hoistedGlobal = *c21_b_M;
  c21_c_hoistedGlobal = *c21_b_G;
  for (c21_i1 = 0; c21_i1 < 44; c21_i1++) {
    c21_state[c21_i1] = (*c21_b_state)[c21_i1];
  }

  c21_j = c21_hoistedGlobal;
  c21_M = c21_b_hoistedGlobal;
  c21_G = c21_c_hoistedGlobal;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 18U, 18U, c21_debug_family_names,
    c21_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_nargin, 0U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_nargout, 1U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c21_state, 2U, c21_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c21_j, 3U, c21_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c21_M, 4U, c21_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c21_G, 5U, c21_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_go, 6U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_quit, 7U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_deliver, 8U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_land, 9U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_load, 10U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_free, 11U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_wait, 12U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_wait2free, 13U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_wait2tunnel, 14U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_takeoff, 15U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_i, 16U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c21_g, 17U, c21_sf_marshallOut,
    c21_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c21_sfEvent, 3);
  c21_go = c21_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j", c21_M * 11.0 * (c21_G - 1.0) + c21_j), 1,
    44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c21_sfEvent, 4);
  c21_quit = c21_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+1*M", (c21_M * 11.0 * (c21_G - 1.0) + c21_j)
                       + c21_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c21_sfEvent, 5);
  c21_deliver = c21_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+2*M", (c21_M * 11.0 * (c21_G - 1.0) + c21_j)
                       + 2.0 * c21_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c21_sfEvent, 6);
  c21_land = c21_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+3*M", (c21_M * 11.0 * (c21_G - 1.0) + c21_j)
                       + 3.0 * c21_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c21_sfEvent, 7);
  c21_load = c21_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+4*M", (c21_M * 11.0 * (c21_G - 1.0) + c21_j)
                       + 4.0 * c21_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c21_sfEvent, 8);
  c21_free = c21_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+5*M", (c21_M * 11.0 * (c21_G - 1.0) + c21_j)
                       + 5.0 * c21_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c21_sfEvent, 9);
  c21_wait = c21_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+6*M", (c21_M * 11.0 * (c21_G - 1.0) + c21_j)
                       + 6.0 * c21_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c21_sfEvent, 10);
  c21_wait2free = c21_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+7*M", (c21_M * 11.0 * (c21_G - 1.0) + c21_j)
                       + 7.0 * c21_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c21_sfEvent, 11);
  c21_wait2tunnel = c21_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+8*M", (c21_M * 11.0 * (c21_G - 1.0) + c21_j)
                       + 8.0 * c21_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c21_sfEvent, 12);
  c21_takeoff = c21_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("M*11*(G-1)+j+9*M", (c21_M * 11.0 * (c21_G - 1.0) + c21_j)
                       + 9.0 * c21_M), 1, 44, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c21_sfEvent, 13);
  c21_i = c21_j;
  _SFD_EML_CALL(0U, chartInstance->c21_sfEvent, 14);
  c21_g = c21_G;
  _SFD_EML_CALL(0U, chartInstance->c21_sfEvent, -14);
  _SFD_SYMBOL_SCOPE_POP();
  *c21_b_go = c21_go;
  *c21_b_quit = c21_quit;
  *c21_b_deliver = c21_deliver;
  *c21_b_land = c21_land;
  *c21_b_load = c21_load;
  *c21_b_free = c21_free;
  *c21_b_wait = c21_wait;
  *c21_b_wait2free = c21_wait2free;
  *c21_b_wait2tunnel = c21_wait2tunnel;
  *c21_b_takeoff = c21_takeoff;
  *c21_b_i = c21_i;
  *c21_b_g = c21_g;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c21_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_EMS_situation22_test2MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  _SFD_DATA_RANGE_CHECK(*c21_b_go, 1U);
  _SFD_DATA_RANGE_CHECK(*c21_b_j, 2U);
  _SFD_DATA_RANGE_CHECK(*c21_b_quit, 3U);
  _SFD_DATA_RANGE_CHECK(*c21_b_deliver, 4U);
  _SFD_DATA_RANGE_CHECK(*c21_b_land, 5U);
  _SFD_DATA_RANGE_CHECK(*c21_b_load, 6U);
  _SFD_DATA_RANGE_CHECK(*c21_b_free, 7U);
  _SFD_DATA_RANGE_CHECK(*c21_b_wait, 8U);
  _SFD_DATA_RANGE_CHECK(*c21_b_wait2free, 9U);
  _SFD_DATA_RANGE_CHECK(*c21_b_wait2tunnel, 10U);
  _SFD_DATA_RANGE_CHECK(*c21_b_takeoff, 11U);
  _SFD_DATA_RANGE_CHECK(*c21_b_i, 12U);
  _SFD_DATA_RANGE_CHECK(*c21_b_M, 13U);
  _SFD_DATA_RANGE_CHECK(*c21_b_g, 14U);
  _SFD_DATA_RANGE_CHECK(*c21_b_G, 15U);
}

static void initSimStructsc21_EMS_situation22_test2
  (SFc21_EMS_situation22_test2InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c21_machineNumber, uint32_T
  c21_chartNumber, uint32_T c21_instanceNumber)
{
  (void)c21_machineNumber;
  (void)c21_chartNumber;
  (void)c21_instanceNumber;
}

static const mxArray *c21_sf_marshallOut(void *chartInstanceVoid, void
  *c21_inData)
{
  const mxArray *c21_mxArrayOutData = NULL;
  real_T c21_u;
  const mxArray *c21_y = NULL;
  SFc21_EMS_situation22_test2InstanceStruct *chartInstance;
  chartInstance = (SFc21_EMS_situation22_test2InstanceStruct *)chartInstanceVoid;
  c21_mxArrayOutData = NULL;
  c21_u = *(real_T *)c21_inData;
  c21_y = NULL;
  sf_mex_assign(&c21_y, sf_mex_create("y", &c21_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c21_mxArrayOutData, c21_y, false);
  return c21_mxArrayOutData;
}

static real_T c21_emlrt_marshallIn(SFc21_EMS_situation22_test2InstanceStruct
  *chartInstance, const mxArray *c21_g, const char_T *c21_identifier)
{
  real_T c21_y;
  emlrtMsgIdentifier c21_thisId;
  c21_thisId.fIdentifier = c21_identifier;
  c21_thisId.fParent = NULL;
  c21_y = c21_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c21_g), &c21_thisId);
  sf_mex_destroy(&c21_g);
  return c21_y;
}

static real_T c21_b_emlrt_marshallIn(SFc21_EMS_situation22_test2InstanceStruct
  *chartInstance, const mxArray *c21_u, const emlrtMsgIdentifier *c21_parentId)
{
  real_T c21_y;
  real_T c21_d0;
  (void)chartInstance;
  sf_mex_import(c21_parentId, sf_mex_dup(c21_u), &c21_d0, 1, 0, 0U, 0, 0U, 0);
  c21_y = c21_d0;
  sf_mex_destroy(&c21_u);
  return c21_y;
}

static void c21_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c21_mxArrayInData, const char_T *c21_varName, void *c21_outData)
{
  const mxArray *c21_g;
  const char_T *c21_identifier;
  emlrtMsgIdentifier c21_thisId;
  real_T c21_y;
  SFc21_EMS_situation22_test2InstanceStruct *chartInstance;
  chartInstance = (SFc21_EMS_situation22_test2InstanceStruct *)chartInstanceVoid;
  c21_g = sf_mex_dup(c21_mxArrayInData);
  c21_identifier = c21_varName;
  c21_thisId.fIdentifier = c21_identifier;
  c21_thisId.fParent = NULL;
  c21_y = c21_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c21_g), &c21_thisId);
  sf_mex_destroy(&c21_g);
  *(real_T *)c21_outData = c21_y;
  sf_mex_destroy(&c21_mxArrayInData);
}

static const mxArray *c21_b_sf_marshallOut(void *chartInstanceVoid, void
  *c21_inData)
{
  const mxArray *c21_mxArrayOutData = NULL;
  int32_T c21_i2;
  real_T c21_b_inData[44];
  int32_T c21_i3;
  real_T c21_u[44];
  const mxArray *c21_y = NULL;
  SFc21_EMS_situation22_test2InstanceStruct *chartInstance;
  chartInstance = (SFc21_EMS_situation22_test2InstanceStruct *)chartInstanceVoid;
  c21_mxArrayOutData = NULL;
  for (c21_i2 = 0; c21_i2 < 44; c21_i2++) {
    c21_b_inData[c21_i2] = (*(real_T (*)[44])c21_inData)[c21_i2];
  }

  for (c21_i3 = 0; c21_i3 < 44; c21_i3++) {
    c21_u[c21_i3] = c21_b_inData[c21_i3];
  }

  c21_y = NULL;
  sf_mex_assign(&c21_y, sf_mex_create("y", c21_u, 0, 0U, 1U, 0U, 1, 44), false);
  sf_mex_assign(&c21_mxArrayOutData, c21_y, false);
  return c21_mxArrayOutData;
}

const mxArray *sf_c21_EMS_situation22_test2_get_eml_resolved_functions_info(void)
{
  const mxArray *c21_nameCaptureInfo = NULL;
  c21_nameCaptureInfo = NULL;
  sf_mex_assign(&c21_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c21_nameCaptureInfo;
}

static const mxArray *c21_c_sf_marshallOut(void *chartInstanceVoid, void
  *c21_inData)
{
  const mxArray *c21_mxArrayOutData = NULL;
  int32_T c21_u;
  const mxArray *c21_y = NULL;
  SFc21_EMS_situation22_test2InstanceStruct *chartInstance;
  chartInstance = (SFc21_EMS_situation22_test2InstanceStruct *)chartInstanceVoid;
  c21_mxArrayOutData = NULL;
  c21_u = *(int32_T *)c21_inData;
  c21_y = NULL;
  sf_mex_assign(&c21_y, sf_mex_create("y", &c21_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c21_mxArrayOutData, c21_y, false);
  return c21_mxArrayOutData;
}

static int32_T c21_c_emlrt_marshallIn(SFc21_EMS_situation22_test2InstanceStruct *
  chartInstance, const mxArray *c21_u, const emlrtMsgIdentifier *c21_parentId)
{
  int32_T c21_y;
  int32_T c21_i4;
  (void)chartInstance;
  sf_mex_import(c21_parentId, sf_mex_dup(c21_u), &c21_i4, 1, 6, 0U, 0, 0U, 0);
  c21_y = c21_i4;
  sf_mex_destroy(&c21_u);
  return c21_y;
}

static void c21_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c21_mxArrayInData, const char_T *c21_varName, void *c21_outData)
{
  const mxArray *c21_b_sfEvent;
  const char_T *c21_identifier;
  emlrtMsgIdentifier c21_thisId;
  int32_T c21_y;
  SFc21_EMS_situation22_test2InstanceStruct *chartInstance;
  chartInstance = (SFc21_EMS_situation22_test2InstanceStruct *)chartInstanceVoid;
  c21_b_sfEvent = sf_mex_dup(c21_mxArrayInData);
  c21_identifier = c21_varName;
  c21_thisId.fIdentifier = c21_identifier;
  c21_thisId.fParent = NULL;
  c21_y = c21_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c21_b_sfEvent),
    &c21_thisId);
  sf_mex_destroy(&c21_b_sfEvent);
  *(int32_T *)c21_outData = c21_y;
  sf_mex_destroy(&c21_mxArrayInData);
}

static uint8_T c21_d_emlrt_marshallIn(SFc21_EMS_situation22_test2InstanceStruct *
  chartInstance, const mxArray *c21_b_is_active_c21_EMS_situation22_test2, const
  char_T *c21_identifier)
{
  uint8_T c21_y;
  emlrtMsgIdentifier c21_thisId;
  c21_thisId.fIdentifier = c21_identifier;
  c21_thisId.fParent = NULL;
  c21_y = c21_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c21_b_is_active_c21_EMS_situation22_test2), &c21_thisId);
  sf_mex_destroy(&c21_b_is_active_c21_EMS_situation22_test2);
  return c21_y;
}

static uint8_T c21_e_emlrt_marshallIn(SFc21_EMS_situation22_test2InstanceStruct *
  chartInstance, const mxArray *c21_u, const emlrtMsgIdentifier *c21_parentId)
{
  uint8_T c21_y;
  uint8_T c21_u0;
  (void)chartInstance;
  sf_mex_import(c21_parentId, sf_mex_dup(c21_u), &c21_u0, 1, 3, 0U, 0, 0U, 0);
  c21_y = c21_u0;
  sf_mex_destroy(&c21_u);
  return c21_y;
}

static void init_dsm_address_info(SFc21_EMS_situation22_test2InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c21_EMS_situation22_test2_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2444706543U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(173935054U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2191186545U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(333294476U);
}

mxArray *sf_c21_EMS_situation22_test2_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1,1,5,
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("5D5iLmtoYpktu2PWN3YbSH");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,4,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(44);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,12,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,5,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,5,"type",mxType);
    }

    mxSetField(mxData,5,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,6,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,6,"type",mxType);
    }

    mxSetField(mxData,6,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,7,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,7,"type",mxType);
    }

    mxSetField(mxData,7,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,8,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,8,"type",mxType);
    }

    mxSetField(mxData,8,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,9,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,9,"type",mxType);
    }

    mxSetField(mxData,9,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,10,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,10,"type",mxType);
    }

    mxSetField(mxData,10,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,11,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,11,"type",mxType);
    }

    mxSetField(mxData,11,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c21_EMS_situation22_test2_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c21_EMS_situation22_test2_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c21_EMS_situation22_test2(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x10'type','srcId','name','auxInfo'{{M[1],M[9],T\"deliver\",},{M[1],M[12],T\"free\",},{M[1],M[20],T\"g\",},{M[1],M[5],T\"go\",},{M[1],M[6],T\"i\",},{M[1],M[10],T\"land\",},{M[1],M[11],T\"load\",},{M[1],M[8],T\"quit\",},{M[1],M[17],T\"takeoff\",},{M[1],M[14],T\"wait\",}}",
    "100 S1x3'type','srcId','name','auxInfo'{{M[1],M[15],T\"wait2free\",},{M[1],M[16],T\"wait2tunnel\",},{M[8],M[0],T\"is_active_c21_EMS_situation22_test2\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 13, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c21_EMS_situation22_test2_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc21_EMS_situation22_test2InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc21_EMS_situation22_test2InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _EMS_situation22_test2MachineNumber_,
           21,
           1,
           1,
           0,
           16,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize ist own list of scripts */
        init_script_number_translation(_EMS_situation22_test2MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_EMS_situation22_test2MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _EMS_situation22_test2MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"state");
          _SFD_SET_DATA_PROPS(1,2,0,1,"go");
          _SFD_SET_DATA_PROPS(2,1,1,0,"j");
          _SFD_SET_DATA_PROPS(3,2,0,1,"quit");
          _SFD_SET_DATA_PROPS(4,2,0,1,"deliver");
          _SFD_SET_DATA_PROPS(5,2,0,1,"land");
          _SFD_SET_DATA_PROPS(6,2,0,1,"load");
          _SFD_SET_DATA_PROPS(7,2,0,1,"free");
          _SFD_SET_DATA_PROPS(8,2,0,1,"wait");
          _SFD_SET_DATA_PROPS(9,2,0,1,"wait2free");
          _SFD_SET_DATA_PROPS(10,2,0,1,"wait2tunnel");
          _SFD_SET_DATA_PROPS(11,2,0,1,"takeoff");
          _SFD_SET_DATA_PROPS(12,2,0,1,"i");
          _SFD_SET_DATA_PROPS(13,1,1,0,"M");
          _SFD_SET_DATA_PROPS(14,2,0,1,"g");
          _SFD_SET_DATA_PROPS(15,1,1,0,"G");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,440);

        {
          unsigned int dimVector[1];
          dimVector[0]= 44;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c21_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)c21_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)c21_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)c21_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)c21_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)c21_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)c21_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)c21_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(9,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)c21_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(10,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)c21_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(11,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)c21_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(12,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)c21_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(13,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(14,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)c21_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(15,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c21_sf_marshallOut,(MexInFcnForType)NULL);

        {
          real_T *c21_go;
          real_T *c21_j;
          real_T *c21_quit;
          real_T *c21_deliver;
          real_T *c21_land;
          real_T *c21_load;
          real_T *c21_free;
          real_T *c21_wait;
          real_T *c21_wait2free;
          real_T *c21_wait2tunnel;
          real_T *c21_takeoff;
          real_T *c21_i;
          real_T *c21_M;
          real_T *c21_g;
          real_T *c21_G;
          real_T (*c21_state)[44];
          c21_G = (real_T *)ssGetInputPortSignal(chartInstance->S, 3);
          c21_g = (real_T *)ssGetOutputPortSignal(chartInstance->S, 12);
          c21_M = (real_T *)ssGetInputPortSignal(chartInstance->S, 2);
          c21_i = (real_T *)ssGetOutputPortSignal(chartInstance->S, 11);
          c21_takeoff = (real_T *)ssGetOutputPortSignal(chartInstance->S, 10);
          c21_wait2tunnel = (real_T *)ssGetOutputPortSignal(chartInstance->S, 9);
          c21_wait2free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 8);
          c21_wait = (real_T *)ssGetOutputPortSignal(chartInstance->S, 7);
          c21_free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 6);
          c21_load = (real_T *)ssGetOutputPortSignal(chartInstance->S, 5);
          c21_land = (real_T *)ssGetOutputPortSignal(chartInstance->S, 4);
          c21_deliver = (real_T *)ssGetOutputPortSignal(chartInstance->S, 3);
          c21_quit = (real_T *)ssGetOutputPortSignal(chartInstance->S, 2);
          c21_j = (real_T *)ssGetInputPortSignal(chartInstance->S, 1);
          c21_go = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
          c21_state = (real_T (*)[44])ssGetInputPortSignal(chartInstance->S, 0);
          _SFD_SET_DATA_VALUE_PTR(0U, *c21_state);
          _SFD_SET_DATA_VALUE_PTR(1U, c21_go);
          _SFD_SET_DATA_VALUE_PTR(2U, c21_j);
          _SFD_SET_DATA_VALUE_PTR(3U, c21_quit);
          _SFD_SET_DATA_VALUE_PTR(4U, c21_deliver);
          _SFD_SET_DATA_VALUE_PTR(5U, c21_land);
          _SFD_SET_DATA_VALUE_PTR(6U, c21_load);
          _SFD_SET_DATA_VALUE_PTR(7U, c21_free);
          _SFD_SET_DATA_VALUE_PTR(8U, c21_wait);
          _SFD_SET_DATA_VALUE_PTR(9U, c21_wait2free);
          _SFD_SET_DATA_VALUE_PTR(10U, c21_wait2tunnel);
          _SFD_SET_DATA_VALUE_PTR(11U, c21_takeoff);
          _SFD_SET_DATA_VALUE_PTR(12U, c21_i);
          _SFD_SET_DATA_VALUE_PTR(13U, c21_M);
          _SFD_SET_DATA_VALUE_PTR(14U, c21_g);
          _SFD_SET_DATA_VALUE_PTR(15U, c21_G);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _EMS_situation22_test2MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "9qrhi10zI774m1uRiZbP0C";
}

static void sf_opaque_initialize_c21_EMS_situation22_test2(void
  *chartInstanceVar)
{
  chart_debug_initialization(((SFc21_EMS_situation22_test2InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c21_EMS_situation22_test2
    ((SFc21_EMS_situation22_test2InstanceStruct*) chartInstanceVar);
  initialize_c21_EMS_situation22_test2
    ((SFc21_EMS_situation22_test2InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c21_EMS_situation22_test2(void *chartInstanceVar)
{
  enable_c21_EMS_situation22_test2((SFc21_EMS_situation22_test2InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c21_EMS_situation22_test2(void *chartInstanceVar)
{
  disable_c21_EMS_situation22_test2((SFc21_EMS_situation22_test2InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c21_EMS_situation22_test2(void *chartInstanceVar)
{
  sf_gateway_c21_EMS_situation22_test2
    ((SFc21_EMS_situation22_test2InstanceStruct*) chartInstanceVar);
}

extern const mxArray* sf_internal_get_sim_state_c21_EMS_situation22_test2
  (SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[4];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_raw2high");
  prhs[1] = mxCreateDoubleScalar(ssGetSFuncBlockHandle(S));
  prhs[2] = (mxArray*) get_sim_state_c21_EMS_situation22_test2
    ((SFc21_EMS_situation22_test2InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
  prhs[3] = (mxArray*) sf_get_sim_state_info_c21_EMS_situation22_test2();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 4, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  mxDestroyArray(prhs[3]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_raw2high'.\n");
  }

  return plhs[0];
}

extern void sf_internal_set_sim_state_c21_EMS_situation22_test2(SimStruct* S,
  const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[3];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_high2raw");
  prhs[1] = mxDuplicateArray(st);      /* high level simctx */
  prhs[2] = (mxArray*) sf_get_sim_state_info_c21_EMS_situation22_test2();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 3, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_high2raw'.\n");
  }

  set_sim_state_c21_EMS_situation22_test2
    ((SFc21_EMS_situation22_test2InstanceStruct*)chartInfo->chartInstance,
     mxDuplicateArray(plhs[0]));
  mxDestroyArray(plhs[0]);
}

static const mxArray* sf_opaque_get_sim_state_c21_EMS_situation22_test2
  (SimStruct* S)
{
  return sf_internal_get_sim_state_c21_EMS_situation22_test2(S);
}

static void sf_opaque_set_sim_state_c21_EMS_situation22_test2(SimStruct* S,
  const mxArray *st)
{
  sf_internal_set_sim_state_c21_EMS_situation22_test2(S, st);
}

static void sf_opaque_terminate_c21_EMS_situation22_test2(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc21_EMS_situation22_test2InstanceStruct*)
                    chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_EMS_situation22_test2_optimization_info();
    }

    finalize_c21_EMS_situation22_test2
      ((SFc21_EMS_situation22_test2InstanceStruct*) chartInstanceVar);
    utFree((void *)chartInstanceVar);
    if (crtInfo != NULL) {
      utFree((void *)crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc21_EMS_situation22_test2
    ((SFc21_EMS_situation22_test2InstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c21_EMS_situation22_test2(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c21_EMS_situation22_test2
      ((SFc21_EMS_situation22_test2InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c21_EMS_situation22_test2(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_EMS_situation22_test2_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      21);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,21,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,21,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,21);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,21,4);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,21,12);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=12; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 4; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,21);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(782203031U));
  ssSetChecksum1(S,(4197775041U));
  ssSetChecksum2(S,(1397746300U));
  ssSetChecksum3(S,(4080604569U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c21_EMS_situation22_test2(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c21_EMS_situation22_test2(SimStruct *S)
{
  SFc21_EMS_situation22_test2InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc21_EMS_situation22_test2InstanceStruct *)utMalloc(sizeof
    (SFc21_EMS_situation22_test2InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc21_EMS_situation22_test2InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c21_EMS_situation22_test2;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c21_EMS_situation22_test2;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c21_EMS_situation22_test2;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c21_EMS_situation22_test2;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c21_EMS_situation22_test2;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c21_EMS_situation22_test2;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c21_EMS_situation22_test2;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c21_EMS_situation22_test2;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c21_EMS_situation22_test2;
  chartInstance->chartInfo.mdlStart = mdlStart_c21_EMS_situation22_test2;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c21_EMS_situation22_test2;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c21_EMS_situation22_test2_method_dispatcher(SimStruct *S, int_T method,
  void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c21_EMS_situation22_test2(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c21_EMS_situation22_test2(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c21_EMS_situation22_test2(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c21_EMS_situation22_test2_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

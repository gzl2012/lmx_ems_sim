/* Include files */

#include "EMS_situation1_3_sfun.h"
#include "EMS_situation1_3_sfun_debug_macros.h"
#include "c1_EMS_situation1_3.h"
#include "c16_EMS_situation1_3.h"
#include "c21_EMS_situation1_3.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
uint32_T _EMS_situation1_3MachineNumber_;

/* Function Declarations */

/* Function Definitions */
void EMS_situation1_3_initializer(void)
{
}

void EMS_situation1_3_terminator(void)
{
}

/* SFunction Glue Code */
unsigned int sf_EMS_situation1_3_method_dispatcher(SimStruct *simstructPtr,
  unsigned int chartFileNumber, const char* specsCksum, int_T method, void *data)
{
  if (chartFileNumber==1) {
    c1_EMS_situation1_3_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==16) {
    c16_EMS_situation1_3_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==21) {
    c21_EMS_situation1_3_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  return 0;
}

unsigned int sf_EMS_situation1_3_process_check_sum_call( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"sf_get_check_sum"))
    return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if (nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if (!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(4061111421U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(497992713U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3298862599U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2625478576U);
    } else if (!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    } else if (!strcmp(commandName,"makefile")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2066425548U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1287099734U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1314793869U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2777402513U);
    } else if (nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch (chartFileNumber) {
       case 1:
        {
          extern void sf_c1_EMS_situation1_3_get_check_sum(mxArray *plhs[]);
          sf_c1_EMS_situation1_3_get_check_sum(plhs);
          break;
        }

       case 16:
        {
          extern void sf_c16_EMS_situation1_3_get_check_sum(mxArray *plhs[]);
          sf_c16_EMS_situation1_3_get_check_sum(plhs);
          break;
        }

       case 21:
        {
          extern void sf_c21_EMS_situation1_3_get_check_sum(mxArray *plhs[]);
          sf_c21_EMS_situation1_3_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    } else if (!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3031367619U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(4001028638U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3978939492U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(838979348U);
    } else {
      return 0;
    }
  } else {
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1220258647U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(770607299U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3658663055U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(785280215U);
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_EMS_situation1_3_autoinheritance_info( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[32];
  char aiChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the autoinheritance_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_autoinheritance_info"))
    return 0;
  mxGetString(prhs[2], aiChksum,sizeof(aiChksum)/sizeof(char));
  aiChksum[(sizeof(aiChksum)/sizeof(char)-1)] = '\0';

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(aiChksum, "JUM9Q0zeqrK72S5q7c0yBH") == 0) {
          extern mxArray *sf_c1_EMS_situation1_3_get_autoinheritance_info(void);
          plhs[0] = sf_c1_EMS_situation1_3_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 16:
      {
        if (strcmp(aiChksum, "Afw1FDFTLDm9bvkxbgSTID") == 0) {
          extern mxArray *sf_c16_EMS_situation1_3_get_autoinheritance_info(void);
          plhs[0] = sf_c16_EMS_situation1_3_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 21:
      {
        if (strcmp(aiChksum, "PAMFtWjg7GxcPG6n7guPeB") == 0) {
          extern mxArray *sf_c21_EMS_situation1_3_get_autoinheritance_info(void);
          plhs[0] = sf_c21_EMS_situation1_3_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_EMS_situation1_3_get_eml_resolved_functions_info( int nlhs,
  mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[64];
  if (nrhs<2 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the get_eml_resolved_functions_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_eml_resolved_functions_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        extern const mxArray
          *sf_c1_EMS_situation1_3_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c1_EMS_situation1_3_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 16:
      {
        extern const mxArray
          *sf_c16_EMS_situation1_3_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c16_EMS_situation1_3_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 21:
      {
        extern const mxArray
          *sf_c21_EMS_situation1_3_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c21_EMS_situation1_3_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_EMS_situation1_3_third_party_uses_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the third_party_uses_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_third_party_uses_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "XTrBqgmeVhfZKf4KDc0EQB") == 0) {
          extern mxArray *sf_c1_EMS_situation1_3_third_party_uses_info(void);
          plhs[0] = sf_c1_EMS_situation1_3_third_party_uses_info();
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "NC0wkO6GaZWIPcY5VAZgNE") == 0) {
          extern mxArray *sf_c16_EMS_situation1_3_third_party_uses_info(void);
          plhs[0] = sf_c16_EMS_situation1_3_third_party_uses_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "vV3Zj8djtxyx0Ugu8XM3mB") == 0) {
          extern mxArray *sf_c21_EMS_situation1_3_third_party_uses_info(void);
          plhs[0] = sf_c21_EMS_situation1_3_third_party_uses_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_EMS_situation1_3_updateBuildInfo_args_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the updateBuildInfo_args_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_updateBuildInfo_args_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "XTrBqgmeVhfZKf4KDc0EQB") == 0) {
          extern mxArray *sf_c1_EMS_situation1_3_updateBuildInfo_args_info(void);
          plhs[0] = sf_c1_EMS_situation1_3_updateBuildInfo_args_info();
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "NC0wkO6GaZWIPcY5VAZgNE") == 0) {
          extern mxArray *sf_c16_EMS_situation1_3_updateBuildInfo_args_info(void);
          plhs[0] = sf_c16_EMS_situation1_3_updateBuildInfo_args_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "vV3Zj8djtxyx0Ugu8XM3mB") == 0) {
          extern mxArray *sf_c21_EMS_situation1_3_updateBuildInfo_args_info(void);
          plhs[0] = sf_c21_EMS_situation1_3_updateBuildInfo_args_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

void EMS_situation1_3_debug_initialize(struct SfDebugInstanceStruct*
  debugInstance)
{
  _EMS_situation1_3MachineNumber_ = sf_debug_initialize_machine(debugInstance,
    "EMS_situation1_3","sfun",0,3,0,0,0);
  sf_debug_set_machine_event_thresholds(debugInstance,
    _EMS_situation1_3MachineNumber_,0,0);
  sf_debug_set_machine_data_thresholds(debugInstance,
    _EMS_situation1_3MachineNumber_,0);
}

void EMS_situation1_3_register_exported_symbols(SimStruct* S)
{
}

static mxArray* sRtwOptimizationInfoStruct= NULL;
mxArray* load_EMS_situation1_3_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct==NULL) {
    sRtwOptimizationInfoStruct = sf_load_rtw_optimization_info(
      "EMS_situation1_3", "EMS_situation1_3");
    mexMakeArrayPersistent(sRtwOptimizationInfoStruct);
  }

  return(sRtwOptimizationInfoStruct);
}

void unload_EMS_situation1_3_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct!=NULL) {
    mxDestroyArray(sRtwOptimizationInfoStruct);
    sRtwOptimizationInfoStruct = NULL;
  }
}

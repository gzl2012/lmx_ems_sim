/* Include files */

#include <stddef.h>
#include "blas.h"
#include "EMS_situation3_3_sfun.h"
#include "c5_EMS_situation3_3.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "EMS_situation3_3_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c5_debug_family_names[16] = { "nargin", "nargout", "state",
  "j", "M", "go", "quit", "deliver", "land", "load", "free", "wait", "wait2free",
  "wait2tunnel", "takeoff", "i" };

/* Function Declarations */
static void initialize_c5_EMS_situation3_3(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance);
static void initialize_params_c5_EMS_situation3_3
  (SFc5_EMS_situation3_3InstanceStruct *chartInstance);
static void enable_c5_EMS_situation3_3(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance);
static void disable_c5_EMS_situation3_3(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance);
static void c5_update_debugger_state_c5_EMS_situation3_3
  (SFc5_EMS_situation3_3InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c5_EMS_situation3_3
  (SFc5_EMS_situation3_3InstanceStruct *chartInstance);
static void set_sim_state_c5_EMS_situation3_3
  (SFc5_EMS_situation3_3InstanceStruct *chartInstance, const mxArray *c5_st);
static void finalize_c5_EMS_situation3_3(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance);
static void sf_gateway_c5_EMS_situation3_3(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance);
static void initSimStructsc5_EMS_situation3_3
  (SFc5_EMS_situation3_3InstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c5_machineNumber, uint32_T
  c5_chartNumber, uint32_T c5_instanceNumber);
static const mxArray *c5_sf_marshallOut(void *chartInstanceVoid, void *c5_inData);
static real_T c5_emlrt_marshallIn(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance, const mxArray *c5_i, const char_T *c5_identifier);
static real_T c5_b_emlrt_marshallIn(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_b_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static const mxArray *c5_c_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static int32_T c5_c_emlrt_marshallIn(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static uint8_T c5_d_emlrt_marshallIn(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance, const mxArray *c5_b_is_active_c5_EMS_situation3_3, const
  char_T *c5_identifier);
static uint8_T c5_e_emlrt_marshallIn(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void init_dsm_address_info(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c5_EMS_situation3_3(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance)
{
  chartInstance->c5_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c5_is_active_c5_EMS_situation3_3 = 0U;
}

static void initialize_params_c5_EMS_situation3_3
  (SFc5_EMS_situation3_3InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c5_EMS_situation3_3(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c5_EMS_situation3_3(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c5_update_debugger_state_c5_EMS_situation3_3
  (SFc5_EMS_situation3_3InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c5_EMS_situation3_3
  (SFc5_EMS_situation3_3InstanceStruct *chartInstance)
{
  const mxArray *c5_st;
  const mxArray *c5_y = NULL;
  real_T c5_hoistedGlobal;
  real_T c5_u;
  const mxArray *c5_b_y = NULL;
  real_T c5_b_hoistedGlobal;
  real_T c5_b_u;
  const mxArray *c5_c_y = NULL;
  real_T c5_c_hoistedGlobal;
  real_T c5_c_u;
  const mxArray *c5_d_y = NULL;
  real_T c5_d_hoistedGlobal;
  real_T c5_d_u;
  const mxArray *c5_e_y = NULL;
  real_T c5_e_hoistedGlobal;
  real_T c5_e_u;
  const mxArray *c5_f_y = NULL;
  real_T c5_f_hoistedGlobal;
  real_T c5_f_u;
  const mxArray *c5_g_y = NULL;
  real_T c5_g_hoistedGlobal;
  real_T c5_g_u;
  const mxArray *c5_h_y = NULL;
  real_T c5_h_hoistedGlobal;
  real_T c5_h_u;
  const mxArray *c5_i_y = NULL;
  real_T c5_i_hoistedGlobal;
  real_T c5_i_u;
  const mxArray *c5_j_y = NULL;
  real_T c5_j_hoistedGlobal;
  real_T c5_j_u;
  const mxArray *c5_k_y = NULL;
  real_T c5_k_hoistedGlobal;
  real_T c5_k_u;
  const mxArray *c5_l_y = NULL;
  uint8_T c5_l_hoistedGlobal;
  uint8_T c5_l_u;
  const mxArray *c5_m_y = NULL;
  real_T *c5_deliver;
  real_T *c5_free;
  real_T *c5_go;
  real_T *c5_i;
  real_T *c5_land;
  real_T *c5_load;
  real_T *c5_quit;
  real_T *c5_takeoff;
  real_T *c5_wait;
  real_T *c5_wait2free;
  real_T *c5_wait2tunnel;
  c5_i = (real_T *)ssGetOutputPortSignal(chartInstance->S, 11);
  c5_takeoff = (real_T *)ssGetOutputPortSignal(chartInstance->S, 10);
  c5_wait2tunnel = (real_T *)ssGetOutputPortSignal(chartInstance->S, 9);
  c5_wait2free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 8);
  c5_wait = (real_T *)ssGetOutputPortSignal(chartInstance->S, 7);
  c5_free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 6);
  c5_load = (real_T *)ssGetOutputPortSignal(chartInstance->S, 5);
  c5_land = (real_T *)ssGetOutputPortSignal(chartInstance->S, 4);
  c5_deliver = (real_T *)ssGetOutputPortSignal(chartInstance->S, 3);
  c5_quit = (real_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c5_go = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c5_st = NULL;
  c5_st = NULL;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_createcellmatrix(12, 1), false);
  c5_hoistedGlobal = *c5_deliver;
  c5_u = c5_hoistedGlobal;
  c5_b_y = NULL;
  sf_mex_assign(&c5_b_y, sf_mex_create("y", &c5_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 0, c5_b_y);
  c5_b_hoistedGlobal = *c5_free;
  c5_b_u = c5_b_hoistedGlobal;
  c5_c_y = NULL;
  sf_mex_assign(&c5_c_y, sf_mex_create("y", &c5_b_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 1, c5_c_y);
  c5_c_hoistedGlobal = *c5_go;
  c5_c_u = c5_c_hoistedGlobal;
  c5_d_y = NULL;
  sf_mex_assign(&c5_d_y, sf_mex_create("y", &c5_c_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 2, c5_d_y);
  c5_d_hoistedGlobal = *c5_i;
  c5_d_u = c5_d_hoistedGlobal;
  c5_e_y = NULL;
  sf_mex_assign(&c5_e_y, sf_mex_create("y", &c5_d_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 3, c5_e_y);
  c5_e_hoistedGlobal = *c5_land;
  c5_e_u = c5_e_hoistedGlobal;
  c5_f_y = NULL;
  sf_mex_assign(&c5_f_y, sf_mex_create("y", &c5_e_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 4, c5_f_y);
  c5_f_hoistedGlobal = *c5_load;
  c5_f_u = c5_f_hoistedGlobal;
  c5_g_y = NULL;
  sf_mex_assign(&c5_g_y, sf_mex_create("y", &c5_f_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 5, c5_g_y);
  c5_g_hoistedGlobal = *c5_quit;
  c5_g_u = c5_g_hoistedGlobal;
  c5_h_y = NULL;
  sf_mex_assign(&c5_h_y, sf_mex_create("y", &c5_g_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 6, c5_h_y);
  c5_h_hoistedGlobal = *c5_takeoff;
  c5_h_u = c5_h_hoistedGlobal;
  c5_i_y = NULL;
  sf_mex_assign(&c5_i_y, sf_mex_create("y", &c5_h_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 7, c5_i_y);
  c5_i_hoistedGlobal = *c5_wait;
  c5_i_u = c5_i_hoistedGlobal;
  c5_j_y = NULL;
  sf_mex_assign(&c5_j_y, sf_mex_create("y", &c5_i_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 8, c5_j_y);
  c5_j_hoistedGlobal = *c5_wait2free;
  c5_j_u = c5_j_hoistedGlobal;
  c5_k_y = NULL;
  sf_mex_assign(&c5_k_y, sf_mex_create("y", &c5_j_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 9, c5_k_y);
  c5_k_hoistedGlobal = *c5_wait2tunnel;
  c5_k_u = c5_k_hoistedGlobal;
  c5_l_y = NULL;
  sf_mex_assign(&c5_l_y, sf_mex_create("y", &c5_k_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 10, c5_l_y);
  c5_l_hoistedGlobal = chartInstance->c5_is_active_c5_EMS_situation3_3;
  c5_l_u = c5_l_hoistedGlobal;
  c5_m_y = NULL;
  sf_mex_assign(&c5_m_y, sf_mex_create("y", &c5_l_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c5_y, 11, c5_m_y);
  sf_mex_assign(&c5_st, c5_y, false);
  return c5_st;
}

static void set_sim_state_c5_EMS_situation3_3
  (SFc5_EMS_situation3_3InstanceStruct *chartInstance, const mxArray *c5_st)
{
  const mxArray *c5_u;
  real_T *c5_deliver;
  real_T *c5_free;
  real_T *c5_go;
  real_T *c5_i;
  real_T *c5_land;
  real_T *c5_load;
  real_T *c5_quit;
  real_T *c5_takeoff;
  real_T *c5_wait;
  real_T *c5_wait2free;
  real_T *c5_wait2tunnel;
  c5_i = (real_T *)ssGetOutputPortSignal(chartInstance->S, 11);
  c5_takeoff = (real_T *)ssGetOutputPortSignal(chartInstance->S, 10);
  c5_wait2tunnel = (real_T *)ssGetOutputPortSignal(chartInstance->S, 9);
  c5_wait2free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 8);
  c5_wait = (real_T *)ssGetOutputPortSignal(chartInstance->S, 7);
  c5_free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 6);
  c5_load = (real_T *)ssGetOutputPortSignal(chartInstance->S, 5);
  c5_land = (real_T *)ssGetOutputPortSignal(chartInstance->S, 4);
  c5_deliver = (real_T *)ssGetOutputPortSignal(chartInstance->S, 3);
  c5_quit = (real_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c5_go = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  chartInstance->c5_doneDoubleBufferReInit = true;
  c5_u = sf_mex_dup(c5_st);
  *c5_deliver = c5_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c5_u, 0)), "deliver");
  *c5_free = c5_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c5_u,
    1)), "free");
  *c5_go = c5_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c5_u, 2)),
    "go");
  *c5_i = c5_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c5_u, 3)),
    "i");
  *c5_land = c5_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c5_u,
    4)), "land");
  *c5_load = c5_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c5_u,
    5)), "load");
  *c5_quit = c5_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c5_u,
    6)), "quit");
  *c5_takeoff = c5_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c5_u, 7)), "takeoff");
  *c5_wait = c5_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c5_u,
    8)), "wait");
  *c5_wait2free = c5_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c5_u, 9)), "wait2free");
  *c5_wait2tunnel = c5_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c5_u, 10)), "wait2tunnel");
  chartInstance->c5_is_active_c5_EMS_situation3_3 = c5_d_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c5_u, 11)),
     "is_active_c5_EMS_situation3_3");
  sf_mex_destroy(&c5_u);
  c5_update_debugger_state_c5_EMS_situation3_3(chartInstance);
  sf_mex_destroy(&c5_st);
}

static void finalize_c5_EMS_situation3_3(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c5_EMS_situation3_3(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance)
{
  int32_T c5_i0;
  real_T c5_hoistedGlobal;
  real_T c5_b_hoistedGlobal;
  int32_T c5_i1;
  real_T c5_state[33];
  real_T c5_j;
  real_T c5_M;
  uint32_T c5_debug_family_var_map[16];
  real_T c5_nargin = 3.0;
  real_T c5_nargout = 11.0;
  real_T c5_go;
  real_T c5_quit;
  real_T c5_deliver;
  real_T c5_land;
  real_T c5_load;
  real_T c5_free;
  real_T c5_wait;
  real_T c5_wait2free;
  real_T c5_wait2tunnel;
  real_T c5_takeoff;
  real_T c5_i;
  real_T *c5_b_go;
  real_T *c5_b_j;
  real_T *c5_b_quit;
  real_T *c5_b_deliver;
  real_T *c5_b_land;
  real_T *c5_b_load;
  real_T *c5_b_free;
  real_T *c5_b_wait;
  real_T *c5_b_wait2free;
  real_T *c5_b_wait2tunnel;
  real_T *c5_b_takeoff;
  real_T *c5_b_i;
  real_T *c5_b_M;
  real_T (*c5_b_state)[33];
  c5_b_M = (real_T *)ssGetInputPortSignal(chartInstance->S, 2);
  c5_b_i = (real_T *)ssGetOutputPortSignal(chartInstance->S, 11);
  c5_b_takeoff = (real_T *)ssGetOutputPortSignal(chartInstance->S, 10);
  c5_b_wait2tunnel = (real_T *)ssGetOutputPortSignal(chartInstance->S, 9);
  c5_b_wait2free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 8);
  c5_b_wait = (real_T *)ssGetOutputPortSignal(chartInstance->S, 7);
  c5_b_free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 6);
  c5_b_load = (real_T *)ssGetOutputPortSignal(chartInstance->S, 5);
  c5_b_land = (real_T *)ssGetOutputPortSignal(chartInstance->S, 4);
  c5_b_deliver = (real_T *)ssGetOutputPortSignal(chartInstance->S, 3);
  c5_b_quit = (real_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c5_b_j = (real_T *)ssGetInputPortSignal(chartInstance->S, 1);
  c5_b_go = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c5_b_state = (real_T (*)[33])ssGetInputPortSignal(chartInstance->S, 0);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
  for (c5_i0 = 0; c5_i0 < 33; c5_i0++) {
    _SFD_DATA_RANGE_CHECK((*c5_b_state)[c5_i0], 0U);
  }

  chartInstance->c5_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
  c5_hoistedGlobal = *c5_b_j;
  c5_b_hoistedGlobal = *c5_b_M;
  for (c5_i1 = 0; c5_i1 < 33; c5_i1++) {
    c5_state[c5_i1] = (*c5_b_state)[c5_i1];
  }

  c5_j = c5_hoistedGlobal;
  c5_M = c5_b_hoistedGlobal;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 16U, 16U, c5_debug_family_names,
    c5_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargin, 0U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargout, 1U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c5_state, 2U, c5_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c5_j, 3U, c5_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c5_M, 4U, c5_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_go, 5U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_quit, 6U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_deliver, 7U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_land, 8U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_load, 9U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_free, 10U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_wait, 11U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_wait2free, 12U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_wait2tunnel, 13U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_takeoff, 14U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_i, 15U, c5_sf_marshallOut,
    c5_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 3);
  c5_go = c5_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("j", c5_j), 1, 33, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 4);
  c5_quit = c5_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("j+1*M", c5_j + c5_M), 1, 33, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 5);
  c5_deliver = c5_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("j+2*M", c5_j + 2.0 * c5_M), 1, 33, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 6);
  c5_land = c5_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("j+3*M", c5_j + 3.0 * c5_M), 1, 33, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 7);
  c5_load = c5_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("j+4*M", c5_j + 4.0 * c5_M), 1, 33, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 8);
  c5_free = c5_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("j+5*M", c5_j + 5.0 * c5_M), 1, 33, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 9);
  c5_wait = c5_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("j+6*M", c5_j + 6.0 * c5_M), 1, 33, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 10);
  c5_wait2free = c5_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("j+7*M", c5_j + 7.0 * c5_M), 1, 33, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 11);
  c5_wait2tunnel = c5_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("j+8*M", c5_j + 8.0 * c5_M), 1, 33, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 12);
  c5_takeoff = c5_state[_SFD_EML_ARRAY_BOUNDS_CHECK("state", (int32_T)
    _SFD_INTEGER_CHECK("j+9*M", c5_j + 9.0 * c5_M), 1, 33, 1, 0) - 1];
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, 13);
  c5_i = c5_j;
  _SFD_EML_CALL(0U, chartInstance->c5_sfEvent, -13);
  _SFD_SYMBOL_SCOPE_POP();
  *c5_b_go = c5_go;
  *c5_b_quit = c5_quit;
  *c5_b_deliver = c5_deliver;
  *c5_b_land = c5_land;
  *c5_b_load = c5_load;
  *c5_b_free = c5_free;
  *c5_b_wait = c5_wait;
  *c5_b_wait2free = c5_wait2free;
  *c5_b_wait2tunnel = c5_wait2tunnel;
  *c5_b_takeoff = c5_takeoff;
  *c5_b_i = c5_i;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_EMS_situation3_3MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  _SFD_DATA_RANGE_CHECK(*c5_b_go, 1U);
  _SFD_DATA_RANGE_CHECK(*c5_b_j, 2U);
  _SFD_DATA_RANGE_CHECK(*c5_b_quit, 3U);
  _SFD_DATA_RANGE_CHECK(*c5_b_deliver, 4U);
  _SFD_DATA_RANGE_CHECK(*c5_b_land, 5U);
  _SFD_DATA_RANGE_CHECK(*c5_b_load, 6U);
  _SFD_DATA_RANGE_CHECK(*c5_b_free, 7U);
  _SFD_DATA_RANGE_CHECK(*c5_b_wait, 8U);
  _SFD_DATA_RANGE_CHECK(*c5_b_wait2free, 9U);
  _SFD_DATA_RANGE_CHECK(*c5_b_wait2tunnel, 10U);
  _SFD_DATA_RANGE_CHECK(*c5_b_takeoff, 11U);
  _SFD_DATA_RANGE_CHECK(*c5_b_i, 12U);
  _SFD_DATA_RANGE_CHECK(*c5_b_M, 13U);
}

static void initSimStructsc5_EMS_situation3_3
  (SFc5_EMS_situation3_3InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c5_machineNumber, uint32_T
  c5_chartNumber, uint32_T c5_instanceNumber)
{
  (void)c5_machineNumber;
  (void)c5_chartNumber;
  (void)c5_instanceNumber;
}

static const mxArray *c5_sf_marshallOut(void *chartInstanceVoid, void *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  real_T c5_u;
  const mxArray *c5_y = NULL;
  SFc5_EMS_situation3_3InstanceStruct *chartInstance;
  chartInstance = (SFc5_EMS_situation3_3InstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u = *(real_T *)c5_inData;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static real_T c5_emlrt_marshallIn(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance, const mxArray *c5_i, const char_T *c5_identifier)
{
  real_T c5_y;
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_i), &c5_thisId);
  sf_mex_destroy(&c5_i);
  return c5_y;
}

static real_T c5_b_emlrt_marshallIn(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  real_T c5_y;
  real_T c5_d0;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_d0, 1, 0, 0U, 0, 0U, 0);
  c5_y = c5_d0;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_i;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  real_T c5_y;
  SFc5_EMS_situation3_3InstanceStruct *chartInstance;
  chartInstance = (SFc5_EMS_situation3_3InstanceStruct *)chartInstanceVoid;
  c5_i = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_i), &c5_thisId);
  sf_mex_destroy(&c5_i);
  *(real_T *)c5_outData = c5_y;
  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_b_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  int32_T c5_i2;
  real_T c5_b_inData[33];
  int32_T c5_i3;
  real_T c5_u[33];
  const mxArray *c5_y = NULL;
  SFc5_EMS_situation3_3InstanceStruct *chartInstance;
  chartInstance = (SFc5_EMS_situation3_3InstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  for (c5_i2 = 0; c5_i2 < 33; c5_i2++) {
    c5_b_inData[c5_i2] = (*(real_T (*)[33])c5_inData)[c5_i2];
  }

  for (c5_i3 = 0; c5_i3 < 33; c5_i3++) {
    c5_u[c5_i3] = c5_b_inData[c5_i3];
  }

  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", c5_u, 0, 0U, 1U, 0U, 1, 33), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

const mxArray *sf_c5_EMS_situation3_3_get_eml_resolved_functions_info(void)
{
  const mxArray *c5_nameCaptureInfo = NULL;
  c5_nameCaptureInfo = NULL;
  sf_mex_assign(&c5_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c5_nameCaptureInfo;
}

static const mxArray *c5_c_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  int32_T c5_u;
  const mxArray *c5_y = NULL;
  SFc5_EMS_situation3_3InstanceStruct *chartInstance;
  chartInstance = (SFc5_EMS_situation3_3InstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  c5_u = *(int32_T *)c5_inData;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c5_mxArrayOutData, c5_y, false);
  return c5_mxArrayOutData;
}

static int32_T c5_c_emlrt_marshallIn(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  int32_T c5_y;
  int32_T c5_i4;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_i4, 1, 6, 0U, 0, 0U, 0);
  c5_y = c5_i4;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  const mxArray *c5_b_sfEvent;
  const char_T *c5_identifier;
  emlrtMsgIdentifier c5_thisId;
  int32_T c5_y;
  SFc5_EMS_situation3_3InstanceStruct *chartInstance;
  chartInstance = (SFc5_EMS_situation3_3InstanceStruct *)chartInstanceVoid;
  c5_b_sfEvent = sf_mex_dup(c5_mxArrayInData);
  c5_identifier = c5_varName;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_sfEvent),
    &c5_thisId);
  sf_mex_destroy(&c5_b_sfEvent);
  *(int32_T *)c5_outData = c5_y;
  sf_mex_destroy(&c5_mxArrayInData);
}

static uint8_T c5_d_emlrt_marshallIn(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance, const mxArray *c5_b_is_active_c5_EMS_situation3_3, const
  char_T *c5_identifier)
{
  uint8_T c5_y;
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c5_b_is_active_c5_EMS_situation3_3), &c5_thisId);
  sf_mex_destroy(&c5_b_is_active_c5_EMS_situation3_3);
  return c5_y;
}

static uint8_T c5_e_emlrt_marshallIn(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  uint8_T c5_y;
  uint8_T c5_u0;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_u0, 1, 3, 0U, 0, 0U, 0);
  c5_y = c5_u0;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void init_dsm_address_info(SFc5_EMS_situation3_3InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c5_EMS_situation3_3_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(876226606U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2277805333U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(784655668U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2966181122U);
}

mxArray *sf_c5_EMS_situation3_3_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1,1,5,
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("LjhUOy8zMynWeBQeecvkwD");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(33);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,11,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,5,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,5,"type",mxType);
    }

    mxSetField(mxData,5,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,6,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,6,"type",mxType);
    }

    mxSetField(mxData,6,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,7,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,7,"type",mxType);
    }

    mxSetField(mxData,7,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,8,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,8,"type",mxType);
    }

    mxSetField(mxData,8,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,9,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,9,"type",mxType);
    }

    mxSetField(mxData,9,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,10,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,10,"type",mxType);
    }

    mxSetField(mxData,10,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c5_EMS_situation3_3_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c5_EMS_situation3_3_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c5_EMS_situation3_3(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x10'type','srcId','name','auxInfo'{{M[1],M[9],T\"deliver\",},{M[1],M[12],T\"free\",},{M[1],M[5],T\"go\",},{M[1],M[6],T\"i\",},{M[1],M[10],T\"land\",},{M[1],M[11],T\"load\",},{M[1],M[8],T\"quit\",},{M[1],M[17],T\"takeoff\",},{M[1],M[14],T\"wait\",},{M[1],M[15],T\"wait2free\",}}",
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[16],T\"wait2tunnel\",},{M[8],M[0],T\"is_active_c5_EMS_situation3_3\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 12, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c5_EMS_situation3_3_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc5_EMS_situation3_3InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc5_EMS_situation3_3InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _EMS_situation3_3MachineNumber_,
           5,
           1,
           1,
           0,
           14,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize ist own list of scripts */
        init_script_number_translation(_EMS_situation3_3MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_EMS_situation3_3MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _EMS_situation3_3MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"state");
          _SFD_SET_DATA_PROPS(1,2,0,1,"go");
          _SFD_SET_DATA_PROPS(2,1,1,0,"j");
          _SFD_SET_DATA_PROPS(3,2,0,1,"quit");
          _SFD_SET_DATA_PROPS(4,2,0,1,"deliver");
          _SFD_SET_DATA_PROPS(5,2,0,1,"land");
          _SFD_SET_DATA_PROPS(6,2,0,1,"load");
          _SFD_SET_DATA_PROPS(7,2,0,1,"free");
          _SFD_SET_DATA_PROPS(8,2,0,1,"wait");
          _SFD_SET_DATA_PROPS(9,2,0,1,"wait2free");
          _SFD_SET_DATA_PROPS(10,2,0,1,"wait2tunnel");
          _SFD_SET_DATA_PROPS(11,2,0,1,"takeoff");
          _SFD_SET_DATA_PROPS(12,2,0,1,"i");
          _SFD_SET_DATA_PROPS(13,1,1,0,"M");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,321);

        {
          unsigned int dimVector[1];
          dimVector[0]= 33;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c5_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(9,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(10,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(11,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(12,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(13,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)NULL);

        {
          real_T *c5_go;
          real_T *c5_j;
          real_T *c5_quit;
          real_T *c5_deliver;
          real_T *c5_land;
          real_T *c5_load;
          real_T *c5_free;
          real_T *c5_wait;
          real_T *c5_wait2free;
          real_T *c5_wait2tunnel;
          real_T *c5_takeoff;
          real_T *c5_i;
          real_T *c5_M;
          real_T (*c5_state)[33];
          c5_M = (real_T *)ssGetInputPortSignal(chartInstance->S, 2);
          c5_i = (real_T *)ssGetOutputPortSignal(chartInstance->S, 11);
          c5_takeoff = (real_T *)ssGetOutputPortSignal(chartInstance->S, 10);
          c5_wait2tunnel = (real_T *)ssGetOutputPortSignal(chartInstance->S, 9);
          c5_wait2free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 8);
          c5_wait = (real_T *)ssGetOutputPortSignal(chartInstance->S, 7);
          c5_free = (real_T *)ssGetOutputPortSignal(chartInstance->S, 6);
          c5_load = (real_T *)ssGetOutputPortSignal(chartInstance->S, 5);
          c5_land = (real_T *)ssGetOutputPortSignal(chartInstance->S, 4);
          c5_deliver = (real_T *)ssGetOutputPortSignal(chartInstance->S, 3);
          c5_quit = (real_T *)ssGetOutputPortSignal(chartInstance->S, 2);
          c5_j = (real_T *)ssGetInputPortSignal(chartInstance->S, 1);
          c5_go = (real_T *)ssGetOutputPortSignal(chartInstance->S, 1);
          c5_state = (real_T (*)[33])ssGetInputPortSignal(chartInstance->S, 0);
          _SFD_SET_DATA_VALUE_PTR(0U, *c5_state);
          _SFD_SET_DATA_VALUE_PTR(1U, c5_go);
          _SFD_SET_DATA_VALUE_PTR(2U, c5_j);
          _SFD_SET_DATA_VALUE_PTR(3U, c5_quit);
          _SFD_SET_DATA_VALUE_PTR(4U, c5_deliver);
          _SFD_SET_DATA_VALUE_PTR(5U, c5_land);
          _SFD_SET_DATA_VALUE_PTR(6U, c5_load);
          _SFD_SET_DATA_VALUE_PTR(7U, c5_free);
          _SFD_SET_DATA_VALUE_PTR(8U, c5_wait);
          _SFD_SET_DATA_VALUE_PTR(9U, c5_wait2free);
          _SFD_SET_DATA_VALUE_PTR(10U, c5_wait2tunnel);
          _SFD_SET_DATA_VALUE_PTR(11U, c5_takeoff);
          _SFD_SET_DATA_VALUE_PTR(12U, c5_i);
          _SFD_SET_DATA_VALUE_PTR(13U, c5_M);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _EMS_situation3_3MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "8KJ9SpELIbNISieho8ONoC";
}

static void sf_opaque_initialize_c5_EMS_situation3_3(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc5_EMS_situation3_3InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c5_EMS_situation3_3((SFc5_EMS_situation3_3InstanceStruct*)
    chartInstanceVar);
  initialize_c5_EMS_situation3_3((SFc5_EMS_situation3_3InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c5_EMS_situation3_3(void *chartInstanceVar)
{
  enable_c5_EMS_situation3_3((SFc5_EMS_situation3_3InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c5_EMS_situation3_3(void *chartInstanceVar)
{
  disable_c5_EMS_situation3_3((SFc5_EMS_situation3_3InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c5_EMS_situation3_3(void *chartInstanceVar)
{
  sf_gateway_c5_EMS_situation3_3((SFc5_EMS_situation3_3InstanceStruct*)
    chartInstanceVar);
}

extern const mxArray* sf_internal_get_sim_state_c5_EMS_situation3_3(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[4];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_raw2high");
  prhs[1] = mxCreateDoubleScalar(ssGetSFuncBlockHandle(S));
  prhs[2] = (mxArray*) get_sim_state_c5_EMS_situation3_3
    ((SFc5_EMS_situation3_3InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
  prhs[3] = (mxArray*) sf_get_sim_state_info_c5_EMS_situation3_3();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 4, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  mxDestroyArray(prhs[3]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_raw2high'.\n");
  }

  return plhs[0];
}

extern void sf_internal_set_sim_state_c5_EMS_situation3_3(SimStruct* S, const
  mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[3];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_high2raw");
  prhs[1] = mxDuplicateArray(st);      /* high level simctx */
  prhs[2] = (mxArray*) sf_get_sim_state_info_c5_EMS_situation3_3();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 3, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_high2raw'.\n");
  }

  set_sim_state_c5_EMS_situation3_3((SFc5_EMS_situation3_3InstanceStruct*)
    chartInfo->chartInstance, mxDuplicateArray(plhs[0]));
  mxDestroyArray(plhs[0]);
}

static const mxArray* sf_opaque_get_sim_state_c5_EMS_situation3_3(SimStruct* S)
{
  return sf_internal_get_sim_state_c5_EMS_situation3_3(S);
}

static void sf_opaque_set_sim_state_c5_EMS_situation3_3(SimStruct* S, const
  mxArray *st)
{
  sf_internal_set_sim_state_c5_EMS_situation3_3(S, st);
}

static void sf_opaque_terminate_c5_EMS_situation3_3(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc5_EMS_situation3_3InstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_EMS_situation3_3_optimization_info();
    }

    finalize_c5_EMS_situation3_3((SFc5_EMS_situation3_3InstanceStruct*)
      chartInstanceVar);
    utFree((void *)chartInstanceVar);
    if (crtInfo != NULL) {
      utFree((void *)crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc5_EMS_situation3_3((SFc5_EMS_situation3_3InstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c5_EMS_situation3_3(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c5_EMS_situation3_3((SFc5_EMS_situation3_3InstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c5_EMS_situation3_3(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_EMS_situation3_3_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,5);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,5,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,5,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,5);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,5,3);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,5,11);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=11; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 3; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,5);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(611174161U));
  ssSetChecksum1(S,(3987058728U));
  ssSetChecksum2(S,(2965903139U));
  ssSetChecksum3(S,(3045966005U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c5_EMS_situation3_3(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c5_EMS_situation3_3(SimStruct *S)
{
  SFc5_EMS_situation3_3InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc5_EMS_situation3_3InstanceStruct *)utMalloc(sizeof
    (SFc5_EMS_situation3_3InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc5_EMS_situation3_3InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c5_EMS_situation3_3;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c5_EMS_situation3_3;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c5_EMS_situation3_3;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c5_EMS_situation3_3;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c5_EMS_situation3_3;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c5_EMS_situation3_3;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c5_EMS_situation3_3;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c5_EMS_situation3_3;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c5_EMS_situation3_3;
  chartInstance->chartInfo.mdlStart = mdlStart_c5_EMS_situation3_3;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c5_EMS_situation3_3;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c5_EMS_situation3_3_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c5_EMS_situation3_3(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c5_EMS_situation3_3(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c5_EMS_situation3_3(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c5_EMS_situation3_3_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

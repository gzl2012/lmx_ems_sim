#ifndef __c11_EMS_situation22_test6_h__
#define __c11_EMS_situation22_test6_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc11_EMS_situation22_test6InstanceStruct
#define typedef_SFc11_EMS_situation22_test6InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c11_sfEvent;
  uint8_T c11_tp_FreeFlight;
  uint8_T c11_tp_Quit;
  uint8_T c11_tp_Standby;
  uint8_T c11_tp_Delivery;
  uint8_T c11_tp_Tunnel;
  uint8_T c11_tp_Landing;
  uint8_T c11_tp_Takeoff;
  boolean_T c11_isStable;
  uint8_T c11_is_active_c11_EMS_situation22_test6;
  uint8_T c11_is_c11_EMS_situation22_test6;
  uint8_T c11_doSetSimStateSideEffects;
  const mxArray *c11_setSimStateSideEffectsInfo;
} SFc11_EMS_situation22_test6InstanceStruct;

#endif                                 /*typedef_SFc11_EMS_situation22_test6InstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c11_EMS_situation22_test6_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c11_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
extern void c11_EMS_situation22_test6_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif

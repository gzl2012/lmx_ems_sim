/* Include files */

#include "EMS_situation22_test6_sfun.h"
#include "EMS_situation22_test6_sfun_debug_macros.h"
#include "c1_EMS_situation22_test6.h"
#include "c2_EMS_situation22_test6.h"
#include "c3_EMS_situation22_test6.h"
#include "c4_EMS_situation22_test6.h"
#include "c5_EMS_situation22_test6.h"
#include "c6_EMS_situation22_test6.h"
#include "c7_EMS_situation22_test6.h"
#include "c10_EMS_situation22_test6.h"
#include "c11_EMS_situation22_test6.h"
#include "c12_EMS_situation22_test6.h"
#include "c13_EMS_situation22_test6.h"
#include "c15_EMS_situation22_test6.h"
#include "c16_EMS_situation22_test6.h"
#include "c17_EMS_situation22_test6.h"
#include "c18_EMS_situation22_test6.h"
#include "c19_EMS_situation22_test6.h"
#include "c21_EMS_situation22_test6.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
uint32_T _EMS_situation22_test6MachineNumber_;

/* Function Declarations */

/* Function Definitions */
void EMS_situation22_test6_initializer(void)
{
}

void EMS_situation22_test6_terminator(void)
{
}

/* SFunction Glue Code */
unsigned int sf_EMS_situation22_test6_method_dispatcher(SimStruct *simstructPtr,
  unsigned int chartFileNumber, const char* specsCksum, int_T method, void *data)
{
  if (chartFileNumber==1) {
    c1_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==2) {
    c2_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==3) {
    c3_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==4) {
    c4_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==5) {
    c5_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==6) {
    c6_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==7) {
    c7_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==10) {
    c10_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==11) {
    c11_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==12) {
    c12_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==13) {
    c13_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==15) {
    c15_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==16) {
    c16_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==17) {
    c17_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==18) {
    c18_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==19) {
    c19_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==21) {
    c21_EMS_situation22_test6_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  return 0;
}

unsigned int sf_EMS_situation22_test6_process_check_sum_call( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"sf_get_check_sum"))
    return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if (nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if (!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3896145265U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3032391618U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2635421054U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(878429564U);
    } else if (!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    } else if (!strcmp(commandName,"makefile")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1613538800U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2927537111U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(751822758U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2510431826U);
    } else if (nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch (chartFileNumber) {
       case 1:
        {
          extern void sf_c1_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c1_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 2:
        {
          extern void sf_c2_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c2_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 3:
        {
          extern void sf_c3_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c3_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 4:
        {
          extern void sf_c4_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c4_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 5:
        {
          extern void sf_c5_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c5_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 6:
        {
          extern void sf_c6_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c6_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 7:
        {
          extern void sf_c7_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c7_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 10:
        {
          extern void sf_c10_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c10_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 11:
        {
          extern void sf_c11_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c11_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 12:
        {
          extern void sf_c12_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c12_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 13:
        {
          extern void sf_c13_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c13_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 15:
        {
          extern void sf_c15_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c15_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 16:
        {
          extern void sf_c16_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c16_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 17:
        {
          extern void sf_c17_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c17_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 18:
        {
          extern void sf_c18_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c18_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 19:
        {
          extern void sf_c19_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c19_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       case 21:
        {
          extern void sf_c21_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
          sf_c21_EMS_situation22_test6_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    } else if (!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3031367619U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(4001028638U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3978939492U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(838979348U);
    } else {
      return 0;
    }
  } else {
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(997617550U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2480239296U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2110050893U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1656186311U);
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_EMS_situation22_test6_autoinheritance_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[32];
  char aiChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the autoinheritance_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_autoinheritance_info"))
    return 0;
  mxGetString(prhs[2], aiChksum,sizeof(aiChksum)/sizeof(char));
  aiChksum[(sizeof(aiChksum)/sizeof(char)-1)] = '\0';

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(aiChksum, "tDVw1S9M3Oz7RTrk8yjJtF") == 0) {
          extern mxArray *sf_c1_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c1_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 2:
      {
        if (strcmp(aiChksum, "UHw7fKuiblXGkcTwzjnaYG") == 0) {
          extern mxArray *sf_c2_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c2_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 3:
      {
        if (strcmp(aiChksum, "PyU4C2qUEQUIjdTodtaDP") == 0) {
          extern mxArray *sf_c3_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c3_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 4:
      {
        if (strcmp(aiChksum, "UHw7fKuiblXGkcTwzjnaYG") == 0) {
          extern mxArray *sf_c4_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c4_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 5:
      {
        if (strcmp(aiChksum, "UHw7fKuiblXGkcTwzjnaYG") == 0) {
          extern mxArray *sf_c5_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c5_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 6:
      {
        if (strcmp(aiChksum, "SiouGZaMW7wli2oqjAtiuH") == 0) {
          extern mxArray *sf_c6_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c6_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 7:
      {
        if (strcmp(aiChksum, "PyU4C2qUEQUIjdTodtaDP") == 0) {
          extern mxArray *sf_c7_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c7_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 10:
      {
        if (strcmp(aiChksum, "UHw7fKuiblXGkcTwzjnaYG") == 0) {
          extern mxArray *sf_c10_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c10_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 11:
      {
        if (strcmp(aiChksum, "UHw7fKuiblXGkcTwzjnaYG") == 0) {
          extern mxArray *sf_c11_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c11_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 12:
      {
        if (strcmp(aiChksum, "UHw7fKuiblXGkcTwzjnaYG") == 0) {
          extern mxArray *sf_c12_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c12_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 13:
      {
        if (strcmp(aiChksum, "UHw7fKuiblXGkcTwzjnaYG") == 0) {
          extern mxArray *sf_c13_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c13_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 15:
      {
        if (strcmp(aiChksum, "SiouGZaMW7wli2oqjAtiuH") == 0) {
          extern mxArray *sf_c15_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c15_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 16:
      {
        if (strcmp(aiChksum, "UHw7fKuiblXGkcTwzjnaYG") == 0) {
          extern mxArray *sf_c16_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c16_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 17:
      {
        if (strcmp(aiChksum, "PyU4C2qUEQUIjdTodtaDP") == 0) {
          extern mxArray *sf_c17_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c17_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 18:
      {
        if (strcmp(aiChksum, "SiouGZaMW7wli2oqjAtiuH") == 0) {
          extern mxArray *sf_c18_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c18_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 19:
      {
        if (strcmp(aiChksum, "PyU4C2qUEQUIjdTodtaDP") == 0) {
          extern mxArray *sf_c19_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c19_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 21:
      {
        if (strcmp(aiChksum, "SiouGZaMW7wli2oqjAtiuH") == 0) {
          extern mxArray *sf_c21_EMS_situation22_test6_get_autoinheritance_info
            (void);
          plhs[0] = sf_c21_EMS_situation22_test6_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_EMS_situation22_test6_get_eml_resolved_functions_info( int nlhs,
  mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[64];
  if (nrhs<2 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the get_eml_resolved_functions_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_eml_resolved_functions_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        extern const mxArray
          *sf_c1_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c1_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 2:
      {
        extern const mxArray
          *sf_c2_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c2_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 3:
      {
        extern const mxArray
          *sf_c3_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c3_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 4:
      {
        extern const mxArray
          *sf_c4_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c4_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 5:
      {
        extern const mxArray
          *sf_c5_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c5_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 6:
      {
        extern const mxArray
          *sf_c6_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c6_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 7:
      {
        extern const mxArray
          *sf_c7_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c7_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 10:
      {
        extern const mxArray
          *sf_c10_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c10_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 11:
      {
        extern const mxArray
          *sf_c11_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c11_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 12:
      {
        extern const mxArray
          *sf_c12_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c12_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 13:
      {
        extern const mxArray
          *sf_c13_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c13_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 15:
      {
        extern const mxArray
          *sf_c15_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c15_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 16:
      {
        extern const mxArray
          *sf_c16_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c16_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 17:
      {
        extern const mxArray
          *sf_c17_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c17_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 18:
      {
        extern const mxArray
          *sf_c18_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c18_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 19:
      {
        extern const mxArray
          *sf_c19_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c19_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 21:
      {
        extern const mxArray
          *sf_c21_EMS_situation22_test6_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c21_EMS_situation22_test6_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_EMS_situation22_test6_third_party_uses_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the third_party_uses_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_third_party_uses_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "jolrv6yAKJqgp7NsWtvBpE") == 0) {
          extern mxArray *sf_c1_EMS_situation22_test6_third_party_uses_info(void);
          plhs[0] = sf_c1_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c2_EMS_situation22_test6_third_party_uses_info(void);
          plhs[0] = sf_c2_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "qWWrkv6oBg8b7g2cQsWASH") == 0) {
          extern mxArray *sf_c3_EMS_situation22_test6_third_party_uses_info(void);
          plhs[0] = sf_c3_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c4_EMS_situation22_test6_third_party_uses_info(void);
          plhs[0] = sf_c4_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 5:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c5_EMS_situation22_test6_third_party_uses_info(void);
          plhs[0] = sf_c5_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 6:
      {
        if (strcmp(tpChksum, "oVALpLnbvk9dpSzsz3kUzF") == 0) {
          extern mxArray *sf_c6_EMS_situation22_test6_third_party_uses_info(void);
          plhs[0] = sf_c6_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "qWWrkv6oBg8b7g2cQsWASH") == 0) {
          extern mxArray *sf_c7_EMS_situation22_test6_third_party_uses_info(void);
          plhs[0] = sf_c7_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c10_EMS_situation22_test6_third_party_uses_info
            (void);
          plhs[0] = sf_c10_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c11_EMS_situation22_test6_third_party_uses_info
            (void);
          plhs[0] = sf_c11_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c12_EMS_situation22_test6_third_party_uses_info
            (void);
          plhs[0] = sf_c12_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c13_EMS_situation22_test6_third_party_uses_info
            (void);
          plhs[0] = sf_c13_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 15:
      {
        if (strcmp(tpChksum, "oVALpLnbvk9dpSzsz3kUzF") == 0) {
          extern mxArray *sf_c15_EMS_situation22_test6_third_party_uses_info
            (void);
          plhs[0] = sf_c15_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c16_EMS_situation22_test6_third_party_uses_info
            (void);
          plhs[0] = sf_c16_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 17:
      {
        if (strcmp(tpChksum, "qWWrkv6oBg8b7g2cQsWASH") == 0) {
          extern mxArray *sf_c17_EMS_situation22_test6_third_party_uses_info
            (void);
          plhs[0] = sf_c17_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 18:
      {
        if (strcmp(tpChksum, "oVALpLnbvk9dpSzsz3kUzF") == 0) {
          extern mxArray *sf_c18_EMS_situation22_test6_third_party_uses_info
            (void);
          plhs[0] = sf_c18_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 19:
      {
        if (strcmp(tpChksum, "qWWrkv6oBg8b7g2cQsWASH") == 0) {
          extern mxArray *sf_c19_EMS_situation22_test6_third_party_uses_info
            (void);
          plhs[0] = sf_c19_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "oVALpLnbvk9dpSzsz3kUzF") == 0) {
          extern mxArray *sf_c21_EMS_situation22_test6_third_party_uses_info
            (void);
          plhs[0] = sf_c21_EMS_situation22_test6_third_party_uses_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_EMS_situation22_test6_updateBuildInfo_args_info( int nlhs,
  mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the updateBuildInfo_args_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_updateBuildInfo_args_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "jolrv6yAKJqgp7NsWtvBpE") == 0) {
          extern mxArray *sf_c1_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c1_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c2_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c2_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "qWWrkv6oBg8b7g2cQsWASH") == 0) {
          extern mxArray *sf_c3_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c3_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c4_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c4_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 5:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c5_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c5_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 6:
      {
        if (strcmp(tpChksum, "oVALpLnbvk9dpSzsz3kUzF") == 0) {
          extern mxArray *sf_c6_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c6_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "qWWrkv6oBg8b7g2cQsWASH") == 0) {
          extern mxArray *sf_c7_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c7_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c10_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c10_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c11_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c11_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c12_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c12_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c13_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c13_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 15:
      {
        if (strcmp(tpChksum, "oVALpLnbvk9dpSzsz3kUzF") == 0) {
          extern mxArray *sf_c15_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c15_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "1x5B5IrKZ5MNh86hsIxmmC") == 0) {
          extern mxArray *sf_c16_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c16_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 17:
      {
        if (strcmp(tpChksum, "qWWrkv6oBg8b7g2cQsWASH") == 0) {
          extern mxArray *sf_c17_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c17_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 18:
      {
        if (strcmp(tpChksum, "oVALpLnbvk9dpSzsz3kUzF") == 0) {
          extern mxArray *sf_c18_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c18_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 19:
      {
        if (strcmp(tpChksum, "qWWrkv6oBg8b7g2cQsWASH") == 0) {
          extern mxArray *sf_c19_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c19_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     case 21:
      {
        if (strcmp(tpChksum, "oVALpLnbvk9dpSzsz3kUzF") == 0) {
          extern mxArray *sf_c21_EMS_situation22_test6_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c21_EMS_situation22_test6_updateBuildInfo_args_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

void EMS_situation22_test6_debug_initialize(struct SfDebugInstanceStruct*
  debugInstance)
{
  _EMS_situation22_test6MachineNumber_ = sf_debug_initialize_machine
    (debugInstance,"EMS_situation22_test6","sfun",0,17,0,0,0);
  sf_debug_set_machine_event_thresholds(debugInstance,
    _EMS_situation22_test6MachineNumber_,0,0);
  sf_debug_set_machine_data_thresholds(debugInstance,
    _EMS_situation22_test6MachineNumber_,0);
}

void EMS_situation22_test6_register_exported_symbols(SimStruct* S)
{
}

static mxArray* sRtwOptimizationInfoStruct= NULL;
mxArray* load_EMS_situation22_test6_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct==NULL) {
    sRtwOptimizationInfoStruct = sf_load_rtw_optimization_info(
      "EMS_situation22_test6", "EMS_situation22_test6");
    mexMakeArrayPersistent(sRtwOptimizationInfoStruct);
  }

  return(sRtwOptimizationInfoStruct);
}

void unload_EMS_situation22_test6_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct!=NULL) {
    mxDestroyArray(sRtwOptimizationInfoStruct);
    sRtwOptimizationInfoStruct = NULL;
  }
}

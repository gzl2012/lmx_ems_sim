#ifndef __c13_EMS_situation22_test6_h__
#define __c13_EMS_situation22_test6_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc13_EMS_situation22_test6InstanceStruct
#define typedef_SFc13_EMS_situation22_test6InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c13_sfEvent;
  uint8_T c13_tp_FreeFlight;
  uint8_T c13_tp_Quit;
  uint8_T c13_tp_Standby;
  uint8_T c13_tp_Delivery;
  uint8_T c13_tp_Tunnel;
  uint8_T c13_tp_Landing;
  uint8_T c13_tp_Takeoff;
  boolean_T c13_isStable;
  uint8_T c13_is_active_c13_EMS_situation22_test6;
  uint8_T c13_is_c13_EMS_situation22_test6;
  uint8_T c13_doSetSimStateSideEffects;
  const mxArray *c13_setSimStateSideEffectsInfo;
} SFc13_EMS_situation22_test6InstanceStruct;

#endif                                 /*typedef_SFc13_EMS_situation22_test6InstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c13_EMS_situation22_test6_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c13_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
extern void c13_EMS_situation22_test6_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif

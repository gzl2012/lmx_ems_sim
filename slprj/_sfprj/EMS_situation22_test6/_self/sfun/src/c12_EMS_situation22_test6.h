#ifndef __c12_EMS_situation22_test6_h__
#define __c12_EMS_situation22_test6_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc12_EMS_situation22_test6InstanceStruct
#define typedef_SFc12_EMS_situation22_test6InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c12_sfEvent;
  uint8_T c12_tp_FreeFlight;
  uint8_T c12_tp_Quit;
  uint8_T c12_tp_Standby;
  uint8_T c12_tp_Delivery;
  uint8_T c12_tp_Tunnel;
  uint8_T c12_tp_Landing;
  uint8_T c12_tp_Takeoff;
  boolean_T c12_isStable;
  uint8_T c12_is_active_c12_EMS_situation22_test6;
  uint8_T c12_is_c12_EMS_situation22_test6;
  uint8_T c12_doSetSimStateSideEffects;
  const mxArray *c12_setSimStateSideEffectsInfo;
} SFc12_EMS_situation22_test6InstanceStruct;

#endif                                 /*typedef_SFc12_EMS_situation22_test6InstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c12_EMS_situation22_test6_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c12_EMS_situation22_test6_get_check_sum(mxArray *plhs[]);
extern void c12_EMS_situation22_test6_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif

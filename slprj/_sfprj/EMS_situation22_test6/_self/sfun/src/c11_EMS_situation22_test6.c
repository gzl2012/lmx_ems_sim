/* Include files */

#include <stddef.h>
#include "blas.h"
#include "EMS_situation22_test6_sfun.h"
#include "c11_EMS_situation22_test6.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "EMS_situation22_test6_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c11_IN_NO_ACTIVE_CHILD         ((uint8_T)0U)
#define c11_IN_Delivery                ((uint8_T)1U)
#define c11_IN_FreeFlight              ((uint8_T)2U)
#define c11_IN_Landing                 ((uint8_T)3U)
#define c11_IN_Quit                    ((uint8_T)4U)
#define c11_IN_Standby                 ((uint8_T)5U)
#define c11_IN_Takeoff                 ((uint8_T)6U)
#define c11_IN_Tunnel                  ((uint8_T)7U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;

/* Function Declarations */
static void initialize_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance);
static void initialize_params_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance);
static void enable_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance);
static void disable_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance);
static void c11_update_debugger_state_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance);
static void set_sim_state_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance, const mxArray
   *c11_st);
static void c11_set_sim_state_side_effects_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance);
static void finalize_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance);
static void sf_gateway_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance);
static void c11_chartstep_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance);
static void initSimStructsc11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c11_machineNumber, uint32_T
  c11_chartNumber, uint32_T c11_instanceNumber);
static const mxArray *c11_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static int32_T c11_emlrt_marshallIn(SFc11_EMS_situation22_test6InstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_b_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static uint8_T c11_b_emlrt_marshallIn(SFc11_EMS_situation22_test6InstanceStruct *
  chartInstance, const mxArray *c11_b_tp_FreeFlight, const char_T
  *c11_identifier);
static uint8_T c11_c_emlrt_marshallIn(SFc11_EMS_situation22_test6InstanceStruct *
  chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void c11_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_c_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static const mxArray *c11_d_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData);
static void c11_d_emlrt_marshallIn(SFc11_EMS_situation22_test6InstanceStruct
  *chartInstance, const mxArray *c11_controll_out, const char_T *c11_identifier,
  real_T c11_y[2]);
static void c11_e_emlrt_marshallIn(SFc11_EMS_situation22_test6InstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId,
  real_T c11_y[2]);
static void c11_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData);
static const mxArray *c11_f_emlrt_marshallIn
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance, const mxArray
   *c11_b_setSimStateSideEffectsInfo, const char_T *c11_identifier);
static const mxArray *c11_g_emlrt_marshallIn
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance, const mxArray
   *c11_u, const emlrtMsgIdentifier *c11_parentId);
static void init_dsm_address_info(SFc11_EMS_situation22_test6InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance)
{
  int32_T c11_i0;
  real_T (*c11_controll_out)[2];
  c11_controll_out = (real_T (*)[2])ssGetOutputPortSignal(chartInstance->S, 1);
  chartInstance->c11_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c11_doSetSimStateSideEffects = 0U;
  chartInstance->c11_setSimStateSideEffectsInfo = NULL;
  chartInstance->c11_tp_Delivery = 0U;
  chartInstance->c11_tp_FreeFlight = 0U;
  chartInstance->c11_tp_Landing = 0U;
  chartInstance->c11_tp_Quit = 0U;
  chartInstance->c11_tp_Standby = 0U;
  chartInstance->c11_tp_Takeoff = 0U;
  chartInstance->c11_tp_Tunnel = 0U;
  chartInstance->c11_is_active_c11_EMS_situation22_test6 = 0U;
  chartInstance->c11_is_c11_EMS_situation22_test6 = c11_IN_NO_ACTIVE_CHILD;
  if (!(sf_get_output_port_reusable(chartInstance->S, 1) != 0)) {
    for (c11_i0 = 0; c11_i0 < 2; c11_i0++) {
      (*c11_controll_out)[c11_i0] = 0.0;
    }
  }
}

static void initialize_params_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c11_update_debugger_state_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance)
{
  uint32_T c11_prevAniVal;
  c11_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c11_is_active_c11_EMS_situation22_test6 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 8U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_FreeFlight) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_Quit) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_Standby) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_Delivery) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_Tunnel) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_Landing) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
  }

  if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_Takeoff) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
  }

  _SFD_SET_ANIMATION(c11_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance)
{
  const mxArray *c11_st;
  const mxArray *c11_y = NULL;
  int32_T c11_i1;
  real_T c11_u[2];
  const mxArray *c11_b_y = NULL;
  uint8_T c11_hoistedGlobal;
  uint8_T c11_b_u;
  const mxArray *c11_c_y = NULL;
  uint8_T c11_b_hoistedGlobal;
  uint8_T c11_c_u;
  const mxArray *c11_d_y = NULL;
  real_T (*c11_controll_out)[2];
  c11_controll_out = (real_T (*)[2])ssGetOutputPortSignal(chartInstance->S, 1);
  c11_st = NULL;
  c11_st = NULL;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_createcellmatrix(3, 1), false);
  for (c11_i1 = 0; c11_i1 < 2; c11_i1++) {
    c11_u[c11_i1] = (*c11_controll_out)[c11_i1];
  }

  c11_b_y = NULL;
  sf_mex_assign(&c11_b_y, sf_mex_create("y", c11_u, 0, 0U, 1U, 0U, 2, 2, 1),
                false);
  sf_mex_setcell(c11_y, 0, c11_b_y);
  c11_hoistedGlobal = chartInstance->c11_is_active_c11_EMS_situation22_test6;
  c11_b_u = c11_hoistedGlobal;
  c11_c_y = NULL;
  sf_mex_assign(&c11_c_y, sf_mex_create("y", &c11_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 1, c11_c_y);
  c11_b_hoistedGlobal = chartInstance->c11_is_c11_EMS_situation22_test6;
  c11_c_u = c11_b_hoistedGlobal;
  c11_d_y = NULL;
  sf_mex_assign(&c11_d_y, sf_mex_create("y", &c11_c_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c11_y, 2, c11_d_y);
  sf_mex_assign(&c11_st, c11_y, false);
  return c11_st;
}

static void set_sim_state_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance, const mxArray
   *c11_st)
{
  const mxArray *c11_u;
  real_T c11_dv0[2];
  int32_T c11_i2;
  real_T (*c11_controll_out)[2];
  c11_controll_out = (real_T (*)[2])ssGetOutputPortSignal(chartInstance->S, 1);
  c11_u = sf_mex_dup(c11_st);
  c11_d_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c11_u, 0)),
    "controll_out", c11_dv0);
  for (c11_i2 = 0; c11_i2 < 2; c11_i2++) {
    (*c11_controll_out)[c11_i2] = c11_dv0[c11_i2];
  }

  chartInstance->c11_is_active_c11_EMS_situation22_test6 =
    c11_b_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c11_u, 1)),
    "is_active_c11_EMS_situation22_test6");
  chartInstance->c11_is_c11_EMS_situation22_test6 = c11_b_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c11_u, 2)),
     "is_c11_EMS_situation22_test6");
  sf_mex_assign(&chartInstance->c11_setSimStateSideEffectsInfo,
                c11_f_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c11_u, 3)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c11_u);
  chartInstance->c11_doSetSimStateSideEffects = 1U;
  c11_update_debugger_state_c11_EMS_situation22_test6(chartInstance);
  sf_mex_destroy(&c11_st);
}

static void c11_set_sim_state_side_effects_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance)
{
  if (chartInstance->c11_doSetSimStateSideEffects != 0) {
    if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_Delivery) {
      chartInstance->c11_tp_Delivery = 1U;
    } else {
      chartInstance->c11_tp_Delivery = 0U;
    }

    if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_FreeFlight) {
      chartInstance->c11_tp_FreeFlight = 1U;
    } else {
      chartInstance->c11_tp_FreeFlight = 0U;
    }

    if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_Landing) {
      chartInstance->c11_tp_Landing = 1U;
    } else {
      chartInstance->c11_tp_Landing = 0U;
    }

    if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_Quit) {
      chartInstance->c11_tp_Quit = 1U;
    } else {
      chartInstance->c11_tp_Quit = 0U;
    }

    if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_Standby) {
      chartInstance->c11_tp_Standby = 1U;
    } else {
      chartInstance->c11_tp_Standby = 0U;
    }

    if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_Takeoff) {
      chartInstance->c11_tp_Takeoff = 1U;
    } else {
      chartInstance->c11_tp_Takeoff = 0U;
    }

    if (chartInstance->c11_is_c11_EMS_situation22_test6 == c11_IN_Tunnel) {
      chartInstance->c11_tp_Tunnel = 1U;
    } else {
      chartInstance->c11_tp_Tunnel = 0U;
    }

    chartInstance->c11_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c11_setSimStateSideEffectsInfo);
}

static void sf_gateway_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance)
{
  int32_T c11_i3;
  real_T *c11_go;
  real_T *c11_quit;
  real_T *c11_deliver;
  real_T *c11_land;
  real_T *c11_load;
  real_T *c11_free;
  real_T *c11_wait;
  real_T *c11_wait2free;
  real_T *c11_wait2tunnel;
  real_T *c11_takeoff;
  real_T *c11_i;
  real_T *c11_g;
  real_T (*c11_controll_out)[2];
  c11_g = (real_T *)ssGetInputPortSignal(chartInstance->S, 11);
  c11_i = (real_T *)ssGetInputPortSignal(chartInstance->S, 10);
  c11_takeoff = (real_T *)ssGetInputPortSignal(chartInstance->S, 9);
  c11_wait2tunnel = (real_T *)ssGetInputPortSignal(chartInstance->S, 8);
  c11_wait2free = (real_T *)ssGetInputPortSignal(chartInstance->S, 7);
  c11_wait = (real_T *)ssGetInputPortSignal(chartInstance->S, 6);
  c11_controll_out = (real_T (*)[2])ssGetOutputPortSignal(chartInstance->S, 1);
  c11_free = (real_T *)ssGetInputPortSignal(chartInstance->S, 5);
  c11_load = (real_T *)ssGetInputPortSignal(chartInstance->S, 4);
  c11_land = (real_T *)ssGetInputPortSignal(chartInstance->S, 3);
  c11_deliver = (real_T *)ssGetInputPortSignal(chartInstance->S, 2);
  c11_quit = (real_T *)ssGetInputPortSignal(chartInstance->S, 1);
  c11_go = (real_T *)ssGetInputPortSignal(chartInstance->S, 0);
  c11_set_sim_state_side_effects_c11_EMS_situation22_test6(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
  _SFD_DATA_RANGE_CHECK(*c11_go, 0U);
  _SFD_DATA_RANGE_CHECK(*c11_quit, 1U);
  _SFD_DATA_RANGE_CHECK(*c11_deliver, 2U);
  _SFD_DATA_RANGE_CHECK(*c11_land, 3U);
  _SFD_DATA_RANGE_CHECK(*c11_load, 4U);
  _SFD_DATA_RANGE_CHECK(*c11_free, 5U);
  for (c11_i3 = 0; c11_i3 < 2; c11_i3++) {
    _SFD_DATA_RANGE_CHECK((*c11_controll_out)[c11_i3], 6U);
  }

  _SFD_DATA_RANGE_CHECK(*c11_wait, 7U);
  _SFD_DATA_RANGE_CHECK(*c11_wait2free, 8U);
  _SFD_DATA_RANGE_CHECK(*c11_wait2tunnel, 9U);
  _SFD_DATA_RANGE_CHECK(*c11_takeoff, 10U);
  _SFD_DATA_RANGE_CHECK(*c11_i, 11U);
  _SFD_DATA_RANGE_CHECK(*c11_g, 12U);
  chartInstance->c11_sfEvent = CALL_EVENT;
  c11_chartstep_c11_EMS_situation22_test6(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_EMS_situation22_test6MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void c11_chartstep_c11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance)
{
  boolean_T c11_out;
  real_T c11_dv1[2];
  int32_T c11_i4;
  int32_T c11_i5;
  boolean_T c11_b_out;
  boolean_T c11_c_out;
  real_T c11_dv2[2];
  int32_T c11_i6;
  int32_T c11_i7;
  boolean_T c11_d_out;
  real_T c11_dv3[2];
  int32_T c11_i8;
  int32_T c11_i9;
  boolean_T c11_e_out;
  real_T c11_dv4[2];
  int32_T c11_i10;
  int32_T c11_i11;
  boolean_T c11_f_out;
  real_T c11_dv5[2];
  int32_T c11_i12;
  int32_T c11_i13;
  boolean_T c11_g_out;
  real_T c11_dv6[2];
  int32_T c11_i14;
  int32_T c11_i15;
  boolean_T c11_h_out;
  boolean_T c11_i_out;
  real_T c11_dv7[2];
  int32_T c11_i16;
  int32_T c11_i17;
  real_T *c11_load;
  real_T *c11_i;
  real_T *c11_g;
  real_T *c11_deliver;
  real_T *c11_land;
  real_T *c11_quit;
  real_T *c11_takeoff;
  real_T *c11_wait2tunnel;
  real_T *c11_go;
  real_T *c11_wait;
  real_T *c11_free;
  real_T (*c11_controll_out)[2];
  c11_g = (real_T *)ssGetInputPortSignal(chartInstance->S, 11);
  c11_i = (real_T *)ssGetInputPortSignal(chartInstance->S, 10);
  c11_takeoff = (real_T *)ssGetInputPortSignal(chartInstance->S, 9);
  c11_wait2tunnel = (real_T *)ssGetInputPortSignal(chartInstance->S, 8);
  c11_wait = (real_T *)ssGetInputPortSignal(chartInstance->S, 6);
  c11_controll_out = (real_T (*)[2])ssGetOutputPortSignal(chartInstance->S, 1);
  c11_free = (real_T *)ssGetInputPortSignal(chartInstance->S, 5);
  c11_load = (real_T *)ssGetInputPortSignal(chartInstance->S, 4);
  c11_land = (real_T *)ssGetInputPortSignal(chartInstance->S, 3);
  c11_deliver = (real_T *)ssGetInputPortSignal(chartInstance->S, 2);
  c11_quit = (real_T *)ssGetInputPortSignal(chartInstance->S, 1);
  c11_go = (real_T *)ssGetInputPortSignal(chartInstance->S, 0);
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
  if (chartInstance->c11_is_active_c11_EMS_situation22_test6 == 0U) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
    chartInstance->c11_is_active_c11_EMS_situation22_test6 = 1U;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
    chartInstance->c11_is_c11_EMS_situation22_test6 = c11_IN_Quit;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c11_sfEvent);
    chartInstance->c11_tp_Quit = 1U;
  } else {
    switch (chartInstance->c11_is_c11_EMS_situation22_test6) {
     case c11_IN_Delivery:
      CV_CHART_EVAL(8, 0, 1);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                   chartInstance->c11_sfEvent);
      c11_out = (CV_TRANSITION_EVAL(4U, (int32_T)_SFD_CCP_CALL(4U, 0, *c11_load ==
        0.0 != 0U, chartInstance->c11_sfEvent)) != 0);
      if (c11_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Delivery = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_c11_EMS_situation22_test6 = c11_IN_Tunnel;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Tunnel = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c11_sfEvent);
        sf_mex_import_named("Scontroller", sf_mex_call_debug
                            (sfGlobalDebugInstanceStruct, "Scontroller", 1U, 2U,
                             6, *c11_i, 6, *c11_g), c11_dv1, 0, 0, 0U, 1, 0U, 2,
                            2, 1);
        for (c11_i4 = 0; c11_i4 < 2; c11_i4++) {
          (*c11_controll_out)[c11_i4] = c11_dv1[c11_i4];
        }

        for (c11_i5 = 0; c11_i5 < 2; c11_i5++) {
          _SFD_DATA_RANGE_CHECK((*c11_controll_out)[c11_i5], 6U);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c11_sfEvent);
      break;

     case c11_IN_FreeFlight:
      CV_CHART_EVAL(8, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 5U,
                   chartInstance->c11_sfEvent);
      c11_b_out = (CV_TRANSITION_EVAL(5U, (int32_T)_SFD_CCP_CALL(5U, 0,
        *c11_deliver == 1.0 != 0U, chartInstance->c11_sfEvent)) != 0);
      if (c11_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_FreeFlight = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_c11_EMS_situation22_test6 = c11_IN_Delivery;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Delivery = 1U;
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 8U,
                     chartInstance->c11_sfEvent);
        c11_c_out = (CV_TRANSITION_EVAL(8U, (int32_T)_SFD_CCP_CALL(8U, 0,
          *c11_land == 1.0 != 0U, chartInstance->c11_sfEvent)) != 0);
        if (c11_c_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 8U, chartInstance->c11_sfEvent);
          chartInstance->c11_tp_FreeFlight = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
          chartInstance->c11_is_c11_EMS_situation22_test6 = c11_IN_Landing;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
          chartInstance->c11_tp_Landing = 1U;
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                       chartInstance->c11_sfEvent);
          sf_mex_import_named("FFcontroller", sf_mex_call_debug
                              (sfGlobalDebugInstanceStruct, "FFcontroller", 1U,
                               2U, 6, *c11_i, 6, *c11_g), c11_dv2, 0, 0, 0U, 1,
                              0U, 2, 2, 1);
          for (c11_i6 = 0; c11_i6 < 2; c11_i6++) {
            (*c11_controll_out)[c11_i6] = c11_dv2[c11_i6];
          }

          for (c11_i7 = 0; c11_i7 < 2; c11_i7++) {
            _SFD_DATA_RANGE_CHECK((*c11_controll_out)[c11_i7], 6U);
          }
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c11_sfEvent);
      break;

     case c11_IN_Landing:
      CV_CHART_EVAL(8, 0, 3);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 9U,
                   chartInstance->c11_sfEvent);
      c11_d_out = (CV_TRANSITION_EVAL(9U, (int32_T)_SFD_CCP_CALL(9U, 0,
        *c11_quit == 1.0 != 0U, chartInstance->c11_sfEvent)) != 0);
      if (c11_d_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 9U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Landing = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_c11_EMS_situation22_test6 = c11_IN_Quit;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Quit = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                     chartInstance->c11_sfEvent);
        sf_mex_import_named("Scontroller", sf_mex_call_debug
                            (sfGlobalDebugInstanceStruct, "Scontroller", 1U, 2U,
                             6, *c11_i, 6, *c11_g), c11_dv3, 0, 0, 0U, 1, 0U, 2,
                            2, 1);
        for (c11_i8 = 0; c11_i8 < 2; c11_i8++) {
          (*c11_controll_out)[c11_i8] = c11_dv3[c11_i8];
        }

        for (c11_i9 = 0; c11_i9 < 2; c11_i9++) {
          _SFD_DATA_RANGE_CHECK((*c11_controll_out)[c11_i9], 6U);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c11_sfEvent);
      break;

     case c11_IN_Quit:
      CV_CHART_EVAL(8, 0, 4);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                   chartInstance->c11_sfEvent);
      c11_e_out = (CV_TRANSITION_EVAL(1U, (int32_T)_SFD_CCP_CALL(1U, 0,
        *c11_takeoff == 1.0 != 0U, chartInstance->c11_sfEvent)) != 0);
      if (c11_e_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Quit = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_c11_EMS_situation22_test6 = c11_IN_Takeoff;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Takeoff = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U,
                     chartInstance->c11_sfEvent);
        sf_mex_import_named("Scontroller", sf_mex_call_debug
                            (sfGlobalDebugInstanceStruct, "Scontroller", 1U, 2U,
                             6, *c11_i, 6, *c11_g), c11_dv4, 0, 0, 0U, 1, 0U, 2,
                            2, 1);
        for (c11_i10 = 0; c11_i10 < 2; c11_i10++) {
          (*c11_controll_out)[c11_i10] = c11_dv4[c11_i10];
        }

        for (c11_i11 = 0; c11_i11 < 2; c11_i11++) {
          _SFD_DATA_RANGE_CHECK((*c11_controll_out)[c11_i11], 6U);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c11_sfEvent);
      break;

     case c11_IN_Standby:
      CV_CHART_EVAL(8, 0, 5);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 3U,
                   chartInstance->c11_sfEvent);
      c11_f_out = (CV_TRANSITION_EVAL(3U, (int32_T)_SFD_CCP_CALL(3U, 0,
        *c11_wait2tunnel == 1.0 != 0U, chartInstance->c11_sfEvent)) != 0);
      if (c11_f_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Standby = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_c11_EMS_situation22_test6 = c11_IN_Tunnel;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Tunnel = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U,
                     chartInstance->c11_sfEvent);
        sf_mex_import_named("Wcontroller", sf_mex_call_debug
                            (sfGlobalDebugInstanceStruct, "Wcontroller", 1U, 2U,
                             6, *c11_i, 6, *c11_g), c11_dv5, 0, 0, 0U, 1, 0U, 2,
                            2, 1);
        for (c11_i12 = 0; c11_i12 < 2; c11_i12++) {
          (*c11_controll_out)[c11_i12] = c11_dv5[c11_i12];
        }

        for (c11_i13 = 0; c11_i13 < 2; c11_i13++) {
          _SFD_DATA_RANGE_CHECK((*c11_controll_out)[c11_i13], 6U);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c11_sfEvent);
      break;

     case c11_IN_Takeoff:
      CV_CHART_EVAL(8, 0, 6);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 7U,
                   chartInstance->c11_sfEvent);
      c11_g_out = (CV_TRANSITION_EVAL(7U, (int32_T)_SFD_CCP_CALL(7U, 0, *c11_go ==
        1.0 != 0U, chartInstance->c11_sfEvent)) != 0);
      if (c11_g_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 7U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Takeoff = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_c11_EMS_situation22_test6 = c11_IN_Tunnel;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Tunnel = 1U;
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U,
                     chartInstance->c11_sfEvent);
        sf_mex_import_named("Scontroller", sf_mex_call_debug
                            (sfGlobalDebugInstanceStruct, "Scontroller", 1U, 2U,
                             6, *c11_i, 6, *c11_g), c11_dv6, 0, 0, 0U, 1, 0U, 2,
                            2, 1);
        for (c11_i14 = 0; c11_i14 < 2; c11_i14++) {
          (*c11_controll_out)[c11_i14] = c11_dv6[c11_i14];
        }

        for (c11_i15 = 0; c11_i15 < 2; c11_i15++) {
          _SFD_DATA_RANGE_CHECK((*c11_controll_out)[c11_i15], 6U);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c11_sfEvent);
      break;

     case c11_IN_Tunnel:
      CV_CHART_EVAL(8, 0, 7);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                   chartInstance->c11_sfEvent);
      c11_h_out = (CV_TRANSITION_EVAL(2U, (int32_T)_SFD_CCP_CALL(2U, 0,
        *c11_wait == 1.0 != 0U, chartInstance->c11_sfEvent)) != 0);
      if (c11_h_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Tunnel = 0U;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
        chartInstance->c11_is_c11_EMS_situation22_test6 = c11_IN_Standby;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c11_sfEvent);
        chartInstance->c11_tp_Standby = 1U;
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 6U,
                     chartInstance->c11_sfEvent);
        c11_i_out = (CV_TRANSITION_EVAL(6U, (int32_T)_SFD_CCP_CALL(6U, 0,
          *c11_free == 1.0 != 0U, chartInstance->c11_sfEvent)) != 0);
        if (c11_i_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
          chartInstance->c11_tp_Tunnel = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c11_sfEvent);
          chartInstance->c11_is_c11_EMS_situation22_test6 = c11_IN_FreeFlight;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c11_sfEvent);
          chartInstance->c11_tp_FreeFlight = 1U;
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 6U,
                       chartInstance->c11_sfEvent);
          sf_mex_import_named("Tcontroller", sf_mex_call_debug
                              (sfGlobalDebugInstanceStruct, "Tcontroller", 1U,
                               2U, 6, *c11_i, 6, *c11_g), c11_dv7, 0, 0, 0U, 1,
                              0U, 2, 2, 1);
          for (c11_i16 = 0; c11_i16 < 2; c11_i16++) {
            (*c11_controll_out)[c11_i16] = c11_dv7[c11_i16];
          }

          for (c11_i17 = 0; c11_i17 < 2; c11_i17++) {
            _SFD_DATA_RANGE_CHECK((*c11_controll_out)[c11_i17], 6U);
          }
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c11_sfEvent);
      break;

     default:
      CV_CHART_EVAL(8, 0, 0);
      chartInstance->c11_is_c11_EMS_situation22_test6 = c11_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c11_sfEvent);
      break;
    }
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c11_sfEvent);
}

static void initSimStructsc11_EMS_situation22_test6
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c11_machineNumber, uint32_T
  c11_chartNumber, uint32_T c11_instanceNumber)
{
  (void)c11_machineNumber;
  (void)c11_chartNumber;
  (void)c11_instanceNumber;
}

const mxArray *sf_c11_EMS_situation22_test6_get_eml_resolved_functions_info(void)
{
  const mxArray *c11_nameCaptureInfo = NULL;
  c11_nameCaptureInfo = NULL;
  sf_mex_assign(&c11_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c11_nameCaptureInfo;
}

static const mxArray *c11_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  int32_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_EMS_situation22_test6InstanceStruct *chartInstance;
  chartInstance = (SFc11_EMS_situation22_test6InstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(int32_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static int32_T c11_emlrt_marshallIn(SFc11_EMS_situation22_test6InstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  int32_T c11_y;
  int32_T c11_i18;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_i18, 1, 6, 0U, 0, 0U, 0);
  c11_y = c11_i18;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_b_sfEvent;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  int32_T c11_y;
  SFc11_EMS_situation22_test6InstanceStruct *chartInstance;
  chartInstance = (SFc11_EMS_situation22_test6InstanceStruct *)chartInstanceVoid;
  c11_b_sfEvent = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_sfEvent),
    &c11_thisId);
  sf_mex_destroy(&c11_b_sfEvent);
  *(int32_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_b_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  uint8_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_EMS_situation22_test6InstanceStruct *chartInstance;
  chartInstance = (SFc11_EMS_situation22_test6InstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(uint8_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static uint8_T c11_b_emlrt_marshallIn(SFc11_EMS_situation22_test6InstanceStruct *
  chartInstance, const mxArray *c11_b_tp_FreeFlight, const char_T
  *c11_identifier)
{
  uint8_T c11_y;
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_tp_FreeFlight),
    &c11_thisId);
  sf_mex_destroy(&c11_b_tp_FreeFlight);
  return c11_y;
}

static uint8_T c11_c_emlrt_marshallIn(SFc11_EMS_situation22_test6InstanceStruct *
  chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  uint8_T c11_y;
  uint8_T c11_u0;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), &c11_u0, 1, 3, 0U, 0, 0U, 0);
  c11_y = c11_u0;
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void c11_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_b_tp_FreeFlight;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  uint8_T c11_y;
  SFc11_EMS_situation22_test6InstanceStruct *chartInstance;
  chartInstance = (SFc11_EMS_situation22_test6InstanceStruct *)chartInstanceVoid;
  c11_b_tp_FreeFlight = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_y = c11_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_b_tp_FreeFlight),
    &c11_thisId);
  sf_mex_destroy(&c11_b_tp_FreeFlight);
  *(uint8_T *)c11_outData = c11_y;
  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_c_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  real_T c11_u;
  const mxArray *c11_y = NULL;
  SFc11_EMS_situation22_test6InstanceStruct *chartInstance;
  chartInstance = (SFc11_EMS_situation22_test6InstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  c11_u = *(real_T *)c11_inData;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", &c11_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static const mxArray *c11_d_sf_marshallOut(void *chartInstanceVoid, void
  *c11_inData)
{
  const mxArray *c11_mxArrayOutData = NULL;
  int32_T c11_i19;
  real_T c11_b_inData[2];
  int32_T c11_i20;
  real_T c11_u[2];
  const mxArray *c11_y = NULL;
  SFc11_EMS_situation22_test6InstanceStruct *chartInstance;
  chartInstance = (SFc11_EMS_situation22_test6InstanceStruct *)chartInstanceVoid;
  c11_mxArrayOutData = NULL;
  for (c11_i19 = 0; c11_i19 < 2; c11_i19++) {
    c11_b_inData[c11_i19] = (*(real_T (*)[2])c11_inData)[c11_i19];
  }

  for (c11_i20 = 0; c11_i20 < 2; c11_i20++) {
    c11_u[c11_i20] = c11_b_inData[c11_i20];
  }

  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_create("y", c11_u, 0, 0U, 1U, 0U, 2, 2, 1), false);
  sf_mex_assign(&c11_mxArrayOutData, c11_y, false);
  return c11_mxArrayOutData;
}

static void c11_d_emlrt_marshallIn(SFc11_EMS_situation22_test6InstanceStruct
  *chartInstance, const mxArray *c11_controll_out, const char_T *c11_identifier,
  real_T c11_y[2])
{
  emlrtMsgIdentifier c11_thisId;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_controll_out),
    &c11_thisId, c11_y);
  sf_mex_destroy(&c11_controll_out);
}

static void c11_e_emlrt_marshallIn(SFc11_EMS_situation22_test6InstanceStruct
  *chartInstance, const mxArray *c11_u, const emlrtMsgIdentifier *c11_parentId,
  real_T c11_y[2])
{
  real_T c11_dv8[2];
  int32_T c11_i21;
  (void)chartInstance;
  sf_mex_import(c11_parentId, sf_mex_dup(c11_u), c11_dv8, 1, 0, 0U, 1, 0U, 2, 2,
                1);
  for (c11_i21 = 0; c11_i21 < 2; c11_i21++) {
    c11_y[c11_i21] = c11_dv8[c11_i21];
  }

  sf_mex_destroy(&c11_u);
}

static void c11_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c11_mxArrayInData, const char_T *c11_varName, void *c11_outData)
{
  const mxArray *c11_controll_out;
  const char_T *c11_identifier;
  emlrtMsgIdentifier c11_thisId;
  real_T c11_y[2];
  int32_T c11_i22;
  SFc11_EMS_situation22_test6InstanceStruct *chartInstance;
  chartInstance = (SFc11_EMS_situation22_test6InstanceStruct *)chartInstanceVoid;
  c11_controll_out = sf_mex_dup(c11_mxArrayInData);
  c11_identifier = c11_varName;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  c11_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c11_controll_out),
    &c11_thisId, c11_y);
  sf_mex_destroy(&c11_controll_out);
  for (c11_i22 = 0; c11_i22 < 2; c11_i22++) {
    (*(real_T (*)[2])c11_outData)[c11_i22] = c11_y[c11_i22];
  }

  sf_mex_destroy(&c11_mxArrayInData);
}

static const mxArray *c11_f_emlrt_marshallIn
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance, const mxArray
   *c11_b_setSimStateSideEffectsInfo, const char_T *c11_identifier)
{
  const mxArray *c11_y = NULL;
  emlrtMsgIdentifier c11_thisId;
  c11_y = NULL;
  c11_thisId.fIdentifier = c11_identifier;
  c11_thisId.fParent = NULL;
  sf_mex_assign(&c11_y, c11_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c11_b_setSimStateSideEffectsInfo), &c11_thisId), false);
  sf_mex_destroy(&c11_b_setSimStateSideEffectsInfo);
  return c11_y;
}

static const mxArray *c11_g_emlrt_marshallIn
  (SFc11_EMS_situation22_test6InstanceStruct *chartInstance, const mxArray
   *c11_u, const emlrtMsgIdentifier *c11_parentId)
{
  const mxArray *c11_y = NULL;
  (void)chartInstance;
  (void)c11_parentId;
  c11_y = NULL;
  sf_mex_assign(&c11_y, sf_mex_duplicatearraysafe(&c11_u), false);
  sf_mex_destroy(&c11_u);
  return c11_y;
}

static void init_dsm_address_info(SFc11_EMS_situation22_test6InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c11_EMS_situation22_test6_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1177298382U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3039667953U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2666254169U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1296056964U);
}

mxArray *sf_c11_EMS_situation22_test6_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1,1,5,
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("UHw7fKuiblXGkcTwzjnaYG");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,12,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,5,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,5,"type",mxType);
    }

    mxSetField(mxData,5,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,6,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,6,"type",mxType);
    }

    mxSetField(mxData,6,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,7,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,7,"type",mxType);
    }

    mxSetField(mxData,7,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,8,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,8,"type",mxType);
    }

    mxSetField(mxData,8,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,9,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,9,"type",mxType);
    }

    mxSetField(mxData,9,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,10,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,10,"type",mxType);
    }

    mxSetField(mxData,10,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,11,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,11,"type",mxType);
    }

    mxSetField(mxData,11,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(2);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c11_EMS_situation22_test6_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c11_EMS_situation22_test6_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c11_EMS_situation22_test6(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x3'type','srcId','name','auxInfo'{{M[1],M[24],T\"controll_out\",},{M[8],M[0],T\"is_active_c11_EMS_situation22_test6\",},{M[9],M[0],T\"is_c11_EMS_situation22_test6\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 3, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c11_EMS_situation22_test6_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc11_EMS_situation22_test6InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc11_EMS_situation22_test6InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _EMS_situation22_test6MachineNumber_,
           11,
           7,
           10,
           0,
           13,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize ist own list of scripts */
        init_script_number_translation(_EMS_situation22_test6MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_EMS_situation22_test6MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _EMS_situation22_test6MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"go");
          _SFD_SET_DATA_PROPS(1,1,1,0,"quit");
          _SFD_SET_DATA_PROPS(2,1,1,0,"deliver");
          _SFD_SET_DATA_PROPS(3,1,1,0,"land");
          _SFD_SET_DATA_PROPS(4,1,1,0,"load");
          _SFD_SET_DATA_PROPS(5,1,1,0,"free");
          _SFD_SET_DATA_PROPS(6,2,0,1,"controll_out");
          _SFD_SET_DATA_PROPS(7,1,1,0,"wait");
          _SFD_SET_DATA_PROPS(8,1,1,0,"wait2free");
          _SFD_SET_DATA_PROPS(9,1,1,0,"wait2tunnel");
          _SFD_SET_DATA_PROPS(10,1,1,0,"takeoff");
          _SFD_SET_DATA_PROPS(11,1,1,0,"i");
          _SFD_SET_DATA_PROPS(12,1,1,0,"g");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(3,0,0);
          _SFD_STATE_INFO(4,0,0);
          _SFD_STATE_INFO(5,0,0);
          _SFD_STATE_INFO(6,0,0);
          _SFD_CH_SUBSTATE_COUNT(7);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_CH_SUBSTATE_INDEX(2,2);
          _SFD_CH_SUBSTATE_INDEX(3,3);
          _SFD_CH_SUBSTATE_INDEX(4,4);
          _SFD_CH_SUBSTATE_INDEX(5,5);
          _SFD_CH_SUBSTATE_INDEX(6,6);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,0);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(3,0);
          _SFD_ST_SUBSTATE_COUNT(4,0);
          _SFD_ST_SUBSTATE_COUNT(5,0);
          _SFD_ST_SUBSTATE_COUNT(6,0);
        }

        _SFD_CV_INIT_CHART(7,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(4,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(5,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(6,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 15 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(3,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 11 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(1,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 6 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(7,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 8 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(2,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 8 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(4,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 8 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(6,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 8 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(9,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 8 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(8,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        {
          static unsigned int sStartGuardMap[] = { 1 };

          static unsigned int sEndGuardMap[] = { 11 };

          static int sPostFixPredicateTree[] = { 0 };

          _SFD_CV_INIT_TRANS(5,1,&(sStartGuardMap[0]),&(sEndGuardMap[0]),1,
                             &(sPostFixPredicateTree[0]));
        }

        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_c_sf_marshallOut,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[2];
          dimVector[0]= 2;
          dimVector[1]= 1;
          _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c11_d_sf_marshallOut,(MexInFcnForType)
            c11_c_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(9,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(10,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(11,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(12,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c11_c_sf_marshallOut,(MexInFcnForType)NULL);

        {
          real_T *c11_go;
          real_T *c11_quit;
          real_T *c11_deliver;
          real_T *c11_land;
          real_T *c11_load;
          real_T *c11_free;
          real_T *c11_wait;
          real_T *c11_wait2free;
          real_T *c11_wait2tunnel;
          real_T *c11_takeoff;
          real_T *c11_i;
          real_T *c11_g;
          real_T (*c11_controll_out)[2];
          c11_g = (real_T *)ssGetInputPortSignal(chartInstance->S, 11);
          c11_i = (real_T *)ssGetInputPortSignal(chartInstance->S, 10);
          c11_takeoff = (real_T *)ssGetInputPortSignal(chartInstance->S, 9);
          c11_wait2tunnel = (real_T *)ssGetInputPortSignal(chartInstance->S, 8);
          c11_wait2free = (real_T *)ssGetInputPortSignal(chartInstance->S, 7);
          c11_wait = (real_T *)ssGetInputPortSignal(chartInstance->S, 6);
          c11_controll_out = (real_T (*)[2])ssGetOutputPortSignal
            (chartInstance->S, 1);
          c11_free = (real_T *)ssGetInputPortSignal(chartInstance->S, 5);
          c11_load = (real_T *)ssGetInputPortSignal(chartInstance->S, 4);
          c11_land = (real_T *)ssGetInputPortSignal(chartInstance->S, 3);
          c11_deliver = (real_T *)ssGetInputPortSignal(chartInstance->S, 2);
          c11_quit = (real_T *)ssGetInputPortSignal(chartInstance->S, 1);
          c11_go = (real_T *)ssGetInputPortSignal(chartInstance->S, 0);
          _SFD_SET_DATA_VALUE_PTR(0U, c11_go);
          _SFD_SET_DATA_VALUE_PTR(1U, c11_quit);
          _SFD_SET_DATA_VALUE_PTR(2U, c11_deliver);
          _SFD_SET_DATA_VALUE_PTR(3U, c11_land);
          _SFD_SET_DATA_VALUE_PTR(4U, c11_load);
          _SFD_SET_DATA_VALUE_PTR(5U, c11_free);
          _SFD_SET_DATA_VALUE_PTR(6U, *c11_controll_out);
          _SFD_SET_DATA_VALUE_PTR(7U, c11_wait);
          _SFD_SET_DATA_VALUE_PTR(8U, c11_wait2free);
          _SFD_SET_DATA_VALUE_PTR(9U, c11_wait2tunnel);
          _SFD_SET_DATA_VALUE_PTR(10U, c11_takeoff);
          _SFD_SET_DATA_VALUE_PTR(11U, c11_i);
          _SFD_SET_DATA_VALUE_PTR(12U, c11_g);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _EMS_situation22_test6MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "1x5B5IrKZ5MNh86hsIxmmC";
}

static void sf_opaque_initialize_c11_EMS_situation22_test6(void
  *chartInstanceVar)
{
  chart_debug_initialization(((SFc11_EMS_situation22_test6InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c11_EMS_situation22_test6
    ((SFc11_EMS_situation22_test6InstanceStruct*) chartInstanceVar);
  initialize_c11_EMS_situation22_test6
    ((SFc11_EMS_situation22_test6InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c11_EMS_situation22_test6(void *chartInstanceVar)
{
  enable_c11_EMS_situation22_test6((SFc11_EMS_situation22_test6InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c11_EMS_situation22_test6(void *chartInstanceVar)
{
  disable_c11_EMS_situation22_test6((SFc11_EMS_situation22_test6InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c11_EMS_situation22_test6(void *chartInstanceVar)
{
  sf_gateway_c11_EMS_situation22_test6
    ((SFc11_EMS_situation22_test6InstanceStruct*) chartInstanceVar);
}

extern const mxArray* sf_internal_get_sim_state_c11_EMS_situation22_test6
  (SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[4];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_raw2high");
  prhs[1] = mxCreateDoubleScalar(ssGetSFuncBlockHandle(S));
  prhs[2] = (mxArray*) get_sim_state_c11_EMS_situation22_test6
    ((SFc11_EMS_situation22_test6InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
  prhs[3] = (mxArray*) sf_get_sim_state_info_c11_EMS_situation22_test6();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 4, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  mxDestroyArray(prhs[3]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_raw2high'.\n");
  }

  return plhs[0];
}

extern void sf_internal_set_sim_state_c11_EMS_situation22_test6(SimStruct* S,
  const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[3];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_high2raw");
  prhs[1] = mxDuplicateArray(st);      /* high level simctx */
  prhs[2] = (mxArray*) sf_get_sim_state_info_c11_EMS_situation22_test6();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 3, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_high2raw'.\n");
  }

  set_sim_state_c11_EMS_situation22_test6
    ((SFc11_EMS_situation22_test6InstanceStruct*)chartInfo->chartInstance,
     mxDuplicateArray(plhs[0]));
  mxDestroyArray(plhs[0]);
}

static const mxArray* sf_opaque_get_sim_state_c11_EMS_situation22_test6
  (SimStruct* S)
{
  return sf_internal_get_sim_state_c11_EMS_situation22_test6(S);
}

static void sf_opaque_set_sim_state_c11_EMS_situation22_test6(SimStruct* S,
  const mxArray *st)
{
  sf_internal_set_sim_state_c11_EMS_situation22_test6(S, st);
}

static void sf_opaque_terminate_c11_EMS_situation22_test6(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc11_EMS_situation22_test6InstanceStruct*)
                    chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_EMS_situation22_test6_optimization_info();
    }

    finalize_c11_EMS_situation22_test6
      ((SFc11_EMS_situation22_test6InstanceStruct*) chartInstanceVar);
    utFree((void *)chartInstanceVar);
    if (crtInfo != NULL) {
      utFree((void *)crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc11_EMS_situation22_test6
    ((SFc11_EMS_situation22_test6InstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c11_EMS_situation22_test6(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c11_EMS_situation22_test6
      ((SFc11_EMS_situation22_test6InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c11_EMS_situation22_test6(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_EMS_situation22_test6_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      11);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,11,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,11,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,11);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 4, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 5, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 6, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 7, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 8, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 9, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 10, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 11, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,11,12);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,11,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 12; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,11);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(302822473U));
  ssSetChecksum1(S,(894974261U));
  ssSetChecksum2(S,(1724348239U));
  ssSetChecksum3(S,(947342924U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c11_EMS_situation22_test6(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c11_EMS_situation22_test6(SimStruct *S)
{
  SFc11_EMS_situation22_test6InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc11_EMS_situation22_test6InstanceStruct *)utMalloc(sizeof
    (SFc11_EMS_situation22_test6InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc11_EMS_situation22_test6InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c11_EMS_situation22_test6;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c11_EMS_situation22_test6;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c11_EMS_situation22_test6;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c11_EMS_situation22_test6;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c11_EMS_situation22_test6;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c11_EMS_situation22_test6;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c11_EMS_situation22_test6;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c11_EMS_situation22_test6;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c11_EMS_situation22_test6;
  chartInstance->chartInfo.mdlStart = mdlStart_c11_EMS_situation22_test6;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c11_EMS_situation22_test6;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c11_EMS_situation22_test6_method_dispatcher(SimStruct *S, int_T method,
  void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c11_EMS_situation22_test6(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c11_EMS_situation22_test6(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c11_EMS_situation22_test6(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c11_EMS_situation22_test6_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

%% 在等待中，free还是tunnel。u（i，8）是wait2free，u（i，9）是wait2tunnel。
function wait2what()
global UavTeam
global U N
global Flaststate
global priority_yes

M = UavTeam.AvailableNumMax;

%% 分组判断
for gg = 1 : N
    tempflaststate = Flaststate (M*(gg-1)+1:M*(gg-1)+M, 1); %%%%%% 取出一组矩阵判断
    %% 是否有在等投递点的飞机 且 这一组还未选出priority飞机
    if ismember(3,tempflaststate) == 1 && priority_yes(gg,1) == 0
        % 是否区域清空
        clearTFD = Toufangdian_clear(UavTeam.Team(gg).Uav(1).Toufangdian); %%%%%%% 每组投放点一致，所以直接取Uav(1).
        clearLagT = Lag_clear(gg,UavTeam.Team(gg).Uav(1).Toufangdian,3); % tunnel尽头有无状态飞机 标志位3投放点
        % 如果清空了，谁优先？
        if clearTFD == 1 && clearLagT == 1
            [TFDHg,TFDHi] = whoH(UavTeam.Team(gg).Uav(1).Toufangdian);
            if TFDHg~= 0 && TFDHi~=0
                U(M*11*(TFDHg-1)+TFDHi+8*M,1)=1; % wait2tunnel
                %                 UavTeam.Team(TFDHg).Uav(TFDHi).CurrentTaskNum = UavTeam.Team(TFDHg).Uav(TFDHi).CurrentTaskNum +1; %%%%%%%%% task +1??!!!
                fprintf('%d组',TFDHg);
                fprintf('%d飞机',TFDHi);
                fprintf('结束等待开始free2投递点fffff，下一任务%d \n',UavTeam.Team(TFDHg).Uav(TFDHi).CurrentTaskNum);
                UavTeam.Team(TFDHg).Uav(TFDHi).priority = inf;
                UavTeam.Team(TFDHg).Uav(TFDHi).state = 6;
                Flaststate(M*(TFDHg-1)+TFDHi,1) = inf; % 回到最初
                priority_yes(gg,1) = 1; % 该分组已经选出一个最优先的，直到他完成任务才会轮到下一个,第1列是投递点
            end
        end
    end
    %% 是否有在等家点的飞机 且 这一组还未选出priority飞机
    if ismember(2,tempflaststate) == 1 && priority_yes(gg,2) == 0
        % 是否区域清空
        clearH1 = Home_clear(UavTeam.Team(gg).Uav(1).HomePos);
        clearLagH = Lag_clear(gg,UavTeam.Team(gg).Uav(1).HomePos,2); % tunnel尽头有无状态飞机 标志位2家点
        % 如果清空了，谁优先
        if clearH1 == 1 && clearLagH == 1
            [TFDHg,TFDHi] = whoH(UavTeam.Team(gg).Uav(1).HomePos);
            if TFDHg~= 0 && TFDHi~=0
                U(M*11*(TFDHg-1)+TFDHi+8*M,1)=1; % wait2tunnel
                %                 UavTeam.Team(TFDHg).Uav(TFDHi).CurrentTaskNum = UavTeam.Team(TFDHg).Uav(TFDHi).CurrentTaskNum +1; %%%%%%%%% task +1??!!!
                fprintf('%d组',TFDHg);
                fprintf('%d飞机',TFDHi);
                fprintf('结束等待开始free2家点ffffff，下一任务%d \n',UavTeam.Team(TFDHg).Uav(TFDHi).CurrentTaskNum);
                UavTeam.Team(TFDHg).Uav(TFDHi).priority = inf;
                UavTeam.Team(TFDHg).Uav(TFDHi).state = 6;
                Flaststate(M*(TFDHg-1)+TFDHi,1) = inf; % 回到最初
                priority_yes(gg,2) = 1; % 该分组已经选出一个最优先的，直到他完成任务才会轮到下一个，第2列是投递点
            end
        end
    end
end
%% HomePos区域内有可见且无货的飞机
function HomPos_see_unload_yes= HomPos_see_unload(j,g,center)
global UavTeam N

M = UavTeam.AvailableNumMax;
HomPos_see_unload_yes = 0;
temp = 0;

for nn=1:N
    for k1=1:M
        if k1 == j && nn == g  % 排除自己
        elseif UavTeam.Team(nn).Uav(k1).state == 4 || UavTeam.Team(nn).Uav(k1).state == 6 || UavTeam.Team(nn).Uav(k1).state == 10
            % 在区域内  HomPos_see_unload_yes = localarea(g,j,UavTeam.Team(g).Uav(j).HomePos,UavTeam.Team(nn).Uav(k1).CurrentPos);
            % 变成，在3个状态和目标对应
            ctn = UavTeam.Team(nn).Uav(k1).CurrentTaskNum;
            Pdes  =  UavTeam.Team(nn).Uav(k1).Waypoint(:,ctn);
            if Pdes == center
                HomPos_see_unload_yes = 1;
                temp = 1;
                break;
            end
        end
    end
    if temp == 1
        break;
    end
end
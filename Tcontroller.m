%% 过隧道模式，标志位=0
function u= Tcontroller(i,g)
global UavTeam
global currenttunnel
global UavHighway
global U

Pcur  =  UavTeam.Team(g).Uav(i).CurrentPos;
Vcur  =  UavTeam.Team(g).Uav(i).Velocity;
ra = UavTeam.Team(g).Uav(i).ra;
UavTeam.Team(g).Uav(i).state = 1;
M = UavTeam.AvailableNumMax;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Area Definition
ctntemp = currenttunnel(M*(g-1)+i,1); % 初始currentunnel = 1

%% 后续tunnel转为第一个
pt1 = coordinate1(ctntemp,UavHighway.group(g).Highway(ctntemp).ph1,g);
pt2 = coordinate1(ctntemp,UavHighway.group(g).Highway(ctntemp).ph2,g);
rt = UavHighway.group(g).Highway(ctntemp).rh;
pt3 = pt2 + [0 rt]';
ksitemp  = Pcur + 1/UavTeam.gain*Vcur;
ksi = coordinate1(ctntemp,ksitemp,g);
rb = ra;
rsr= 10000;
rrt= 10000;
%% 判断区域
if ksi(1)>0 && ksi(2)>rt  % Left Standby Area
    psr1 = [pt2(1) rt+rsr]';
    psr2 = [-rb rt+rsr]';
    psr3 = [-rb rt]';
    %% 转回初始状态
    psr1 = coordinate2(ctntemp,psr1,g);
    psr2 = coordinate2(ctntemp,psr2,g);
    psr3 = coordinate2(ctntemp,psr3,g);
    u = mytunnel(g,i,psr1,psr2,psr3,rsr,0,ctntemp);
    
elseif  ksi(1)>0 && ksi(2)<-rt  % Right Standby Area
    psr1p = [pt2(1) -rt-rsr]';
    psr2p = [-rb -rt-rsr]';
    psr3p = [-rb -rt]';
    %% 转回初始状态
    psr1p = coordinate2(ctntemp,psr1p,g);
    psr2p = coordinate2(ctntemp,psr2p,g);
    psr3p = coordinate2(ctntemp,psr3p,g);
    u = mytunnel(g,i,psr1p,psr2p,psr3p,rsr,0,ctntemp);
    
elseif  ksi(1)<=0 && ksi(2)>=rt % Left Ready Area
    prt1 = [-rrt rt+rrt]';
    prt2 = [-rrt rt-rb]';
    prt3 = [0    rt-rb]';
    %% 转回初始状态
    prt1 = coordinate2(ctntemp,prt1,g);
    prt2 = coordinate2(ctntemp,prt2,g);
    prt3 = coordinate2(ctntemp,prt3,g);
    u = mytunnel(g,i,prt1,prt2,prt3,rrt,0,ctntemp);
    
elseif  ksi(1)<=0 && ksi(2)<=-rt % Left Ready Area
    prt1p = [-rrt rt+rrt]';
    prt2p = [-rrt rt-rb]';
    prt3p = [0    rt-rb]';
    %% 转回初始状态
    prt1p = coordinate2(ctntemp,prt1p,g);
    prt2p = coordinate2(ctntemp,prt2p,g);
    prt3p = coordinate2(ctntemp,prt3p,g);
    u = mytunnel(g,i,prt1p,prt2p,prt3p,rrt,0,ctntemp);
    
else% Tunnel and Tunnel Extension
    %% 转回初始状态
    pt1 = coordinate2(ctntemp,pt1,g);
    pt2 = coordinate2(ctntemp,pt2,g);
    pt3 = coordinate2(ctntemp,pt3,g);
    u = mytunnel(g,i,pt1,pt2,pt3,rt,0,ctntemp);
end

%% task+1
length = norm(UavHighway.group(g).Highway(ctntemp).ph1-UavHighway.group(g).Highway(ctntemp).ph2);
tempksi1 = round(ksi(1));
tempksi2 = round(ksi(2));
if  tempksi1 <= length+ra && tempksi1 >= length && tempksi2 <= rt && tempksi2 >= -rt
    % 每通过一个tunnel，wait2tunnel就恢复0
    U(M*11*(g-1)+i+8*M,1)=0;
    
    UavTeam.Team(g).Uav(i).CurrentTaskNum = UavTeam.Team(g).Uav(i).CurrentTaskNum + 1;
    fprintf('%d组 ',g);
    fprintf('飞机%d ',i);
    fprintf('通过该隧道，task+1，下一任务%d，',UavTeam.Team(g).Uav(i).CurrentTaskNum);
    % tunnel与task的关系
    currenttunnel(M*(g-1)+i,1) = currenttunnel(M*(g-1)+i,1)+1;
    fprintf('currenttunnel+1，下一管道号%d \n',currenttunnel(M*(g-1)+i,1));
end
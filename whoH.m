%% in X area which has the highest priority
function [Hg,Hi]= whoH(center)
global UavTeam 
global N

M = UavTeam.AvailableNumMax;
temp = inf;
Hg = 0;
Hi = 0;
for nn = 1:N
    for mm = 1:M
        ctn = UavTeam.Team(nn).Uav(mm).CurrentTaskNum;
        % 1.处于等待状态。2.next目标点一致。3.优先级更小
        if UavTeam.Team(nn).Uav(mm).state == 7 & UavTeam.Team(nn).Uav(mm).Waypoint(:,ctn+1) == center & UavTeam.Team(nn).Uav(mm).priority < temp
                temp = UavTeam.Team(nn).Uav(mm).priority;
                Hg = nn;
                Hi = mm;
        end
    end
end